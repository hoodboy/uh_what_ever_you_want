﻿using UnityEngine;
using GameManager;

public class LeftRightObserver : MonoBehaviour {
    private Player player;
    private Animator anim;
    private bool isSplatting;
    private bool wasSplatting;

    void Start () {
        player = GetComponent<Player>();
        anim = GetComponent<Animator>();
    }
    
    void Update () {
        anim.SetBool("isFacingRight", player.facingDirection == Vector2.right);

        SplatCheck();
    }

    void SplatCheck() {
        isSplatting = anim.GetCurrentAnimatorStateInfo(0).IsName("Sketchy_WildTake04");

        if (isSplatting && !wasSplatting) {
            wasSplatting = true;
            Generic.isControllable = false;

            CameraFollowSmooth camSmooth = Camera.main.GetComponent<CameraFollowSmooth>();
            if (camSmooth) {
                camSmooth.StartCoroutine("Shake");
            }
        }

        if (!isSplatting && wasSplatting) {
            Generic.isControllable = true;
        }

        wasSplatting = isSplatting;
    }
}
