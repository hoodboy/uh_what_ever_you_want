﻿using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

[CustomEditor(typeof(PuzzleBlockadeUnit))]
public class PuzzleBlockadeUnitEditor : Editor {
    PuzzleBlockadeUnit puzzleBlockadeUnit;

    void OnEnable() {
        puzzleBlockadeUnit = (PuzzleBlockadeUnit)target;
    }

    public override void OnInspectorGUI() {
        DrawDefaultInspector();
        if (GUILayout.Button("Record Start Position")) {
            puzzleBlockadeUnit.startPos = puzzleBlockadeUnit.transform.position;
        }

        if (GUILayout.Button("Record End Position")) {
            puzzleBlockadeUnit.endPos = puzzleBlockadeUnit.transform.position;
        }
    }
}