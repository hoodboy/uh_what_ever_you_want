﻿using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

[CustomEditor(typeof(EventsTrigger))]
public class EventsTriggerEditor : Editor {
    Events events;
    Color[] colours = new Color[16];
    string[] errorIDs = new string[16];
    Color nullColour = Color.grey;
    Color positiveColour = Color.green;
    Color negativeColour = Color.red;

    void OnEnable() {
        events = (Events)target;

        for (int i = 0; i < colours.Length; i++) {
            colours[i] = Color.clear;
        }

        for (int i = 0; i < errorIDs.Length; i++) {
            errorIDs[i] = "";
        }
    }

    public override void OnInspectorGUI() {
        DrawDefaultInspector();
        GUI.backgroundColor = Color.black;
        EditorGUILayout.HelpBox("Subject\t\t\t\tBrokenID\t\tAmount Found", MessageType.None, true);
        GUI.backgroundColor = colours[0];
        EditorGUILayout.HelpBox("Events.Lights\t\t\t"           + errorIDs[0] + "\t\t\t" + Lights(), MessageType.None, true);
        GUI.backgroundColor = colours[1];
        EditorGUILayout.HelpBox("Events.ParticleSystems\t\t"    + errorIDs[1] + "\t\t\t" + ParticleSystems(), MessageType.None, true);
        GUI.backgroundColor = colours[2];
        EditorGUILayout.HelpBox("Events.Animators\t\t\t"        + errorIDs[2] + "\t\t\t" + Animators(), MessageType.None, true);
        GUI.backgroundColor = colours[3];
        EditorGUILayout.HelpBox("Events.AdvancedAnimators\t\t"  + errorIDs[3] + "\t\t\t" + AdvancedAnimators(), MessageType.None, true);
        GUI.backgroundColor = colours[4];
        EditorGUILayout.HelpBox("Events.ObjectsToSpawn\t\t"     + errorIDs[4] + "\t\t\t" + ObjectsToSpawn(), MessageType.None, true);
        GUI.backgroundColor = colours[5];
        EditorGUILayout.HelpBox("Events.ObjectsToDestroy\t\t"   + errorIDs[5] + "\t\t\t" + ObjectsToDestroy(), MessageType.None, true);
        GUI.backgroundColor = colours[6];
        EditorGUILayout.HelpBox("Events.BeginningDialogue\t\t"  + "N/A" + "\t\t\t" + BeginningDialogue(), MessageType.None, true);
        GUI.backgroundColor = colours[7];
        EditorGUILayout.HelpBox("Events.EndDialogue\t\t"        + "N/A" + "\t\t\t" + EndDialogue(), MessageType.None, true);
        GUI.backgroundColor = colours[8];
        EditorGUILayout.HelpBox("CutScenes\t\t\t"               + errorIDs[8] + "\t\t\t" + CutScenes(), MessageType.None, true);
        GUI.backgroundColor = colours[9];
        EditorGUILayout.HelpBox("CutScenes.Dialogues\t\t"       + "N/A" + "\t\t\t" + CutSceneEventsDialogues(), MessageType.None, true);
        GUI.backgroundColor = colours[10];
        EditorGUILayout.HelpBox("CutScenes.Lights\t\t\t"        + errorIDs[10] + "\t\t\t" + CutSceneEventsLights(), MessageType.None, true);
        GUI.backgroundColor = colours[11];
        EditorGUILayout.HelpBox("CutScenes.ParticleSystems\t\t" + errorIDs[11] + "\t\t\t" + CutSceneEventsParticleSystems(), MessageType.None, true);
        GUI.backgroundColor = colours[12];
        EditorGUILayout.HelpBox("CutScenes.Animators\t\t"       + errorIDs[12] + "\t\t\t" + CutSceneEventsAnimators(), MessageType.None, true);
        GUI.backgroundColor = colours[13];
        EditorGUILayout.HelpBox("CutScenes.AdvancedAnimators\t" + errorIDs[13] + "\t\t\t" + CutSceneEventsAdvancedAnimators(), MessageType.None, true);
        GUI.backgroundColor = colours[14];
        EditorGUILayout.HelpBox("CutScenes.ObjectsToSpawn\t\t"  + errorIDs[14] + "\t\t\t" + CutSceneEventsObjectsToSpawn(), MessageType.None, true);
        GUI.backgroundColor = colours[15];
        EditorGUILayout.HelpBox("CutScenes.ObjectsToDestroy\t"  + errorIDs[15] + "\t\t\t" + CutSceneEventsObjectsToDestroy(), MessageType.None, true);
    }

    private int Lights() {
        errorIDs[0] = "";

        if (events.events.lights != null && events.events.lights.Length > 0) {
            for (int i = 0; i < events.events.lights.Length; i++) {
                if (events.events.lights[i] == null) {
                    colours[0] = negativeColour;
                    errorIDs[0] = "";
                    Debug.Log(events.gameObject.name + " has a broken Light (Light: " + i.ToString() + ")");
                    break;
                } else {
                    colours[0] = positiveColour;
                }
            }
        } else {
            colours[0] = nullColour;
        }

        return events.events.lights != null ? events.events.lights.Length : 0;
    }

    private int ParticleSystems() {
        errorIDs[1] = "";

        if (events.events.particleSystems != null && events.events.particleSystems.Length > 0) {
            for (int i = 0; i < events.events.particleSystems.Length; i++) {
                if (events.events.particleSystems[i] == null) {
                    colours[1] = negativeColour;
                    errorIDs[1] = "";
                    Debug.Log(events.gameObject.name + " has a broken ParticleSystem (ParticleSystem: " + i.ToString() + ")");
                    break;
                } else {
                    colours[1] = positiveColour;
                }
            }
        } else {
            colours[1] = nullColour;
        }

        return events.events.particleSystems != null ? events.events.particleSystems.Length : 0;
    }

    private int Animators() {
        errorIDs[2] = "";

        if (events.events.animators != null && events.events.animators.Length > 0) {
            for (int i = 0; i < events.events.animators.Length; i++) {
                if (events.events.animators[i] == null) {
                    colours[2] = negativeColour;
                    errorIDs[2] = "";
                    Debug.Log(events.gameObject.name + " has a broken Animator (Animator: " + i.ToString() + ")");
                    break;
                } else {
                    colours[2] = positiveColour;
                }
            }
        } else {
            colours[2] = nullColour;
        }

        return events.events.animators != null ? events.events.animators.Length : 0;
    }

    private int AdvancedAnimators() {
        errorIDs[3] = "";

        if (events.events.advancedAnimators != null && events.events.advancedAnimators.Length > 0) {
            for (int i = 0; i < events.events.advancedAnimators.Length; i++) {
                if (events.events.advancedAnimators[i].triggeredAnimator == null) {
                    colours[3] = negativeColour;
                    errorIDs[3] = i.ToString();
                    Debug.Log(events.gameObject.name + " has a broken AdvancedAnimator (AdvancedAnimator: " + i.ToString() + ")");
                    break;
                } else {
                    colours[3] = positiveColour;
                }
            }
        } else {
            colours[3] = nullColour;
        }

        return events.events.advancedAnimators != null ? events.events.advancedAnimators.Length : 0;
    }

    private int ObjectsToSpawn() {
        errorIDs[4] = "";

        if (events.events.objectsToSpawn != null && events.events.objectsToSpawn.Length > 0) {
            for (int i = 0; i < events.events.objectsToSpawn.Length; i++) {
                if (events.events.objectsToSpawn[i].objectToSpawn == null || events.events.objectsToSpawn[i].spawnLocationObject == null) {
                    colours[4] = negativeColour;
                    errorIDs[4] = i.ToString();
                    Debug.Log(events.gameObject.name + " has a broken ObjectToSpawn (ObjectToSpawn: " + i.ToString() + ")");
                    break;
                } else {
                    colours[4] = positiveColour;
                }
            }
        } else {
            colours[4] = nullColour;
        }

        return events.events.objectsToSpawn != null ? events.events.objectsToSpawn.Length : 0;
    }

    private int ObjectsToDestroy() {
        errorIDs[5] = "";

        if (events.events.objectsToDestroy != null && events.events.objectsToDestroy.Length > 0) {
            for (int i = 0; i < events.events.objectsToDestroy.Length; i++) {
                if (events.events.objectsToDestroy[i].objectToDestroy == null) {
                    colours[5] = negativeColour;
                    errorIDs[5] = i.ToString();
                    Debug.Log(events.gameObject.name + " has a broken ObjectToDestroy (ObjectToDestroy: " + i.ToString() + ")");
                    break;
                } else {
                    colours[5] = positiveColour;
                }
            }
        } else {
            colours[5] = nullColour;
        }

        return events.events.objectsToDestroy != null ? events.events.objectsToDestroy.Length : 0;
    }

    private int BeginningDialogue() {
        colours[6] = events.beginningDialogue.text != null && events.beginningDialogue.text.Length > 0 ? positiveColour : nullColour;
        return events.beginningDialogue.text != null && events.beginningDialogue.text.Length > 0 ? 1 : 0;
    }

    private int EndDialogue() {
        colours[7] = events.endDialogue.text != null && events.endDialogue.text.Length > 0 ? positiveColour : nullColour;
        return events.endDialogue.text != null && events.endDialogue.text.Length > 0 ? 1 : 0;
    }

    private int CutScenes() {
        errorIDs[8] = "";

        if (events.cutScenes != null && events.cutScenes.Length > 0) {
            colours[8] = positiveColour;
        } else {
            colours[8] = nullColour;
        }

        return events.cutScenes != null ? events.cutScenes.Length : 0;
    }

    private int CutSceneEventsDialogues() {
        CutSceneEventsDialoguesColour();

        int count = 0;

        if (events.cutScenes != null && events.cutScenes.Length > 0) {
            foreach (Events.CutScenes cs in events.cutScenes) {
                if (cs.dialogue.text != null && cs.dialogue.text.Length > 0) {
                    count++;
                }
            }
        }

        return count;
    }

    private void CutSceneEventsDialoguesColour() {
        errorIDs[9] = "";

        if (events.cutScenes != null && events.cutScenes.Length > 0) {
            bool hasAtLeastOne = false;

            for (int i = 0; i < events.cutScenes.Length; i++) {
                if (events.cutScenes[i].dialogue.text != null && events.cutScenes[i].dialogue.text.Length > 0) {
                    colours[9] = positiveColour;
                    errorIDs[9] = i.ToString();
                    break;
                }
                else if (!hasAtLeastOne) {
                    colours[9] = nullColour;
                    hasAtLeastOne = true;
                }
            }
        } else {
            colours[9] = nullColour;
        }
    }

    private int CutSceneEventsLights() {
        CutSceneEventsLightsColour();

        int count = 0;

        if (events.cutScenes != null && events.cutScenes.Length > 0) {
            foreach (Events.CutScenes cs in events.cutScenes) {
                count += cs.advancedEvents.lights.Length;
            }
        }

        return count;
    }

    private void CutSceneEventsLightsColour() {
        errorIDs[10] = "";

        if (events.cutScenes != null && events.cutScenes.Length > 0) {
            bool hasAtLeastOne = false;

            for (int a = 0; a < events.cutScenes.Length; a++) {
                if (events.cutScenes[a].advancedEvents.lights != null 
                &&  events.cutScenes[a].advancedEvents.lights.Length > 0) {
                    for (int b = 0; b < events.cutScenes[a].advancedEvents.lights.Length; b++) {
                        if (events.cutScenes[a].advancedEvents.lights[b] == null) {
                            colours[10] = negativeColour;
                            errorIDs[10] = a.ToString() + " : " + b.ToString();
                            Debug.Log(events.gameObject.name + " has a broken Cutscene.Light (CutScene: " + a.ToString() + " / Light: " + b.ToString() + ")");
                            return;
                        } else {
                            colours[10] = positiveColour;
                            hasAtLeastOne = true;
                        }
                    }
                } else if (!hasAtLeastOne) {
                    colours[10] = nullColour;
                }
            }
        } else {
            colours[10] = nullColour;
        }
    }

    private int CutSceneEventsParticleSystems() {
        CutSceneEventsParticleSystemsColour();

        int count = 0;

        if (events.cutScenes != null && events.cutScenes.Length > 0) {
            foreach (Events.CutScenes cs in events.cutScenes) {
                count += cs.advancedEvents.particleSystems.Length;
            }
        }

        return count;
    }

    private void CutSceneEventsParticleSystemsColour() {
        errorIDs[11] = "";

        if (events.cutScenes != null && events.cutScenes.Length > 0) {
            bool hasAtLeastOne = false;

            for (int a = 0; a < events.cutScenes.Length; a++) {
                if (events.cutScenes[a].advancedEvents.particleSystems != null 
                &&  events.cutScenes[a].advancedEvents.particleSystems.Length > 0) {
                    for (int b = 0; b < events.cutScenes[a].advancedEvents.particleSystems.Length; b++) {
                        if (events.cutScenes[a].advancedEvents.particleSystems[b] == null) {
                            colours[11] = negativeColour;
                            errorIDs[11] = a.ToString() + " : " + b.ToString();
                            Debug.Log(events.gameObject.name + " has a broken Cutscene.ParticleSystem (CutScene: " + a.ToString() + " / ParticleSystem: " + b.ToString() + ")");
                            return;
                        } else {
                            colours[11] = positiveColour;
                            hasAtLeastOne = true;
                        }
                    }
                } else if (!hasAtLeastOne) {
                    colours[11] = nullColour;
                }
            }
        } else {
            colours[11] = nullColour;
        }
    }

    private int CutSceneEventsAnimators() {
        CutSceneEventsAnimatorsColour();

        int count = 0;

        if (events.cutScenes != null && events.cutScenes.Length > 0) {
            foreach (Events.CutScenes cs in events.cutScenes) {
                count += cs.advancedEvents.animators.Length;
            }
        }

        return count;
    }

    private void CutSceneEventsAnimatorsColour() {
        errorIDs[12] = "";

        if (events.cutScenes != null  && events.cutScenes.Length > 0) {
            bool hasAtLeastOne = false;

            for (int a = 0; a < events.cutScenes.Length; a++) {
                if (events.cutScenes[a].advancedEvents.animators != null && events.cutScenes[a].advancedEvents.animators.Length > 0) {
                    for (int b = 0; b < events.cutScenes[a].advancedEvents.animators.Length; b++) {
                        if (events.cutScenes[a].advancedEvents.animators[b] == null) {
                            colours[12] = negativeColour;
                            errorIDs[12] = a.ToString() + " : " + b.ToString();
                            Debug.Log(events.gameObject.name + " has a broken Cutscene.Animator (CutScene: " + a.ToString() + " / Animator: " + b.ToString() + ")");
                            return;
                        } else {
                            colours[12] = positiveColour;
                            hasAtLeastOne = true;
                        }
                    }
                } else if (!hasAtLeastOne) {
                    colours[12] = nullColour;
                }
            }
        } else {
            colours[12] = nullColour;
        }
    }

    private int CutSceneEventsAdvancedAnimators() {
        CutSceneEventsAdvancedAnimatorsColour();

        int count = 0;

        if (events.cutScenes != null && events.cutScenes.Length > 0) {
            foreach (Events.CutScenes cs in events.cutScenes) {
                count += cs.advancedEvents.advancedAnimators.Length;
            }
        }

        return count;
    }

    private void CutSceneEventsAdvancedAnimatorsColour() {
        errorIDs[13] = "";

        if (events.cutScenes != null && events.cutScenes.Length > 0) {
            bool hasAtLeastOne = false;

            for (int a = 0; a < events.cutScenes.Length; a++) {
                if (events.cutScenes[a].advancedEvents.advancedAnimators != null && events.cutScenes[a].advancedEvents.advancedAnimators.Length > 0) {
                    for (int b = 0; b < events.cutScenes[a].advancedEvents.advancedAnimators.Length; b++) {
                        if (events.cutScenes[a].advancedEvents.advancedAnimators[b].triggeredAnimator == null) {
                            colours[13] = negativeColour;
                            errorIDs[13] = a.ToString() + " : " + b.ToString();
                            Debug.Log(events.gameObject.name + " has a broken Cutscene.AdvancedAnimator (CutScene: " + a.ToString() + " / AdvancedAnimator: " + b.ToString() + ")");
                            break;
                        } else {
                            colours[13] = positiveColour;
                            hasAtLeastOne = true;
                        }
                    }
                } else if (!hasAtLeastOne) {
                    colours[13] = nullColour;
                }
            }
        } else {
            colours[13] = nullColour;
        }
    }

    private int CutSceneEventsObjectsToSpawn() {
        CutSceneEventsObjectsToSpawnColour();

        int count = 0;

        if (events.cutScenes != null && events.cutScenes.Length > 0) {
            foreach (Events.CutScenes cs in events.cutScenes) {
                count += cs.advancedEvents.objectsToSpawn.Length;
            }
        }

        return count;
    }

    private void CutSceneEventsObjectsToSpawnColour() {
        errorIDs[14] = "";

        if (events.cutScenes != null && events.cutScenes.Length > 0) {
            bool hasAtLeastOne = false;

            for (int a = 0; a < events.cutScenes.Length; a++) {
                if (events.cutScenes[a].advancedEvents.objectsToSpawn != null && events.cutScenes[a].advancedEvents.objectsToSpawn.Length > 0) {
                    for (int b = 0; b < events.cutScenes[a].advancedEvents.objectsToSpawn.Length; b++) {
                        if (events.cutScenes[a].advancedEvents.objectsToSpawn[b].objectToSpawn == null || events.cutScenes[a].advancedEvents.objectsToSpawn[b].spawnLocationObject == null) {
                            colours[14] = negativeColour;
                            errorIDs[14] = a.ToString() + " : " + b.ToString();
                            Debug.Log(events.gameObject.name + " has a broken Cutscene.ObjectToSpawn (CutScene: " + a.ToString() + " / ObjectToSpawn: " + b.ToString() + ")");
                            break;
                        } else {
                            colours[14] = positiveColour;
                            hasAtLeastOne = true;
                        }
                    }
                } else if (!hasAtLeastOne) {
                    colours[14] = nullColour;
                }
            }
        } else {
            colours[14] = nullColour;
        }
    }

    private int CutSceneEventsObjectsToDestroy() {
        CutSceneEventsObjectsToDestroyColour();

        int count = 0;

        if (events.cutScenes != null && events.cutScenes.Length > 0) {
            foreach (Events.CutScenes cs in events.cutScenes) {
                count += cs.advancedEvents.objectsToDestroy.Length;
            }
        }

        return count;
    }

    private void CutSceneEventsObjectsToDestroyColour() {
        errorIDs[15] = "";

        if (events.cutScenes != null && events.cutScenes.Length > 0) {
            bool hasAtLeastOne = false;

            for (int a = 0; a < events.cutScenes.Length; a++) {
                if (events.cutScenes[a].advancedEvents.objectsToDestroy != null && events.cutScenes[a].advancedEvents.objectsToDestroy.Length > 0) {
                    for ( int b = 0; b < events.cutScenes[a].advancedEvents.objectsToDestroy.Length; b++) {
                        if (events.cutScenes[a].advancedEvents.objectsToDestroy[b].objectToDestroy == null) {
                            colours[15] = negativeColour;
                            errorIDs[15] = a.ToString() + " : " + b.ToString();
                            Debug.Log(events.gameObject.name + " has a broken Cutscene.ObjectToDestroy (CutScene: " + a.ToString() + " / ObjectToDestroy: " + b.ToString() + ")");
                            break;
                        } else {
                            colours[15] = positiveColour;
                            hasAtLeastOne = true;
                        }
                    }
                } else if (!hasAtLeastOne) {
                    colours[15] = nullColour;
                }
            }
        } else {
            colours[15] = nullColour;
        }
    }
}