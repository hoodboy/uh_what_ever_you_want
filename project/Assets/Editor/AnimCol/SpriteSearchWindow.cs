﻿using UnityEngine;
using System.Collections.Generic;
using UnityEditor;

public class SpriteSearchWindow : EditorWindow {
    Sprite sprite;
    SpriteRenderer spriteRenderer;
    [SerializeField]
    List<SpriteRenderer> spriteRenderers = new List<SpriteRenderer>();

    Vector2 scrollPos = new Vector2();

    [MenuItem("Window/AnimCol/SpriteSearch")]
    static void Init() {
        SpriteSearchWindow window = (SpriteSearchWindow)GetWindow(typeof(SpriteSearchWindow));
        window.Show();

        GUIContent titleContent = new GUIContent("Sprite Search");
        window.titleContent = titleContent;
    }

    void OnGUI() {
        sprite = (Sprite)EditorGUILayout.ObjectField("Sprite Asset:", sprite, typeof(Sprite), false);
        spriteRenderer = (SpriteRenderer)EditorGUILayout.ObjectField("or Scene Object:", spriteRenderer, typeof(SpriteRenderer), true);

        // WELL THIS WAS A HEADACHE!!!
        // "target" can be any class derrived from ScriptableObject 
        // (could be EditorWindow, MonoBehaviour, etc)
        if (spriteRenderers.Count > 0) {
            scrollPos =
            EditorGUILayout.BeginScrollView(scrollPos, GUILayout.MaxHeight(200));
                ScriptableObject target = this;
                SerializedObject so = new SerializedObject(target);
                SerializedProperty spriteRenderersProperty = so.FindProperty("spriteRenderers");
                // SERIOUSLY... OK THAT COMMENTED BLOCK DOWN THE BOTTOM IS MY REFERENCE, FOUND FROM: 
                EditorGUILayout.PropertyField(spriteRenderersProperty, true);
                so.ApplyModifiedProperties();
            EditorGUILayout.EndScrollView();
        }

        if (GUILayout.Button("Find by Sprite")) {
            FindBySprite();
        }

        if (GUILayout.Button("Find by SpriteRenderer")) {
            FindBySpriteRenderer();
        }
        if (GUILayout.Button("Organize")) {
            Organize();
        }

        if (GUILayout.Button("Clear")) {
            Clear();
        }
    }

    public void FindBySprite() {
        if (sprite) {
            spriteRenderers.Clear();

            foreach	(SpriteRenderer sprR in FindObjectsOfType<SpriteRenderer>()){
                if (sprite == sprR.sprite) {
                    spriteRenderers.Add(sprR);
                }
            }
        }
    }

    public void FindBySpriteRenderer() {
        if (spriteRenderer) {
            spriteRenderers.Clear();

            foreach	(SpriteRenderer sprR in FindObjectsOfType<SpriteRenderer>()){
                if (spriteRenderer.sprite == sprR.sprite) {
                    spriteRenderers.Add(sprR);
                }
            }
        }
    }

    public void Organize() {
        GameObject go = new GameObject();
        go.name = spriteRenderers[0].sprite.name;

        foreach (SpriteRenderer sprRend in spriteRenderers) {
            sprRend.transform.SetParent(go.transform, true);
        }
    }

    public void Clear() {
        spriteRenderers.Clear();
    }
}

//public class MyEditorWindow : EditorWindow
//{
//    [MenuItem("Window/My Editor Window")]
//    public static void ShowWindow()
//    {
//        GetWindow<MyEditorWindow>();
//    }

//    public string[] Strings = { "Larry", "Curly", "Moe" };

//    void OnGUI()
//    {
//        // "target" can be any class derrived from ScriptableObject 
//        // (could be EditorWindow, MonoBehaviour, etc)
//        ScriptableObject target = this;
//        SerializedObject so = new SerializedObject(target);
//        SerializedProperty stringsProperty = so.FindProperty("Strings");

//        EditorGUILayout.PropertyField(stringsProperty, true); // True means show children
//        so.ApplyModifiedProperties(); // Remember to apply modified properties
//    }
//}