﻿using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

[CustomEditor(typeof(Switch))]
public class SwitchEditor : Editor {
    Switch switchEvents;
    Color[] colours = new Color[4];
    string[] errorIDs = new string[4];
    Color nullColour = Color.grey;
    Color positiveColour = Color.green;
    Color negativeColour = Color.red;

    void OnEnable() {
        switchEvents = (Switch)target;

        for (int i = 0; i < colours.Length; i++) {
            colours[i] = Color.clear;
        }

        for (int i = 0; i < errorIDs.Length; i++) {
            errorIDs[i] = "";
        }
    }

    public override void OnInspectorGUI() {
        DrawDefaultInspector();
        GUI.backgroundColor = Color.black;
        EditorGUILayout.HelpBox("Subject\t\t\t\tBrokenID\t\tAmount Found", MessageType.None, true);
        GUI.backgroundColor = colours[0];
        EditorGUILayout.HelpBox("Events.Lights\t\t\t"           + errorIDs[0] + "\t\t\t" + Lights(), MessageType.None, true);
        GUI.backgroundColor = colours[1];
        EditorGUILayout.HelpBox("Events.ParticleSystems\t\t"    + errorIDs[1] + "\t\t\t" + ParticleSystems(), MessageType.None, true);
        GUI.backgroundColor = colours[2];
        EditorGUILayout.HelpBox("Events.Animators\t\t\t"        + errorIDs[2] + "\t\t\t" + Animators(), MessageType.None, true);
        GUI.backgroundColor = colours[3];
        EditorGUILayout.HelpBox("Events.AdvancedAnimators\t\t"  + errorIDs[3] + "\t\t\t" + AdvancedAnimators(), MessageType.None, true);
    }

    private int Lights() {
        errorIDs[0] = "";

        if (switchEvents.lights != null && switchEvents.lights.Length > 0) {
            for (int i = 0; i < switchEvents.lights.Length; i++) {
                if (switchEvents.lights[i] == null) {
                    colours[0] = negativeColour;
                    errorIDs[0] = "";
                    Debug.Log(switchEvents.gameObject.name + " has a broken Light (Light: " + i.ToString() + ")");
                    break;
                } else {
                    colours[0] = positiveColour;
                }
            }
        } else {
            colours[0] = nullColour;
        }

        return switchEvents.lights != null ? switchEvents.lights.Length : 0;
    }

    private int ParticleSystems() {
        errorIDs[1] = "";

        if (switchEvents.particleSystems != null && switchEvents.particleSystems.Length > 0) {
            for (int i = 0; i < switchEvents.particleSystems.Length; i++) {
                if (switchEvents.particleSystems[i] == null) {
                    colours[1] = negativeColour;
                    errorIDs[1] = "";
                    Debug.Log(switchEvents.gameObject.name + " has a broken ParticleSystem (ParticleSystem: " + i.ToString() + ")");
                    break;
                } else {
                    colours[1] = positiveColour;
                }
            }
        } else {
            colours[1] = nullColour;
        }

        return switchEvents.particleSystems != null ? switchEvents.particleSystems.Length : 0;
    }

    private int Animators() {
        errorIDs[2] = "";

        if (switchEvents.animators != null && switchEvents.animators.Length > 0) {
            for (int i = 0; i < switchEvents.animators.Length; i++) {
                if (switchEvents.animators[i] == null) {
                    colours[2] = negativeColour;
                    errorIDs[2] = "";
                    Debug.Log(switchEvents.gameObject.name + " has a broken Animator (Animator: " + i.ToString() + ")");
                    break;
                } else {
                    colours[2] = positiveColour;
                }
            }
        } else {
            colours[2] = nullColour;
        }

        return switchEvents.animators != null ? switchEvents.animators.Length : 0;
    }

    private int AdvancedAnimators() {
        errorIDs[3] = "";

        if (switchEvents.advancedAnimators != null && switchEvents.advancedAnimators.Length > 0) {
            for (int i = 0; i < switchEvents.advancedAnimators.Length; i++) {
                if (switchEvents.advancedAnimators[i].triggeredAnimator == null) {
                    colours[3] = negativeColour;
                    errorIDs[3] = i.ToString();
                    Debug.Log(switchEvents.gameObject.name + " has a broken AdvancedAnimator (AdvancedAnimator: " + i.ToString() + ")");
                    break;
                } else {
                    colours[3] = positiveColour;
                }
            }
        } else {
            colours[3] = nullColour;
        }

        return switchEvents.advancedAnimators != null ? switchEvents.advancedAnimators.Length : 0;
    }
}