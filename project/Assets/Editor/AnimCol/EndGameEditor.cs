﻿using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

[CustomEditor(typeof(EndGame))]
public class EndGameEditor : Editor {
    public override void OnInspectorGUI() {
        DrawDefaultInspector();
        if (GUILayout.Button("Clear High Scores")) {
            ResetHighScores();
        }
        EditorGUILayout.HelpBox("Press the above button to clear your high scores and start a completely new High Score Board.", MessageType.Info, true);
    }

    public void ResetHighScores() {
        for (int i = 0; i < 5; i++) {
            if (PlayerPrefs.HasKey("Score" + i) && PlayerPrefs.HasKey("Name" + i)) {
                PlayerPrefs.DeleteKey("Score" + i);
                PlayerPrefs.DeleteKey("Name" + i);
            }
        }
    }
}