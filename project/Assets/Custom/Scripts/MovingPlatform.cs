﻿using UnityEngine;
using System.Collections;
using System;

[AddComponentMenu("AnimCol/Misc/MovingPlatform")]
public class MovingPlatform : MonoBehaviour {
    [Serializable]
    public class AutoRebuild {
        [Tooltip("Enable this, and your platform will re-assemble itself after collapsing, yet only after your specified amount of time.")]
        public bool shouldReassembleAfter;
        [Tooltip("The time to wait before re-assembling.")]
        public float delay = 3.0f;
        [Tooltip("The time it should take for the bridge to transition from fallen to working again. This occurs AFTER the delay.")]
        public float rebuildTime = 3.0f;
    }

    [Tooltip("Motion will be defined by the properties below, un-check this to use animation instead.")]
    public bool automaticMotion = true;
    [Tooltip("The time in seconds that it will take to complete a cycle of motion.")]
    public float duration = 1.0f;
    [Tooltip("The distance from the point of origin that the object should move.")]
    public Vector2 distance = new Vector2(1.0f, 0.0f);
    [Tooltip("Adjust the phase of the sine wave that drives the animation.")]
    [Range(0.0f, 1.0f)]
    public float phase;
    [Tooltip("Use a random phase instead.")]
    public bool randomPhase;
    [Tooltip("This script is intended to be used for platforms, but disable this if this behaviour is not wanted.")]
    public bool isPlatform = true;
    [Header("Falling Platform")]
    [Tooltip("Whether or not the platform should fall after a specified amount of time when the player stands on it.")]
    public bool isFallingPlatform = false;
    [Tooltip("Sometimes the collider makes a falling platform slightly awkward. Enable this, and the collider will be disabled when falling.")]
    public bool disableCollider = true;
    [Tooltip("The platform can reassemble itself afterwards, if you like. Set the behaviour here.")]
    public AutoRebuild autoRebuild;
    [Tooltip("The amount of time that the player is allowed to stand on this platform before it falls.")]
    public float fallTimer = 3.0f;
    [Tooltip("By default, the platform will fall according to the size of the collider. This is not always sufficient, hence some additional force can be applied here.")]
    public float downwardForce = 2.0f;

    private float truePhase;
    private float progress;
    private float animTime;
    private Vector2 startPosition;
    private Animator animator;
    private Coroutine beginCountdown;
    private Rigidbody2D rb2d;
    private Collider2D col2d;

    void Start() {
        gameObject.layer = LayerMask.NameToLayer("Ground");
        startPosition = transform.position;
        InitializeSinMotion();

        animator = GetComponent<Animator>();
        col2d = GetComponent<Collider2D>();
    }

    void Update() {
        if (automaticMotion) {
            progress += Time.deltaTime;

            transform.position = startPosition + new Vector2 (
                GetMotion(ref distance.x)
            ,   GetMotion(ref distance.y)
            );
        }
    }

    private float GetMotion(ref float val) {
        return Mathf.Sin((progress * (Mathf.PI / duration) + truePhase *2)) * val;
    }

    private void InitializeSinMotion() {
        truePhase = randomPhase ? UnityEngine.Random.Range(0.0f, 1.0f) : phase;
        truePhase *= Mathf.PI;
    }

    void OnCollisionEnter2D (Collision2D other){
        if (other.gameObject.tag == "Player" && isPlatform) {
            other.transform.parent = gameObject.transform;

            if (beginCountdown == null && isFallingPlatform) {
                beginCountdown = StartCoroutine(BeginCountdown());
            }
        }
    }
    
    void OnCollisionExit2D (Collision2D other){
        if (other.gameObject.tag == "Player" && isPlatform) {
            other.transform.parent = null;
        }
    }

    private IEnumerator BeginCountdown() {
        yield return new WaitForSeconds(fallTimer);
        bool automated = automaticMotion;

        yield return StartCoroutine(BeginFalling(automated));
        yield return StartCoroutine(Rebuild(automated));

        yield return null;
    }

    private IEnumerator BeginFalling(bool automated) {
        if (automated) {
            automaticMotion = false;
        } else {
            startPosition = transform.position;
            animTime = animator.GetCurrentAnimatorStateInfo(0).normalizedTime;
            animator.enabled = false;
        }

        GameManager.Generic.player.transform.parent = null;
        col2d.enabled = !disableCollider;
        rb2d = gameObject.AddComponent<Rigidbody2D>();
        rb2d.AddForce(Vector2.down * downwardForce, ForceMode2D.Impulse);

        yield return null;
    }

    private IEnumerator Rebuild(bool automated) {
        if (autoRebuild.shouldReassembleAfter) {
            yield return new WaitForSeconds(autoRebuild.delay);
            Destroy(rb2d);

            // Bring the platform back to it's original location.
            float t = 0.0f;
            Vector2 pos = transform.position;

            while (t < autoRebuild.rebuildTime) {
                transform.position = Vector2.Lerp(
                    pos,
                    startPosition,
                    t / autoRebuild.rebuildTime
                );

                t += Time.deltaTime;
                yield return null;
            }

            transform.position = startPosition;

            // Restore it's motion.
            if (automated) {
                automaticMotion = true;
            } else {
                animator.enabled = true;
                AnimatorStateInfo stateInfo = animator.GetCurrentAnimatorStateInfo(0);
                animator.Play(stateInfo.fullPathHash, 0, animTime);
            }

            col2d.enabled = true;

            // Make all this possible again.
            beginCountdown = null;
        }
    }
}
