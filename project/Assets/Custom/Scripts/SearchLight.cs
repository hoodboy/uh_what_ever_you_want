﻿using UnityEngine;
using System.Collections;
using GameManager;

[ExecuteInEditMode]
[AddComponentMenu("AnimCol/Misc/SearchLight")]
public class SearchLight : MonoBehaviour {
    [Header("Events")]
    [Tooltip("A series of lights to toggle on or off when the event is triggered. Lights that are off will be turned on, and vise-versa.")]
    public Light[] lights;
    [Tooltip("A series of particle systems to play or stop when the event is triggered. Particle Systems not currently playing will be triggered, particle systems that ARE currently playing will be stopped.")]
    public ParticleSystem[] particleSystems;
    [Tooltip("A series of animators that will have their 'active' booleans toggled.")]
    public Animator[] animators;

    [Header("Search Light")]
    [Tooltip("Offset the origin's Y position.")]
    public float originOffset;
    [Tooltip("Amount of cone divisions. Essentially, increase this number for a more accurate search-light, be aware that adding more rays will have a performance cost... though the cost appears to be negligible... but be aware of it nonetheless.")]
    public int searchSamplesDivisions = 10;
    [Tooltip("The distance in height from the point of origin. This essentially describes how far the search-light can search.")]
    public float searchDistance = 7.0f;
    [Tooltip("The width of the cone formed from all the ray-casts. A wider spot-light requires a higher search width.")]
    public float searchWidth = 7.0f;
    [Tooltip("Search lights toggle a series of events once the player has been detected. After a delay in seconds, it will toggle everything back to how it was before.")]
    public float alertDuration = 7.0f;
    [Tooltip("Sometimes you don't want all the events to trigger right away. You can delay this occurance in seconds by using this value. This delay completes, events are triggers, and only after that does the alertDuration property apply.")]
    public float alertDelay = 0.0f;

    private bool isAlert;
    private float spacing;

    void Update () {
        Search();
    }

    private void Search() {
        if (!isAlert) {
            spacing = (searchWidth * 2) / searchSamplesDivisions;

            for (int i = 0; i <= searchSamplesDivisions; i++) {
                RaycastHit2D hit = Physics2D.Raycast(
                    (Vector2)transform.position + ((Vector2)transform.up * originOffset)
                ,   -(Vector2)transform.up * searchDistance + ((-(Vector2)transform.right * searchWidth)) + ((Vector2)transform.right * (spacing * i))
                ,   searchDistance);

                if (hit) {
#if UNITY_EDITOR
                        Debug.DrawLine(
                        (Vector2)transform.position + ((Vector2)transform.up * originOffset)
                    ,   hit.point
                    ,   Color.red);
#endif
                    if (hit.transform.tag == "Player") {
                        StartCoroutine(IntruderAlert());
                        break;
                    }
                } else {
#if UNITY_EDITOR
                Debug.DrawRay(
                    (Vector2)transform.position + ((Vector2)transform.up * originOffset)
                ,   -(Vector2)transform.up * searchDistance + ((-(Vector2)transform.right * searchWidth)) + ((Vector2)transform.right * (spacing * i))
                ,   Color.red);
#endif
                }
            }
        }
    }

    private IEnumerator IntruderAlert() {
        if (Application.isPlaying) {
            isAlert = true;

            yield return new WaitForSeconds(alertDelay);

            ToggleItems();

            yield return new WaitForSeconds(alertDuration);

            ToggleItems();
            isAlert = false;
        }
    }

    private void ToggleItems () {
        Generic.ToggleLights(lights);
        Generic.ToggleParticleSystems(particleSystems);
        Generic.ToggleAnimators(animators);
    }
}
