﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using GameManager;

namespace Init {
    [AddComponentMenu("AnimCol/SceneManagers/InitializerSequential")]
    [RequireComponent(typeof(AudioSource))]
    [RequireComponent(typeof(CoroutineCompanion))]
    [RequireComponent(typeof(EventManager))]
    [RequireComponent(typeof(Updater))]
    public class InitializerSequential : MonoBehaviour {
        [Tooltip("A collection of properties concerning the Pause menu.")]
        public PauseMenu pauseMenu;
        [Tooltip("A collection of properties concerning the score gauge and life-meter.")]
        public Hud hud;
        [Tooltip("A collection of properties concerning the option where fall-death is calculated by the amount of time spent falling.")]
        public DialogueProperties dialogue;
        [Tooltip("A collection of properties concerning the pop-up dialogue UI.")]
        public PopUp popUp;
        [Tooltip("A collection of properties with general effects on the game.")]
        public Misc misc;

        private List<Image> lifeIcons = new List<Image>();
        private float cutSceneTransitionDuration = 0.5f;

        void Awake () {
            // Just in-case somebody turned off their canvas.
            if (misc.mainCanvas) {
                misc.mainCanvas.gameObject.SetActive(true);
            }

            // Cameras.
            CameraSetUp();

            // Player
            Generic.player = FindObjectOfType<Player>();
            Generic.playerspriteRenderer = Generic.player.GetComponent<SpriteRenderer>();

            // Lives.
            if (Generic.lifeSystem && hud.lifeGaugeParent) {
                foreach (Transform t in hud.lifeGaugeParent) {
                    lifeIcons.Add(t.GetChild(0).GetComponent<Image>());
                }

                Generic.lifeIcons = lifeIcons.Count > 0 ? lifeIcons : null;
            } else if (hud.lifeGaugeParent) {
                Destroy(hud.lifeGaugeParent.gameObject);
            }

            // Health.
            if (Generic.health > 0) {
                Generic.healthGauge = hud.healthGauge ? hud.healthGauge : null;
                Generic.healthGaugeWidth = hud.healthGauge ? hud.healthGauge.rect.width : 0;
            } else if (hud.healthGauge) {
                Destroy(hud.healthGauge.transform.parent.gameObject);
            }

            // Score.
            if (Generic.scoreSystem) {
                Generic.targetScore += CalculateTargetScore();
                Generic.scoreGauge = hud.scoreGauge ? hud.scoreGauge : null;
                Generic.scoreGaugeAnimator = hud.scoreGaugeAnimator ? hud.scoreGaugeAnimator : null;
                Generic.scoreGaugeWidth = hud.scoreGauge ? hud.scoreGauge.rect.width : 0;
            } else if (hud.scoreGaugeAnimator) {
                Destroy(hud.scoreGaugeAnimator.gameObject);
            }

            // Dialogue.
            Generic.dialogueAnimator    = dialogue.dialogueAnimator    ? dialogue.dialogueAnimator    : null;
            Generic.dialogueAudioSource = dialogue.dialogueAnimator    ? dialogue.dialogueAnimator.gameObject.AddComponent<AudioSource>() : null;
            Generic.dialogueText        = dialogue.dialogueText        ? dialogue.dialogueText        : null;
            Generic.dialogueAvatarImage = dialogue.dialogueAvatarImage ? dialogue.dialogueAvatarImage : null;
            Generic.dialogueAvatarName  = dialogue.dialogueAvatarName  ? dialogue.dialogueAvatarName  : null;
            Generic.choiceBox           = dialogue.choiceBox           ? dialogue.choiceBox           : null;
            Generic.choiceButtonA       = dialogue.choiceButtonA       ? dialogue.choiceButtonA       : null;
            Generic.choiceButtonB       = dialogue.choiceButtonB       ? dialogue.choiceButtonB       : null;

            // PopUpDialogue.
            Generic.popUp = popUp.popUp ? popUp.popUp : null;

            if (popUp.popUp) {
                Generic.popUpCanvas = popUp.popUp.transform.root.GetComponent<Canvas>() ? popUp.popUp.transform.root.GetComponent<RectTransform>() : null;
                Generic.popUpText = popUp.popUpText ? popUp.popUpText : null;
                Generic.popUpTextDummy = popUp.popUpText && popUp.popUpText.rectTransform.GetChild(0).GetComponent<Text>() ? popUp.popUpText.rectTransform.GetChild(0).GetComponent<Text>() : null;

                if (popUp.popUpText) {
                    Generic.popUpTextDummy.font = popUp.popUpText.font;
                    Generic.popUpTextDummy.fontSize = popUp.popUpText.fontSize;
                }

                Generic.popUpOffset = popUp.offset;
            }

            // Pause.
            Generic.pauseMenu = pauseMenu.pauseMenu ? pauseMenu.pauseMenu : null;
            Generic.bgMusic = misc.bgMusic ? misc.bgMusic : null;
            Generic.defaultPauseButton = pauseMenu.pauseMenuButtons.resumeButton ? pauseMenu.pauseMenuButtons.resumeButton : null;
            if (Generic.defaultPauseButton) {
                pauseMenu.pauseMenuButtons.resumeButton.onClick.RemoveAllListeners();
                pauseMenu.pauseMenuButtons.resumeButton.onClick.AddListener(delegate {  UIEvents.TogglePause(); });
            }
            if (pauseMenu.pauseMenuButtons.quitButton) {
                pauseMenu.pauseMenuButtons.quitButton.onClick.RemoveAllListeners();
                pauseMenu.pauseMenuButtons.quitButton.onClick.AddListener(delegate {    UIEvents.ReturnToStartScreen(); });
            }

            // Misc.
            Generic.spawnPoint = misc.initialSpawnPoint ? misc.initialSpawnPoint : null;
            Generic.sfx = GetComponent<AudioSource>();
            Generic.cutSceneTransitionDuration = cutSceneTransitionDuration * 0.5f;
            Generic.coroutineCompanion = GetComponent<CoroutineCompanion>();
            Generic.SetObjectsToReset(misc.objectsToReset);
            Generic.eventManager = GetComponent<EventManager>();

            Log();
        }

        void Start() {
            Generic.UpdateLives(0);
            Generic.UpdateScore (0);
            Generic.UpdateHealth (0);
            Generic.isControllable = true;
            StartCoroutine(Generic.SceneFadeIn());
        }

        private int CalculateTargetScore() {
            PuzzleRotation[] puzzleRotations = (PuzzleRotation[])FindObjectsOfType(typeof(PuzzleRotation));
            EventsTrigger[] eventTriggers = (EventsTrigger[])FindObjectsOfType(typeof(EventsTrigger));
            EnemySpawnPoint[] enemySpawnPoints = (EnemySpawnPoint[])FindObjectsOfType(typeof(EnemySpawnPoint));
            PickUp[] pickups = (PickUp[])FindObjectsOfType(typeof(PickUp));

            int tally = new int();

            // Calculate score from Pickup objects.
            foreach (PickUp p in pickups) {
                tally += p.score;
            }

            // Extract score from PuzzleRotation objects.
            if (puzzleRotations != null) {
                foreach (PuzzleRotation pr in puzzleRotations) {
                    foreach (Events.ObjectsToSpawn ots in pr.events.objectsToSpawn) {
                        if (ots.objectToSpawn && ots.objectToSpawn.GetComponent<PickUp>()) {
                            tally += ots.objectToSpawn.GetComponent<PickUp>().score;
                        }
                    }

                    // Remember to search thr cutscenes as well.
                    foreach (Events.CutScenes cs in pr.cutScenes) {
                        foreach (Events.ObjectsToSpawn ots in cs.advancedEvents.objectsToSpawn)
                        if (ots.objectToSpawn.GetComponent<PickUp>()) {
                            tally += ots.objectToSpawn.GetComponent<PickUp>().score;
                        }
                    }
                }
            }

            // Extract score from EventTrigger objects.
            foreach (EventsTrigger et in eventTriggers) {
                foreach (Events.ObjectsToSpawn ots in et.events.objectsToSpawn) {
                    if (ots.objectToSpawn.GetComponent<PickUp>()) {
                        tally += ots.objectToSpawn.GetComponent<PickUp>().score;
                    }
                }

                // Remember to search thr cutscenes as well.
                foreach (Events.CutScenes cs in et.cutScenes) {
                    foreach (Events.ObjectsToSpawn ots in cs.advancedEvents.objectsToSpawn)
                    if (ots.objectToSpawn && ots.objectToSpawn.GetComponent<PickUp>()) {
                        tally += ots.objectToSpawn.GetComponent<PickUp>().score;
                    }
                }
            }

            // Extract from enemies.
            foreach (EnemySpawnPoint esp in enemySpawnPoints) {
                tally += esp.enemy.whenEnemyDies.pickupToSpawn.score;
            }

            return tally;
        }

        private void CameraSetUp() {
            foreach (Camera cam in FindObjectsOfType<Camera>()) {
                if (cam == Camera.main) {
                    cam.enabled = true;
                    if (cam.GetComponent<AudioListener>()) {
                        cam.GetComponent<AudioListener>().enabled = true;
                    }
                } else {
                    cam.enabled = false;
                    if (cam.GetComponent<AudioListener>()) {
                        cam.GetComponent<AudioListener>().enabled = false;
                    }
                }
            }
        }

        private void Log() {
            if (!misc.mainCanvas) Debug.Log(gameObject.name + ": The main canvas has not been applied to misc.mainCanvas.");
            if (!Generic.player)        Debug.Log(gameObject.name + ": No object tagged as 'Player' was found in the scene.");
            if (Generic.lifeSystem && !hud.lifeGaugeParent)   Debug.Log(gameObject.name + ": 'lifeGaugeParent' has not been assigned.");
            if (Generic.lifeSystem && !(lifeIcons.Count > 0)) Debug.Log(gameObject.name + ": No 'lifeIcons' with Image componenent were found beneath the 'lifeGaugeParent'.");

            if (Generic.scoreSystem && !hud.scoreGauge)         Debug.Log(gameObject.name + ": The 'scoreGauge' has not been assigned.");
            if (Generic.scoreSystem && !hud.scoreGaugeAnimator) Debug.Log(gameObject.name + ": The 'scoreGaugeAnimator' has not been assigned.");
            if (!dialogue.dialogueAnimator)    Debug.Log(gameObject.name + ": The 'dialogueAnimator' has not been assigned.");
            if (!dialogue.dialogueText)        Debug.Log(gameObject.name + ": The 'dialogueText' has not been assigned.");
            if (!dialogue.dialogueAvatarImage) Debug.Log(gameObject.name + ": The 'dialogueAvatarImage' has not been assigned.");
            if (!dialogue.choiceBox)           Debug.Log(gameObject.name + ": The 'choiceBox' has not been assigned.");
            if (!dialogue.choiceButtonA)       Debug.Log(gameObject.name + ": The 'choiceButtonA' has not been assigned.");
            if (!dialogue.choiceButtonB)       Debug.Log(gameObject.name + ": The 'choiceButtonB' has not been assigned.");

            if (!popUp.popUp)                                       Debug.Log(gameObject.name + ": The 'popUp' has not been assigned.");
            if (!popUp.popUp.transform.root.GetComponent<Canvas>()) Debug.Log(gameObject.name + ": The root parent of the 'popUpText' needs to be a UI Canvas. One possible cause could be that you have your 'popUp' parented to something odd, or that you have parented the canvas to some other object. The Canvas should not have a parent.");
            if (!popUp.popUpText)                                   Debug.Log (gameObject.name + ": The 'popUpText' has not been assigned.");

            if (!pauseMenu.pauseMenu)                     Debug.Log(gameObject.name + ": No 'pauseMenu' has been assigned.");
            if (!pauseMenu.pauseMenuButtons.resumeButton) Debug.Log(gameObject.name + ": No 'resumeButton' has been assigned.");
            if (!pauseMenu.pauseMenuButtons.quitButton)   Debug.Log(gameObject.name + ": No 'quitButton' has been assigned.");

            if (!misc.initialSpawnPoint) Debug.Log(gameObject.name + ": 'initialSpawnPoint' has not been set. World Position (x0, y0, z0) will be used as the initial respawn point if the character dies.");
        }
    }
}