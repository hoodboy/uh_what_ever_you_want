﻿using UnityEngine;
using UnityEngine.UI;

[AddComponentMenu("AnimCol/UI/HighScoreTableItem")]
public class HighScoreTableItem : MonoBehaviour {
    [Tooltip("The Text element that displays the name of the one who holds this high score.")]
    public Text nameText;
    [Tooltip("The Text element that displays this particular high score.")]
    public Text scoreText;
}
