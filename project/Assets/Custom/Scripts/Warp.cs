﻿using UnityEngine;
using GameManager;
using System;
using System.Collections;

[AddComponentMenu("AnimCol/Misc/Warp")]
public class Warp : MonoBehaviour {
    [Serializable]
    public class EffectPair {
        [Tooltip("An effect prefab that is spawned either on exit or entry.")]
        public ParticleSystem particleSystem;
        [Tooltip("Sometimes the pivot of the warp device is not the ideal position to spawn a particleSystem. This value allows you to offset the spawn position.")]
        public Vector2 spawnOffset;
        [Tooltip("Add a sound to play when warping commences.")]
        public AudioClip sfx;
    }

    [Tooltip("The other warp device that you're taken to when you enter this warp device.")]
    public Warp recipientWarp;
    [Tooltip("The amount of time it takes to travel from this warp device, to the recipient warp device.")]
    public float warpDuration = 0.5f;
    [Tooltip("Whether or not this warp device is available. Please note that this is toggled when you enter and exit warp devices.")]
    public bool isAvailable = true;
    [Tooltip("This warp device will remain unavailable if you exit a warp transition from it.")]
    public bool exitAndClose = false;
    [Tooltip("This warp device will remain unavailable if you enter a warp transition from it.")]
    public bool enterAndClose = false;
    [Tooltip("Enable this if you only want warping to commence at a certain point along an animation timeline. If you enable this, please note that the 'EnterWarp' and 'ExitWarp' events must be added to your animations or else this warp will freeze your game.")]
    public bool waitForAnimation = false;
    [Tooltip("The 'Spacebar' must be pressed if you wish to enter this warp.")]
    public bool isButtonActivated = false;
    [Tooltip("If this warp device uses particleSystems to define it visually, it is recommended to have the 'active' version parented beneath this gameObject, and applied to this slot.")]
    public ParticleSystem activeEffect;
    [Tooltip("If this warp device uses particleSystems to define it visually, it is recommended to have the 'inactive' version parented beneath this gameObject, and applied to this slot.")]
    public ParticleSystem inactiveEffect;
    [Tooltip("Settings for the particleSystem prefab that spawns when entering this warp device.")]
    public EffectPair entryEffects;
    [Tooltip("Settings for the particleSystem prefab that spawns when exiting this warp device.")]
    public EffectPair exitEffects;

    private Animator anim;
    private SpriteRenderer playerSpriteRenderer;
    private Rigidbody2D playerRb2D;
    private Collider2D playerCol2D;
    private float playerOriginalGravityScale;
    private Warp incomingWarp;

    void Start () {
        GetComponent<Collider2D>().isTrigger = true;
        anim = GetComponent<Animator>();

        playerSpriteRenderer = Generic.player.GetComponent<SpriteRenderer>();
        playerRb2D = Generic.player.GetComponent<Rigidbody2D>();
        playerCol2D = Generic.player.GetComponent<Collider2D>();
        playerOriginalGravityScale = playerRb2D.gravityScale;

        ToggleParticleSystems(isAvailable);
    }

    void LateUpdate() {
        if (anim && isAvailable != anim.GetBool("active")) {
            anim.SetBool("active", isAvailable);
        }
    }

    void OnTriggerEnter2D(Collider2D other) {
        if (other.tag == "Player" 
        && isAvailable 
        && recipientWarp 
        && !isButtonActivated) {
            InitializeWarp();
        }
    }

    void OnTriggerStay2D(Collider2D other) {
        if (other.tag == "Player" 
        && isAvailable 
        && recipientWarp 
        && isButtonActivated
        && Input.GetButtonDown("Jump")) {
            InitializeWarp();
        }
    }

    void OnTriggerExit2D(Collider2D other) {
        if (other.tag == "Player" && incomingWarp) {
            ToggleParticleSystems(!exitAndClose);
            isAvailable = !exitAndClose;

            // Incoming warp.
            incomingWarp.isAvailable = !incomingWarp.enterAndClose;

            if (incomingWarp.activeEffect && incomingWarp.inactiveEffect && !incomingWarp.enterAndClose) {
                incomingWarp.ToggleParticleSystems(true);
            }

            incomingWarp = null;
        }
    }

    private void InitializeWarp() {
        Generic.isControllable = false;

        if (waitForAnimation) {
            anim.SetTrigger("enter");
        } else {
            EnterWarp();
        }
    }

    public void EnterWarp() {
        PlayEffects(entryEffects);

        ToggleParticleSystems(false);

        // Player.
        playerSpriteRenderer.enabled = false;
        playerCol2D.enabled = false;
        playerRb2D.isKinematic = true;
        playerRb2D.gravityScale = 0.0f;

        // This warp.
        isAvailable = false;

        // Recipient Warp.
        recipientWarp.incomingWarp = this;
        recipientWarp.isAvailable = false;

        StartCoroutine(Travel());
    }

    private IEnumerator Travel() {
        float t = 0.0f;
        Vector2 currentPlayerPos = Generic.player.transform.position;

        while (t < warpDuration) {
            Generic.player.transform.position = Vector2.Lerp(
                currentPlayerPos,
                recipientWarp.transform.position,
                t / warpDuration
            );

            t += Time.deltaTime;
            yield return null;
        }

        Generic.player.transform.position = recipientWarp.transform.position;

        if (waitForAnimation) {
            recipientWarp.anim.SetTrigger("exit");
        } else {
            recipientWarp.ExitWarp();
        }
    }

    public void ExitWarp() {
        PlayEffects(exitEffects);

        ToggleParticleSystems(false);

        // Player.
        playerSpriteRenderer.enabled = true;
        playerCol2D.enabled = true;
        playerRb2D.isKinematic = false;
        playerRb2D.gravityScale = playerOriginalGravityScale;

        Generic.isControllable = true;
    }

    private void PlayEffects(EffectPair ep) {
        if (ep.particleSystem) {
            Instantiate(ep.particleSystem, (Vector2)transform.position + ep.spawnOffset, Quaternion.identity);
        }

        if (ep.sfx) {
            AudioClip[] audioClips = { ep.sfx };
            Generic.PlaySFX(audioClips);
        }
    }

    private void ToggleParticleSystems(bool enabled) {
        ParticleSystem.EmissionModule active = activeEffect.emission;
        ParticleSystem.EmissionModule inactive = inactiveEffect.emission;
        active.enabled = enabled;
        inactive.enabled = !enabled;
    }
}
