﻿using UnityEngine;
using System;

public class Bouncy : MonoBehaviour {
    public float bouncePower = 5;
    public float bounceIncrement = 5;
    public float bouncePowerLimit;

    [NonSerialized]
    public float originalBouncePower;

    void Start (){
        originalBouncePower = bouncePower;
        gameObject.tag = "Bouncy";
    }
}
