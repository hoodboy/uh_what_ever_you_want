﻿using UnityEngine;
using System;
using System.Collections;
using GameManager;
#if UNITY_EDITOR
using UnityEditor;
#endif

#if UNITY_EDITOR
[InitializeOnLoad]
#endif
[ExecuteInEditMode]
[AddComponentMenu("AnimCol/Character/NPC")]
[RequireComponent(typeof(SpriteRenderer))]
[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(Collider2D))]
[RequireComponent(typeof(Animator))]
public class NPC : MonoBehaviour {
    public enum Mode {
        idle,
        patrol,
        talking
    }

    [Serializable]
    public struct PatrolBoundaries {
        public Vector2 min;
        public Vector2 max;
    }

    [Tooltip("Enemies will patrol left and right, set the speed at which they move.")]
    public float speed = 1.0f;
    [Tooltip("Min and Max X values within whcih this NPC can walk.")]
    public Vector2 minMaxPatrol = new Vector2(-5, 5);
    [Tooltip("Enable this to allow this NPC to patrol left and right within their minMaxPatrol bounds.")]
    public bool canPatrol;
    [Tooltip("How close the player must be to speak to this NPC.")]
    public float playerProximityCheck = 5.0f;
    [Tooltip("How close this NPC needs to be to the player in order to be spoken to.")]
    public float speakProximity = 1.0f;
    [Tooltip("A series of dialogues to play when speaking to this NPC.")]
    public Events.Dialogue[] dialogues;

    private bool playerIsInRange;
    private bool isTalkingAlready;
    private PatrolBoundaries patrolBoundaries;
    private Rigidbody2D rb2d;
    private Vector2 targetPosition;
    private SpriteRenderer spriteRenderer;
    private Animator anim;

    void Awake() {
        rb2d = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
        spriteRenderer = GetComponent<SpriteRenderer>();
    }

    void Start () {
        if (Application.isPlaying) {
            patrolBoundaries.min = new Vector2(transform.position.x + minMaxPatrol.x, transform.position.y);
            patrolBoundaries.max = new Vector2(transform.position.x + minMaxPatrol.y, transform.position.y);
            StartCoroutine(MainLoop());
        }
    }

    void Update() {
        if (Application.isPlaying) {
            playerIsInRange = PlayerInRange();
            anim.SetFloat("FacingDirection", IsFacingLeft() ? -1.0f : 1.0f);
        } else {
            patrolBoundaries.min = new Vector2(transform.position.x + minMaxPatrol.x, transform.position.y);
            patrolBoundaries.max = new Vector2(transform.position.x + minMaxPatrol.y, transform.position.y);
        }

        DebugRayCast();
    }

    private bool IsFacingLeft() {
        return targetPosition.x < transform.position.x;
    }

    private IEnumerator MainLoop() {
        while (true) {
            if (UnityEngine.Random.Range(0, 2) == 0 || playerIsInRange) {
                yield return StartCoroutine(Idle());
            } else if (canPatrol) {
                yield return StartCoroutine(Patrol());
            }
        }
    }

    private IEnumerator Idle() {
        anim.SetInteger("mode", 0);

        float timer = UnityEngine.Random.Range(1.0f, 3.0f);
        while (timer > 0.0f && playerIsInRange) {
            timer -= Time.deltaTime;

            if (Input.GetButtonDown("Submit") && !isTalkingAlready) {
                yield return StartCoroutine(Speech());
            }

            anim.SetInteger("mode", (int)Mode.idle);

            yield return null;
        }
    }

    private IEnumerator Speech() {
        isTalkingAlready = true;
        anim.SetInteger("mode", (int)Mode.talking);

        Generic.isControllable = false;
        foreach (Events.Dialogue dialogue in dialogues) {
            yield return StartCoroutine(Generic.DisplayDialogue (dialogue));
        }
        Generic.isControllable = true;

        isTalkingAlready = false;
    }

    private IEnumerator Patrol() {
        anim.SetInteger("mode", (int)Mode.patrol);

        // Get new patrol position.
        targetPosition = (int)UnityEngine.Random.Range(0, 2) == (int)0 ? patrolBoundaries.min : patrolBoundaries.max;

        float randomCountdown = UnityEngine.Random.Range(3, 8);
        while (Vector3.Distance(targetPosition, transform.position) >= 1
        && randomCountdown > 0.0f && anim.GetInteger("mode") == (int)Mode.patrol
        && !playerIsInRange) {
            rb2d.velocity = new Vector2(anim.GetFloat("FacingDirection") * speed, rb2d.velocity.y);
            randomCountdown -= Time.deltaTime;

            yield return null;
        }

        rb2d.velocity = Vector2.zero;
    }

    private bool PlayerInRange() {
        return Vector2.Distance(transform.position, Generic.player.transform.position) < playerProximityCheck;
    }

    private void DebugRayCast() {
        Vector2 topOfSprite = new Vector2(transform.position.x, spriteRenderer.bounds.max.y);
        Debug.DrawLine(topOfSprite, patrolBoundaries.min, Color.green);
        Debug.DrawLine(topOfSprite, patrolBoundaries.max, Color.blue);

        if (playerIsInRange) {
            Debug.DrawLine(topOfSprite, Generic.player.transform.position, Color.magenta);
        }
    }
}
