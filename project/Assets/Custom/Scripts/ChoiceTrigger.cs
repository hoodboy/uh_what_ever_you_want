﻿using UnityEngine;
using UnityEngine.UI;
using GameManager;
using System;
using System.Collections;

[AddComponentMenu("AnimCol/Events/ChoiceTrigger")]
public class ChoiceTrigger : MonoBehaviour {
    [Serializable]
    public class PossibleEvents {
        [Tooltip("The 'Events' object that's triggered if you select option 'A'. Leaving this blank will cancel the selection dialogue and allow it to be triggerable again.")]
        public Events a;
        [Tooltip("The 'Events' object that's triggered if you select option 'B'. Leaving this blank will cancel the selection dialogue and allow it to be triggerable again.")]
        public Events b;
    }

    [Serializable]
    public class Choices {
        [Tooltip("The text that describes option 'A'.")]
        public string choiceA;
        [Tooltip("The text that describes option 'A'.")]
        public string choiceB;
    }

    public enum Selection {
        A,
        B
    }

    [Header("Choice parameters")]
    [Tooltip("The dialogue that accomodates your options.")]
    public Events.Dialogue dialogue;
    [Tooltip("The text that describes your options to choose from.")]
    public Choices choices;
    [Tooltip("The events that can be triggered based on which option is chosen. Leaving either of these empty will simply cancel the dialogue and allow it to be triggerable again.")]
    public PossibleEvents possibleEvents;
    [Tooltip("Whether or not the 'Submit' button needs to be pressed while inside the trigger-collider in order to trigger the events.")]
    public bool isButtonActivated;

    private bool hasFired;
    private Selection selection = Selection.A;

    private void Start() {
        Generic.CheckForCollider(gameObject);
    }

    private void OnTriggerEnter2D(Collider2D other) {
        if (!hasFired 
        &&  other.tag == "Player" 
        &&  !isButtonActivated) {
            hasFired = true;
            StartCoroutine(ProvideChoice());
        }
    }

    private void OnTriggerStay2D(Collider2D other) {
        if (!hasFired 
        &&  other.tag == "Player" 
        &&  isButtonActivated 
        &&  Input.GetButtonDown("Submit")) {
            hasFired = true;
            StartCoroutine(ProvideChoice());
        }
    }

    private IEnumerator ProvideChoice() {
        // inform the designer if they've forgotten to add text.
        string dummyText = "Error";
        if (choices.choiceA.Length == 0) {
            choices.choiceA = dummyText;
        }
        if (choices.choiceB.Length == 0) {
            choices.choiceB = dummyText;
        }

        Generic.isControllable = false;

        ButtonSetUpSelection(Generic.choiceButtonA, SetOutcome, Selection.A);
        Generic.choiceButtonA.transform.GetChild(0).GetComponent<Text>().text = choices.choiceA;
        ButtonSetUpSelection(Generic.choiceButtonB, SetOutcome, Selection.B);
        Generic.choiceButtonB.transform.GetChild(0).GetComponent<Text>().text = choices.choiceB;

        Generic.choiceBox.gameObject.SetActive(true);
        Generic.choiceBox.GetComponent<Image>().enabled = false;
        dialogue.isAwaitingChoice = true;

        yield return StartCoroutine(Generic.DisplayDialogue(dialogue));

        Generic.choiceBox.gameObject.SetActive(false);

        if (selection == Selection.A && possibleEvents.a) {
            StartCoroutine(possibleEvents.a.TriggerEvents(false, null));
        } else if (possibleEvents.b) {
            StartCoroutine(possibleEvents.b.TriggerEvents(false, null));
        }
    }

    private void SetOutcome(Selection sel) {
        selection = sel;
        dialogue.isAwaitingChoice = false;
    }

    public void ButtonSetUpSelection(Button button, Action<Selection> method, Selection selection) {
        button.onClick.RemoveAllListeners();
        button.onClick.AddListener(() => method(selection));
    }
}
