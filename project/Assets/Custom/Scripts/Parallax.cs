﻿using UnityEngine;
using System;
using System.Collections.Generic;

[AddComponentMenu("AnimCol/Misc/Parallax")]
public class Parallax : MonoBehaviour {
    [Serializable]
    public class ParallaxSet {
        [Tooltip("The precise name of the sorting layer that we plan to treat as parallax objects.")]
        public string sortingLayer;
        [Tooltip("This multiplies the speed at which all the parallax objects travel.")]
        public float speed = 1;
        [Tooltip("Don't bother adding anything to this. It's just here because I wasn't able to hide it. It auto-populates itself anyway.")]
        public List<Transform> doNotTouch;
    }

    [Tooltip("Min / Max X positions allowed. Any item that positions itself beyond min X will be sent to the max X position, and vise-versa.")]
    public Vector2 minMaxX = new Vector2(-100.0f, 100.0f);
    [Tooltip("Include however many layers of parallax as you please.")]
    public ParallaxSet[] parallaxSets;

    private float lastPlayerXPos;

    void Awake() {
        if (parallaxSets != null && parallaxSets.Length > 0) {
            foreach (ParallaxSet ps in parallaxSets) {
                List<Transform> transforms = FindTransformsInSortingLayer(ps.sortingLayer);
                if (transforms != null && transforms.Count > 0) {
                    foreach (Transform t in transforms) {
                        ps.doNotTouch.Add(t);

                        minMaxX.x = t.position.x < minMaxX.x ? t.position.x : minMaxX.x;
                        minMaxX.y = t.position.x > minMaxX.y ? t.position.x : minMaxX.y;
                    }
                }
            }
        }
    }

    void Update() {
        float playerPosXDelta = GameManager.Generic.player.transform.position.x - lastPlayerXPos;
        if (parallaxSets != null && parallaxSets.Length > 0) {
            foreach (ParallaxSet ps in parallaxSets) {
                if (ps.doNotTouch != null && ps.doNotTouch.Count > 0) {
                    foreach (Transform t in ps.doNotTouch) {
                        float newXPos = playerPosXDelta * (Time.deltaTime * ps.speed);
                        t.position -= new Vector3(newXPos, 0, 0);

                        if (t.position.x < minMaxX.x) {
                            t.position = new Vector3(minMaxX.y, t.position.y);
                        } else if (t.position.x > minMaxX.y) {
                            t.position = new Vector3(minMaxX.x, t.position.y);
                        }
                    }
                }
            }
        }

        lastPlayerXPos = GameManager.Generic.player.transform.position.x;
    }

    List<Transform> FindTransformsInSortingLayer(string layer) {
        SpriteRenderer[] arrSR = FindObjectsOfType<SpriteRenderer>();
        List<Transform> tList = new List<Transform>();

        foreach (SpriteRenderer sr in arrSR) {
            if (sr.sortingLayerName == layer) {
                tList.Add(sr.transform);
            }
        }

        return tList.Count > 0 ? tList : null;
    }
}