﻿using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

#if UNITY_EDITOR
[InitializeOnLoad]
#endif
[ExecuteInEditMode]
[AddComponentMenu("AnimCol/Character/PlayerRaycastDebug")]
public class PlayerRaycastDebug : MonoBehaviour {
    private Player player;
    private SpriteRenderer playerSpriteRenderer;

    void Start() {
        player = GetComponent<Player>();
        playerSpriteRenderer = GetComponent<SpriteRenderer>();
    }

    void Update() {
        if (player) {
            player.DebugRaycasts(playerSpriteRenderer);
        }
    }
}
