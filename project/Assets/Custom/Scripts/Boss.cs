﻿using UnityEngine;
using System;
using System.Collections;

[RequireComponent(typeof(Collider2D))]
public class Boss : MonoBehaviour {
    [Serializable]
    public class PositionNodes {
        public Transform a;
        public Transform b;
    }

    public PositionNodes positionNodes;
    public ParticleSystem deathEffect;
    public int health;
    public int flickerRate = 10;
    public float invincibilityTime = 1.0f;

    [Header("Random Position")]
    [Tooltip("Allow position to be randomly animated.")]
    public bool randomizePosition;
    [Tooltip("The first position to potentially shift to.")]
    public Vector3 minPos = new Vector3(-1, -1, 0);
    [Tooltip("The second position to potentially shift to.")]
    public Vector3 maxPos = new Vector3(7, 7, 0);
    [Tooltip("The speed of the randomized position.")]
    public float positionSpeed = 0.5f;

    [Header("Rotation")]
    [Tooltip("Allow rotation to be randomly animated.")]
    public bool randomizeRotation;
    [Tooltip("The first rotation to potentially shift to.")]
    public Vector3 minRot = new Vector3(0, 0, -2);
    [Tooltip("The second rotation to potentially shift to.")]
    public Vector3 maxRot = new Vector3(0, 0, 2);
    [Tooltip("The speed of the randomized rotation.")]
    public float rotationSpeed = 1.0f;

    private bool isReceivingDamage = false;
    private SpriteRenderer spriteRenderer;
    private Color spriteRendererColour;
    private Transform currentTarget;
    private Vector3 random;
    private Vector3 initialPosition;
    private Vector3 initialRotation;
    private bool originalRandomizePosition;
    private bool originalRandomizeRotation;

    void Start () {
        GetComponent<Collider2D>().isTrigger = true;
        spriteRenderer = GetComponent<SpriteRenderer>();
        spriteRendererColour = spriteRenderer.color;
        currentTarget = positionNodes.a;

        random = new Vector3(RandomValue(), RandomValue(), RandomValue());
        originalRandomizePosition = randomizePosition;
        randomizeRotation = false;
        originalRandomizeRotation = randomizeRotation;
        randomizePosition = false;

        StartCoroutine(TravelToFirstPosition());
    }

    void Update() {
        if (!isReceivingDamage) {
            if (randomizePosition) {
                transform.position = GetRandomizedV3(initialPosition, minPos, maxPos, positionSpeed);
            }

            if (randomizeRotation) {
                transform.rotation = Quaternion.Euler(GetRandomizedV3(initialRotation, minRot, maxRot, rotationSpeed));
            }
        }
    }

    void OnTriggerEnter2D(Collider2D other){
        if (other.tag == "Player" && !isReceivingDamage) {
            StartCoroutine(ReceiveDamage());
        }
    }

    private Vector3 GetRandomizedV3(Vector3 initialVal, Vector3 val1, Vector3 val2, float speed) {
        Vector3 noise = new Vector3(
            GetPerlinNoise(random.x, speed),
            GetPerlinNoise(random.y, speed),
            GetPerlinNoise(random.z, speed)
        );

        return new Vector3(
            initialVal.x + Mathf.Lerp(val1.x, val2.x, noise.x),
            initialVal.y + Mathf.Lerp(val1.y, val2.y, noise.y),
            initialVal.z + Mathf.Lerp(val1.z, val2.z, noise.z)
        );
    }

    private float RandomValue() {
        return UnityEngine.Random.Range(0.0f, 65535.0f);
    }

    private float GetPerlinNoise(float val, float speed) {
        return Mathf.PerlinNoise(val, Time.time * speed);
    }

    private IEnumerator TravelToFirstPosition() {
        Vector2 pos = transform.position;
        float t = 0.0f;
        float travelTime = 1.0f;

        while (t < travelTime) {
            transform.position = Vector2.Lerp(
                pos
            ,   positionNodes.a.position
            ,   t / travelTime);

            t += Time.deltaTime;
            yield return null;
        }

        float posA = positionNodes.a.position.x;
        float posB = positionNodes.b.position.x;

        transform.localEulerAngles += new Vector3(0, posA > posB ? 0 : 180 , 0);
        isReceivingDamage = false;
        currentTarget = positionNodes.a;

        StartRandomMovement();
    }

    private void StartRandomMovement() {
        initialPosition = transform.position;
        randomizePosition = originalRandomizePosition;
        initialRotation = transform.localRotation.eulerAngles;
        randomizeRotation = originalRandomizeRotation;
    }

    public IEnumerator ReceiveDamage() {
        isReceivingDamage = true;
        randomizePosition = false;
        health--;

        if (health <= 0) {
            Die();
        } else {
            float invincibilityTimer = 0.0f;

            StartCoroutine(SpriteFlicker(invincibilityTimer));
            yield return StartCoroutine(Travel(invincibilityTimer));

            isReceivingDamage = false;
            StartRandomMovement();
        }
    }

    private IEnumerator SpriteFlicker(float invincibilityTimer) {
        float singleFlickerDuration = 0.5f / flickerRate;

        while (invincibilityTimer < invincibilityTime) {
            float t = 0.0f;

            while (t < singleFlickerDuration) {
                spriteRenderer.color = Color.Lerp(
                    spriteRendererColour
                ,   Color.clear
                ,   t / singleFlickerDuration);

                invincibilityTimer += Time.deltaTime;
                if (invincibilityTimer >= invincibilityTime) {
                    break;
                }

                t += Time.deltaTime;
                yield return null;
            }

            spriteRenderer.color = Color.clear;
            t = 0.0f;

            while (t < singleFlickerDuration) {
                spriteRenderer.color = Color.Lerp(
                    Color.clear
                ,   spriteRendererColour
                ,   t / singleFlickerDuration);

                invincibilityTimer += Time.deltaTime;
                if (invincibilityTimer >= invincibilityTime) {
                    break;
                }

                t += Time.deltaTime;
                yield return null;
            }

            spriteRenderer.color = spriteRendererColour;
            yield return null;
        }

        spriteRenderer.color = spriteRendererColour;
    }

    private IEnumerator Travel(float invincibilityTimer) {
        currentTarget = currentTarget == positionNodes.a ? positionNodes.b : positionNodes.a;

        Vector2 pos = transform.position;

        while (invincibilityTimer < invincibilityTime) {
            transform.position = Vector2.Lerp(
                pos
            ,   currentTarget == positionNodes.a ? positionNodes.a.position : positionNodes.b.position
            ,   invincibilityTimer / invincibilityTime);

            invincibilityTimer += Time.deltaTime;
            yield return null;
        }

        transform.localEulerAngles += new Vector3(0, 180, 0);
    }

    private void Die() {
        Instantiate(deathEffect, transform.position, Quaternion.identity);
        Destroy(gameObject);
    }
}
