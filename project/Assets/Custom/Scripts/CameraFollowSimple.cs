﻿using UnityEngine;

[AddComponentMenu("AnimCol/Camera/CameraFollowSimple")]
[RequireComponent(typeof(Camera))]
[ExecuteInEditMode]
public class CameraFollowSimple : MonoBehaviour {
    [Tooltip("The camera's distance fro the player. The effects of this value is previewable in the editor.")]
    public Player player;
    [Tooltip("")]
    public bool limitX;
    [Tooltip("The min and max X position that the camera is allowed to travel between.")]
    public Vector2 minMaxX = new Vector2(-100, 100);
    [Tooltip("")]
    public bool limitY;
    [Tooltip("")]
    public Vector2 minMaxY = new Vector2(-100, 100);
    [Tooltip("")]
    public Color rayCastColour = Color.magenta;
    [Tooltip("Enable this to see a line drawn from your lower to upper xPos limits.")]
    public bool debugMode;

    void LateUpdate() {
        if (player) {
            DebugCameraLimits();

            float xPos = player.transform.position.x;
            float yPos = player.transform.position.y;

            if (limitX) {
                xPos = Mathf.Clamp(player.transform.position.x, minMaxX.x, minMaxX.y);
            }
            if (limitY) {
                yPos = Mathf.Clamp(player.transform.position.y, minMaxY.x, minMaxY.y);
            }

            transform.position = new Vector3(xPos, yPos, Vector3.back.z);
        } else {
            Debug.Log("Please assign the Player to the '" + gameObject.name + "' game object.");
        }
    }

    private void DebugCameraLimits() {
#if UNITY_EDITOR
        if (debugMode) {
            float centerOfX = Mathf.Lerp(minMaxX.x, minMaxX.y, 0.5f);
            float centerOfY = Mathf.Lerp(minMaxY.x, minMaxY.y, 0.5f);

            Vector3 debugStart = new Vector2(minMaxX.x, centerOfY);
            Vector3 debugEnd = new Vector2(minMaxX.y, centerOfY);
            Debug.DrawLine(debugStart, debugEnd, rayCastColour);

            debugStart = new Vector2(centerOfX, minMaxY.x);
            debugEnd = new Vector2(centerOfX, minMaxY.y);
            Debug.DrawLine(debugStart, debugEnd, rayCastColour);
        }
#endif
    }
}