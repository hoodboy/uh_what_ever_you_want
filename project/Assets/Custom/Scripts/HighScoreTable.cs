﻿using UnityEngine;

[RequireComponent(typeof(Animator))]
[AddComponentMenu("AnimCol/UI/HighScoreTable")]
public class HighScoreTable : MonoBehaviour {
    void Start() {
        HighScoreTableItem[] highScoreItems = GetComponentsInChildren<HighScoreTableItem>();

        for (int i = 0; i < 5; i++) {
            highScoreItems[i].nameText.text = "";
            highScoreItems[i].scoreText.text = "";

            if (PlayerPrefs.HasKey("Score" + i)) {
                highScoreItems[i].nameText.text = PlayerPrefs.GetString("Name" + i);
                highScoreItems[i].scoreText.text = (PlayerPrefs.GetInt("Score" + i)).ToString();
            }
        }
    }
}
