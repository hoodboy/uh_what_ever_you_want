﻿using UnityEngine;
using System.Collections;
using System;
using GameManager;

[AddComponentMenu("")]
public class Events : MonoBehaviour {
    [Serializable]
    public class AdvancedAnimator {
        [Tooltip("The object you wish to trigger an animation for. It must have an Animator component attached.")]
        public Animator triggeredAnimator;
        [Tooltip("The state that should be triggered from your selected animator. Check your state within the animator to find this name. If the name is not found or specified, then we will look for an 'active' bool parameter in the 'triggeredAnimator'.")]
        public string triggeredState;
        [Tooltip("The time in seconds that should pass before this animator is triggered.")]
        public float delayAnimationInSeconds = 0.25f;
        [Range(0.0f, 1.0f)]
        [Tooltip("If your OffsetBehaviour is specified, it is specified by this value.")]
        public float cycleOffset = 0.0f;
    }

    [Serializable]
    public class ObjectsToSpawn {
        [Tooltip("The prefab you wish to spawn.")]
        public GameObject objectToSpawn;
        [Tooltip("The time in seconds that should pass before this object is spawned.")]
        public float spawnDelayInSeconds = 0.25f;
        [Tooltip("The gizmo object that defines the spawn location.")]
        public Gizmo spawnLocationObject;
        [Tooltip("The prefab particle system that gets spawned before the object is spawned.")]
        public ParticleSystem spawnEffect;
        [Tooltip("A distance offset from the spawn point's point-of-origin")]
        public Vector2 spawnEffectOffset;
        [Tooltip("An array of audio clips that are randomly selected and played when the object is spawned.")]
        public AudioClip[] audioClip;
        
    }

    [Serializable]
    public class ObjectsToDestroy {
        [Tooltip("The object in your scene that needs to be destroyed.")]
        public GameObject objectToDestroy;
        [Tooltip("The time in seconds that should pass before this object is destroyed.")]
        public float destroyDelayInSeconds = 0.25f;
        [Tooltip("The prefab particle system that gets spawned before the object is destroyed.")]
        public ParticleSystem destroyEffect;
        [Tooltip("A distance offset from the destroyed object's point-of-origin")]
        public Vector2 destroyEffectOffset;
        [Tooltip("An array of audio clips that are randomly selected and played when the object is destroyed.")]
        public AudioClip[] audioClip;
    }

    [Serializable]
    public class CutScenes {
        [Tooltip("The camera to switch to for this cutscene.")]
        public Camera camera;
        [Tooltip("Whether or not the camera should fade-to-black when transitioning to the next camera.")]
        public bool fadeToBlack = true;
        [Tooltip("Adds some delay at the start / end of the current cutscene and dialogue. This comes at a cost, note that it adds to the total duration.")]
        public Vector2 padding = Vector2.zero;
        [Tooltip("The dialogue set associated with this cut-scene.")]
        public Dialogue dialogue;
        [Tooltip("Advanced events to add to this cutscene.")]
        public AdvancedEvents advancedEvents;
    }

    [Serializable]
    public class AdvancedEvents {
        [Tooltip("A series of lights to toggle on or off when the event is triggered. Lights that are off will be turned on, and vise-versa.")]
        public Light[] lights;
        [Tooltip("A series of particle systems to play or stop when the event is triggered. Particle Systems not currently playing will be triggered, particle systems that ARE currently playing will be stopped.")]
        public ParticleSystem[] particleSystems;
        [Tooltip("A series of animators that will have their 'active' booleans toggled.")]
        public Animator[] animators;
        [Tooltip("A series of objects to animate when the event is triggered")]
        public AdvancedAnimator[] advancedAnimators;
        [Tooltip("A series of objects to spawn when the event is triggered")]
        public ObjectsToSpawn[] objectsToSpawn;
        [Tooltip("A series of objects to destroy when the event is triggered")]
        public ObjectsToDestroy[] objectsToDestroy;
    }

    [Serializable]
    public class Dialogue {
        [Tooltip("The dialogue text that displays.")]
        public string text;
        [Tooltip("The audio cilp that plays during the dialogue.")]
        public AudioClip audio;
        [Tooltip("The avatar with a name and image that appears during dialogue.")]
        public CharacterAvatar avatar;
        [NonSerialized]
        public bool isAwaitingChoice;
    }

    [Header("Triggered Events")]
    [Tooltip("Advanced events to add to this cutscene.")]
    public AdvancedEvents events;
    [Header("Cut Scene")]
    [Tooltip("A delay in seconds that occurs before the chain of cutscenes begin.")]
    public float cutSceneDelay;
    [Tooltip("A sequence of cameras that we switch to. Animations within these camera views should be triggerd via ObjectsToAnimate.")]
    public CutScenes[] cutScenes;
    [Header("Dialogue")]
    [Tooltip("Dialogue that plays before the cutscene.")]
    public Dialogue beginningDialogue;
    [Tooltip("Dialogue that plays after the cutscene.")]
    public Dialogue endDialogue;
    [Tooltip("A set of player features that can be tweaked when receiving this InventoryPickUp.")]
    public Player.FeatureConfig featureConfig;

    public IEnumerator TriggerEvents(bool shouldEndTheGame, string nextLevel) {
        Generic.isControllable = !(
            beginningDialogue.text.Length > 0 
        ||  cutScenes.Length > 0 
        ||  endDialogue.text.Length > 0);

        Generic.ToggleLights                                     (events.lights);
        Generic.ToggleParticleSystems                            (events.particleSystems);
        Generic.ToggleAnimators                                  (events.animators);

        if (beginningDialogue.text.Length > 0) {
            yield return StartCoroutine(Generic.DisplayDialogue  (beginningDialogue));
        }

        StartCoroutine(Generic.SpawnObjects                      (events.objectsToSpawn));
        StartCoroutine(Generic.DestroyObjects                    (events.objectsToDestroy));
        StartCoroutine(Generic.TriggerAnimators                  (events.advancedAnimators));

        if (cutScenes.Length > 0) {
            yield return StartCoroutine(
                Generic.CameraCutSceneSwitch                     (cutScenes, cutSceneDelay, shouldEndTheGame));
        }

        if (shouldEndTheGame) {
            if (Generic.scoreSystem) {
                Generic.TallyTotalScore();
            }
            StartCoroutine(Generic.LoadScene(nextLevel));
        } else if (beginningDialogue.text.Length > 0) {
            yield return StartCoroutine(Generic.DisplayDialogue  (endDialogue));
        }

        featureConfig.SetPlayerFeatures();
        Generic.isControllable = true;
    }
}
