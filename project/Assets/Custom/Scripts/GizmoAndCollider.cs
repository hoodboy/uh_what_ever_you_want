﻿using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

[AddComponentMenu("AnimCol/Gizmos/GizmoAndCollider")]
[RequireComponent(typeof(BoxCollider2D))]
[RequireComponent(typeof(Rigidbody2D))]
[ExecuteInEditMode]
#if UNITY_EDITOR
[InitializeOnLoad]
#endif
public class GizmoAndCollider : MonoBehaviour {
    [Header("Gizmo Properties")]
    [Tooltip("The colour of the gizmo.")]
    public Color colour = Color.magenta;
    [Tooltip("The size of the gizmo.")]
    public Vector2 volume = Vector2.one;
    private BoxCollider2D boxCol;

    void Awake() {
        SetBoxCol();
        SetRigidBody();
    }

    void Update () {
        boxCol.size = volume;
    }

    private void OnDrawGizmos() {
        Gizmos.color = colour;
        Gizmos.DrawCube(transform.position, volume);
    }

    private void SetRigidBody() {
        Rigidbody2D rb = GetComponent<Rigidbody2D>();
        rb.isKinematic = true;
        rb.interpolation = RigidbodyInterpolation2D.None;
        rb.collisionDetectionMode = CollisionDetectionMode2D.Discrete;
    }

    private void SetBoxCol() {
        boxCol = GetComponent<BoxCollider2D>();
        boxCol.isTrigger = true;
        boxCol.offset = Vector2.zero;
        boxCol.size = volume;
    }
}
