﻿using UnityEngine;
using System;

[AddComponentMenu("AnimCol/Events/PuzzleRotation")]
public class PuzzleRotation : Events {
    public enum RotateByDegrees {
        fourtyFive = 45,
        ninety     = 90
    }

    public enum DegreesOfRotation {
        zero          = 0,
        fourtyFive    = 45,
        ninety        = 90,
        oneThirtyFive = 135,
        oneEighty     = 180,
        twoTwentyFive = 225,
        twoSeventy    = 270,
        threeFifteen  = 315
    }

    [Serializable]
    public class FacePuzzleSet {
        [Tooltip("The object that gets rotated in order to solve the puzzle.")]
        public Transform rotatable;
        [Tooltip("The object that triggers the rotatable's rotation.")]
        public Collider2D trigger;
        [Tooltip("The angle of rotation that the rotatable object begins at.")]
        public DegreesOfRotation startRotation  = DegreesOfRotation.zero;
        [Tooltip("The angle of rotation that the rotatable object needs to be at in order to clear the puzzle.")]
        public DegreesOfRotation targetRotation = DegreesOfRotation.zero;
    }

    //fields begin
    [Header("Rotation Puzzle Parameters")]
    [Tooltip("The amount of degrees that the puzzle should operate in. 90 will cause all of the Face Puzzle Sets to take 45 degree rotations and drop them to their 90 degree subordinate.")]
    public RotateByDegrees rotationStep = RotateByDegrees.fourtyFive;
    [Tooltip("An array of sounds from which only one will be played when interacting with trigger object.")]
    public AudioClip[] rotateSound;
    [Tooltip("An array of sounds from which only one will be played when the puzzle has been cleared.")]
    public AudioClip[] successSound;
    [Tooltip("All sets involved in clearing the puzzle.")]
    public FacePuzzleSet[] facePuzzleSets;

    public bool hasFired { get; set; }

    void Start() {
        InitializePuzzleSets();
        RotationCheck();
    }

    private void InitializePuzzleSets() {
        foreach (FacePuzzleSet facePuzzleSet in facePuzzleSets) {
            if (facePuzzleSet.trigger) {
                PuzzleRotationTrigger puzzleTrigger = facePuzzleSet.trigger.gameObject.AddComponent<PuzzleRotationTrigger>();
                puzzleTrigger.puzzle                = this;
                puzzleTrigger.targetToRotate        = facePuzzleSet.rotatable;
                facePuzzleSet.trigger.isTrigger     = true;

                if (rotationStep == RotateByDegrees.ninety) {
                    ModifyToDegrees(ref facePuzzleSet.startRotation);
                    ModifyToDegrees(ref facePuzzleSet.targetRotation);
                }

                puzzleTrigger.targetToRotate.transform.eulerAngles = new Vector3(0, 0, Mathf.Round((float)facePuzzleSet.startRotation));
            }

        }
    }

    private void ModifyToDegrees(ref DegreesOfRotation property) {
        if (property == DegreesOfRotation.fourtyFive) {
            property = DegreesOfRotation.zero;
        }
        if (property == DegreesOfRotation.oneThirtyFive) {
            property = DegreesOfRotation.ninety;
        }
        if (property == DegreesOfRotation.twoTwentyFive) {
            property = DegreesOfRotation.oneEighty;
        }
        if (property == DegreesOfRotation.threeFifteen) {
            property = DegreesOfRotation.twoSeventy;
        }
    }

    public void RotationCheck() {
        if (!hasFired) {
            foreach (FacePuzzleSet facePuzzleSet in facePuzzleSets) {
                if (Mathf.Round(facePuzzleSet.rotatable.eulerAngles.z) != (int)facePuzzleSet.targetRotation) {
                    return;
                }
            }

            Success();
        }
    }

    private void Success() {
        GameManager.Generic.PlaySFX(successSound);
        Activate();
    }

    void Activate() {
        hasFired = true;
        StartCoroutine(TriggerEvents(false, null));
    }
}
