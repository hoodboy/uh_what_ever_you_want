﻿using UnityEngine;
using System.Collections;
using GameManager;

[AddComponentMenu("AnimCol/Events/Switch")]
public class Switch : MonoBehaviour {
    [Tooltip("A series of lights to toggle on or off when the event is triggered. Lights that are off will be turned on, and vise-versa.")]
    public Light[] lights;
    [Tooltip("A series of particle systems to play or stop when the event is triggered. Particle Systems not currently playing will be triggered, particle systems that ARE currently playing will be stopped.")]
    public ParticleSystem[] particleSystems;
    [Tooltip("A series of animators that will have their 'active' booleans toggled.")]
    public Animator[] animators;
    [Tooltip("A series of objects to animate when the event is triggered")]
    public Events.AdvancedAnimator[] advancedAnimators;
    [Tooltip("An animator object that will animate between active and inactive unconditionally when entering and exiting the trigger collider.")]
    public Animator enterExitAnimator;
    [Tooltip("Whether or not the switch should activate only once the 'Submit' button has been pressed.")]
    public bool isButtonActivated = true;
    [Tooltip("Check this if you want the switch to be inaccessible after being fired for the first time.")]
    public bool fireOnceOnly = true;
    [Tooltip("Enable this to nullify player input while switch events occur.")]
    public bool ignoreInputDuringEvents = true;

    private bool inactive = true;
    private Animator[] enterExitAnimatorArray;

    private void Start() {
        if (GetComponent<Collider2D>()) {
            GetComponent<BoxCollider2D>().isTrigger = true;
        } else {
            Debug.LogWarning(gameObject.name + " is missing a Collider2D component.");
        }

        if (enterExitAnimator) {
            enterExitAnimatorArray = new Animator[1];
            enterExitAnimatorArray[0] = enterExitAnimator;
        }
    }

    private void OnTriggerEnter2D(Collider2D other) {
        if (inactive 
        &&  other.tag == "Player") {
            if (enterExitAnimator) {
                Generic.ToggleAnimators(enterExitAnimatorArray);
            }

            if (!isButtonActivated) {
                if (fireOnceOnly) {
                    inactive = false;
                }

                StartCoroutine(ToggleItems());
            }
        }
    }

    private void OnTriggerExit2D(Collider2D other) {
        if (enterExitAnimator
        &&  other.tag == "Player" 
        &&  inactive) {
            Generic.ToggleAnimators(enterExitAnimatorArray);
        }
    }

    private void OnTriggerStay2D(Collider2D other) {
        if (inactive 
        &&  other.tag == "Player" 
        &&  isButtonActivated 
        &&  Input.GetButtonDown("Submit")) {
            if (fireOnceOnly) {
                inactive = false;
            }

            StartCoroutine(ToggleItems());
        }
    }

    private IEnumerator ToggleItems () {
        Generic.isControllable = !ignoreInputDuringEvents;

        Generic.ToggleLights(lights);
        Generic.ToggleParticleSystems(particleSystems);
        Generic.ToggleAnimators(animators);
        yield return StartCoroutine(Generic.TriggerAnimators(advancedAnimators));

        Generic.isControllable = true;
    }
}
