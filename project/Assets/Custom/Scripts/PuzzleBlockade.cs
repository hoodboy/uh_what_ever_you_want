﻿using UnityEngine;
using System;
using System.Collections;
using GameManager;
#if UNITY_EDITOR
using UnityEditor;
#endif

#if UNITY_EDITOR
[InitializeOnLoad]
[ExecuteInEditMode]
#endif
[AddComponentMenu("AnimCol/Events/PuzzleBlockade")]
public class PuzzleBlockade : MonoBehaviour {
    [Serializable]
    public class WarpEffects {
        public ParticleSystem enterEffect;
        public ParticleSystem exitEffect;
    }

    [Tooltip("Whether or not the switch should activate only once the 'Submit' button has been pressed.")]
    public bool isButtonActivated = true;

    public Animator switchAnimator;
    public Transform playerRespawnPos;
    public float delayBeforeRespawn;
    public WarpEffects warpEffects;
    public Transform blockadeUnitParent;
    public Color debugColor = Color.green;
    public ParticleSystem[] particleSystems;
    public Animator[] animators;

    public bool isCompleted { get; set; }
    public PuzzleBlockadeUnit[] puzzleBlockadeUnits { get; set; }

    [NonSerialized]
    public bool animatorsShouldBeToggled = true;

    private void Awake() {
        if (Application.isPlaying) {
            GetComponent<Collider2D>().isTrigger = true;
            switchAnimator.SetBool("active", false);
        }

        puzzleBlockadeUnits = blockadeUnitParent.GetComponentsInChildren<PuzzleBlockadeUnit>();
        foreach (PuzzleBlockadeUnit pbu in puzzleBlockadeUnits) {
            pbu.transform.position = pbu.beginOnEndPos ? pbu.endPos : pbu.startPos;
            pbu.puzzleBlockade = this;
        }
    }
#if UNITY_EDITOR
    void Update() {
        if (!Application.isPlaying) {
            foreach (PuzzleBlockadeUnit pbu in puzzleBlockadeUnits) {
                Debug.DrawLine(transform.position, pbu.transform.position, debugColor);
            }

            Debug.DrawLine(transform.position, playerRespawnPos.position, debugColor);
        }
    }
#endif

    private void OnTriggerEnter2D(Collider2D other) {
        if (Application.isPlaying && other.tag == "Player") {
            if (!switchAnimator.GetBool("active") && !isCompleted && !isButtonActivated) {
                switchAnimator.SetBool("active", false);
                StartCoroutine("TriggerBlockades");
            } else if (switchAnimator.GetBool("active") && isCompleted) {
                isButtonActivated = true;
                ResetBlockades();
            }
        }
    }

    private void OnTriggerStay2D(Collider2D other) {
        if (Application.isPlaying) {
            if (!switchAnimator.GetBool("active")
            && !isCompleted
            &&  other.tag == "Player" 
            &&  isButtonActivated 
            &&  Input.GetButtonDown("Submit")) {
                switchAnimator.SetBool("active", false);
                StartCoroutine(TriggerBlockades());
            }
        }
    }

    private IEnumerator TriggerBlockades () {
        switchAnimator.SetBool("active", true);
        Generic.ToggleParticleSystems(particleSystems);
        Generic.ToggleAnimators(animators);
        foreach (PuzzleBlockadeUnit pbu in puzzleBlockadeUnits) {
            yield return new WaitForSeconds(pbu.delay);

            pbu.StartCoroutine("Travel");
        }
    }

    public IEnumerator RevertBlockades() {
        yield return new WaitForSeconds(delayBeforeRespawn);

        //Generic.player.transform.position = playerRespawnPos.position;
        StartCoroutine("EnterWarp");

        foreach (PuzzleBlockadeUnit pbu in puzzleBlockadeUnits) {
            pbu.StopAllCoroutines();
            pbu.StartCoroutine("Revert");
        }

        switchAnimator.SetBool("active", false);
        Generic.ToggleParticleSystems(particleSystems);
        Generic.ToggleAnimators (animators);
    }

    public void ResetBlockades() {
        foreach (PuzzleBlockadeUnit pbu in puzzleBlockadeUnits) {
            pbu.StopAllCoroutines();
            pbu.StartCoroutine("Revert");
        }

        switchAnimator.SetBool("active", false);
        Generic.ToggleParticleSystems(particleSystems);

        if (animatorsShouldBeToggled) {
            Generic.ToggleAnimators (animators);
            Generic.ToggleParticleSystems(particleSystems);
        } else
        {
            animatorsShouldBeToggled = true;
        }

        isCompleted = false;
    }

    public void EnterWarp() {
        Generic.isControllable = false;

        if (warpEffects.enterEffect) {
            Instantiate(warpEffects.enterEffect, Generic.player.transform.position, Quaternion.identity);
        }

        // Player.
        Generic.player.spriteRenderer.enabled = false;
        Generic.player.col2d.enabled = false;
        Generic.player.rb2d.isKinematic = true;
        Generic.player.rb2d.gravityScale = 0.0f;

        StartCoroutine(Travel());
    }

    private IEnumerator Travel() {
        float t = 0.0f;
        float duration = 0.25f;
        Vector2 currentPlayerPos = Generic.player.transform.position;

        while (t < duration) {
            Generic.player.transform.position = Vector2.Lerp(
                currentPlayerPos,
                playerRespawnPos.position,
                t / duration
            );

            t += Time.deltaTime;
            yield return null;
        }

        Generic.player.transform.position = playerRespawnPos.position;
        ExitWarp();
    }

    public void ExitWarp() {
        if (warpEffects.exitEffect) {
            Instantiate(warpEffects.exitEffect, Generic.player.transform.position, Quaternion.identity);
        }

        // Player.
        Generic.player.spriteRenderer.enabled = true;
        Generic.player.col2d.enabled = true;
        Generic.player.rb2d.isKinematic = false;
        Generic.player.rb2d.gravityScale = Generic.player.originalGravityScale;

        Generic.isControllable = true;
    }
}
