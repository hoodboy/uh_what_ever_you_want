﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using GameManager;

[AddComponentMenu("AnimCol/SceneManagers/EndGame")]
public class EndGame : MonoBehaviour {
    [Tooltip("The text component for the UI that displays your player score in realtime")]
    public Text scoreText;
    [Tooltip("The text component for the UI that displays your player score in realtime")]
    public float waitTime = 5.0f;
    [Tooltip("The input field responsible for receiving the player's name, this gets applied to the High Score chart.")]
    public InputField highScoreInputField;
    [Tooltip("The name of the scene that will be loaded after this scene.")]
    public string nextScene = "StartScreen";

    private bool leavingScene;
    private float scorePosition;
    private Animator highScoreInputFieldAnimator;

    void Start() {
        if (highScoreInputField && scoreText) {
            GetParentAnimator();
            SetUpInputField();

            if (IsHighScore()) {
                scoreText.text = "High Score: " + Generic.totalScore;
            } else {
                Invoke("GoToStartScreen", waitTime);
                scoreText.text = "Score: " + Generic.totalScore;
            }

            if (!Generic.scoreSystem) {
                Destroy(highScoreInputField.gameObject);
                Invoke("GoToStartScreen", waitTime);
            }
        } else {
            Invoke("GoToStartScreen", waitTime);
        }

        StartCoroutine(Generic.SceneFadeIn());
    }

    void Update() {
        if (Generic.scoreSystem && highScoreInputField) {
            if (Input.GetButton("Fire1") || Input.GetButton("Fire2") || Input.GetButton("Fire3")) {
                highScoreInputField.Select();
            }

            if (Input.anyKeyDown && !leavingScene) {
                //GoToStartScreen();
            }
        }
    }

    private void GetParentAnimator() {
        Transform transformInQuestion = highScoreInputField.transform;
        while (!highScoreInputFieldAnimator) {
            highScoreInputFieldAnimator = transformInQuestion.GetComponent<Animator>();
            transformInQuestion = transformInQuestion.parent;
        }
    }

    private void SetUpInputField() {
        highScoreInputField.onEndEdit.RemoveAllListeners();
        highScoreInputField.onEndEdit.AddListener(delegate { StartCoroutine(SubmitScore()); });
    }

    private void GoToStartScreen() {
        if (!leavingScene) {
            leavingScene = true;
            StartCoroutine(Generic.LoadScene(nextScene));
        }
    }

    private bool IsHighScore() {
        for (int i = 0; i < 5; i++) {
            if (!PlayerPrefs.HasKey("Score" + i) && !PlayerPrefs.HasKey("Name" + i)
            ||  PlayerPrefs.HasKey("Score" + i) && Generic.totalScore > PlayerPrefs.GetInt("Score" + i)) {
                scorePosition = i;
                StartCoroutine(DisplayHighScoreInputField());
                return true;
            }
        }

        return false;
    }

    private IEnumerator DisplayHighScoreInputField() {
        highScoreInputFieldAnimator.SetBool("active", true);

        while (highScoreInputFieldAnimator.IsInTransition(0)) {
            yield return null;
        }

        highScoreInputField.Select();
    }

    private IEnumerator SubmitScore() {
        if (highScoreInputField.text.Length > 0) {
            PlayerPrefs.SetInt("Score" + scorePosition, Generic.totalScore);
            PlayerPrefs.SetString("Name" + scorePosition, highScoreInputField.text);

            while (highScoreInputFieldAnimator.IsInTransition(0)) {
                yield return null;
            }

            GoToStartScreen();
        }
    }
}


