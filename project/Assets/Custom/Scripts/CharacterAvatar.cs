﻿using UnityEngine;

[AddComponentMenu("AnimCol/Character/CharacterAvatar")]
public class CharacterAvatar : MonoBehaviour {
    [Tooltip("The name of the avatar who is speaking.")]
    public string avatarName;
    [Tooltip("The image of the avatar who is speaking.")]
    public Sprite avatarSprite;
    [Tooltip("Whether or not to display the avatar's name.")]
    public bool displayName = true;
}
