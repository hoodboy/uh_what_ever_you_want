﻿using UnityEngine;
using System;
using System.Collections;
using GameManager;

[AddComponentMenu("AnimCol/Character/Player")]
[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(SpriteRenderer))]
[RequireComponent(typeof(Animator))]
/// <summary>
/// Handle collisions that should result in death.
/// Take care of returning the player to his original colour in the event of death or application quit.
/// Fade to black when dying and returning to a checkpoint.
/// </summary>
public class Player : MonoBehaviour {
    [Serializable]
    public class FeatureConfig {
        [Serializable]
        public class NewSpeed {
            public bool isAffected;
            public float newSpeed;
        }

        [Serializable]
        public class NewJumpPower {
            public bool isAffected;
            public float newJumpPower;
        }

        [Serializable]
        public class NewMultiJumpAllowance {
            public bool isAffected;
            public int newMultiJumpAllowance;
        }

        [Serializable]
        public class NewGravityMultiplier {
            public bool isAffected;
            public float newGravityMultiplier;
        }

        [Serializable]
        public class NewWallJump {
            public bool isAffected;
            public bool newWallJump;
        }

        public NewSpeed speed;
        public NewJumpPower jumpPower;
        public NewMultiJumpAllowance multiJumpAllowance;
        public NewGravityMultiplier gravityMultiplier;
        public NewWallJump wallJump;

        public void SetPlayerFeatures() {
            Generic.player.speed = speed.isAffected ? speed.newSpeed : Generic.player.speed;
            Generic.player.jumpPower = jumpPower.isAffected ? jumpPower.newJumpPower : Generic.player.jumpPower;
            Generic.player.multiJumpAllowance = multiJumpAllowance.isAffected ? multiJumpAllowance.newMultiJumpAllowance : Generic.player.multiJumpAllowance;
            Generic.player.gravityMultiplier = gravityMultiplier.isAffected ? gravityMultiplier.newGravityMultiplier : Generic.player.gravityMultiplier;
            Generic.player.wallJump.enableWallJump = wallJump.isAffected ? wallJump.newWallJump : Generic.player.wallJump.enableWallJump;
        }
    }

    [Serializable]
    public class Recoil {
        [Tooltip("Only valid if you have features.health set above zero. This is the amount of time in seconds that the player spends being stunned after taking a hit and losing some health.")]
        public float stunnedTime = 0.3f;
        [Tooltip("Only valid if you have features.health set above zero. This is the amount of time in seconds that the player is invincible for after taking a hit and losing some health.")]
        public float invincibilityTime = 1.0f;
        [Tooltip("The flicker rate per second when health is enabled and the player takes a hit. Flickering lasts for the duration of invincibilityTime.")]
        public float flickerRate = 10.0f;
        [Tooltip("The amount of upward force applied to show that damage has been taken.")]
        public float recoilVelocity = 20.0f;
    }

    [Serializable]
    public class WallJump {
        [Serializable]
        public class WallRaycast {
            [Tooltip("The height along the character, at which we should begin checking for walls while jumping, in preparation for a wall-jump.")]
            public float originOffset = 1.0f;
            [Tooltip("The distance before the player, within which to check if a wall is touched.")]
            public float distance = 1.0f;
            [Tooltip("Increase this value for more accurate, albeit, more expensive ground detection.")]
            public int samplesDivisions = 10;
            [Tooltip("The width within which to distribute the rays that search for the ground.")]
            public float span = 1.27f;
            [Tooltip("The name of the layer you wish to use for your wallJumpable colliders.")]
            public string wallLayerMask = "Ground";

            public bool isWallSliding { get; set; }
            public float spacing { get; set; }
        }

        [Tooltip("Enable wall jump for your character.")]
        public bool enableWallJump;
        [Tooltip("Enable this to allow your character to rebound off of walls with colliders that exist under the 'Ground' layer. Said walls must have a collider.")]
        public bool autoRebounding = false;
        [Tooltip("The player will not be able to turn away from the wall, once attached.")]
        public bool bindToWall;
        [Tooltip("The Power of your rebound when wall jumping.")]
        public Vector2 reboundPower = new Vector2(8.0f, 22.0f);
        [Tooltip("The horizontal speed that's applied from the player while rebounding.")]
        public float airSpeed = 40.0f;
        [Tooltip("Adjust settings for the rays cast to find walls.")]
        public WallRaycast wallRayCast;

        public bool isRebounding { get; set; }
    }

    [Serializable]
    public class ClimbBehaviour {
        [Tooltip("Whether or not to instantly latch on to a climbable on-contact.")]
        public bool autoClimb;
        [Tooltip("If you happen to be climbing on anything, this value will adjust the speed at which you can climb.")]
        public Vector2 climbSpeed = new Vector2(2.0f, 2.0f);
    }

    [Serializable]
    public class MaxSpeed {
        [Tooltip("Enable this to log the kinds of speeds you're achieving in terms of velocity. Observing this output will help you decide which values to use, should you decide to limit your characters max speeds.")]
        public bool logMySpeed;
        [Tooltip("Enable this, and your character will never exceed the speed specified by the 'Max Velocity' property, below.")]
        public bool enableVelocityCap = false;
        [Tooltip("The maximum velocity that can be reached both horizontally and vertically.")]
        public Vector2 MaxVelocity = new Vector2(40, 40);
    }

    [Serializable]
    public class GroundRaycast {
        [Tooltip("By default, the groundCheck raycast is sent from the pivot of the character, which is usually at his feet. If for any reason this is not ideal, you can offset it here.")]
        public float originOffset = 0.005f;
        [Tooltip("Increase this value for more accurate, albeit, more expensive ground detection.")]
        public int samplesDivisions = 10;
        [Tooltip("The distance at which to search for the ground when in the air. Preview by applying the PlayerRaycastDebug script to this very Game Object.")]
        public float distance = 0.49f;
        [Tooltip("A smaller raycast size, used for the distance while ascending. You shouldn't have to modify this, but do so if you're getting problems with your character failing to enter the jump animation.")]
        public float ascendingDistance = 0.05f;
        [Tooltip("The width within which to distribute the rays that search for the ground.")]
        public float width = 1.27f;

        public bool isGrounded { get; set; }
        public float originalDistance { get; set; }
        public float spacing { get; set; }
    }

    [Tooltip("Enable this to avoid the gradual speed increase when pressing the horizontal keys or tilting the left analogue stick on an input controller. This will make the walk-cycle redundant. This is intended as a fix for when 2 opposite horizontal keys are pressed at the exact same time.")]
    public bool rawAxialInput = false;
    [Tooltip("The speed at which the player moves left and right.")]
    public float speed = 8.0f;
    [Tooltip("The amount of upward force applied when jumping.")]
    public float jumpPower = 18.0f;
    [Tooltip("The amount of extra jumps you're allowed to do, beyond your initial jump, before hitting the ground again.")]
    public int multiJumpAllowance = 1;
    [Tooltip("The speed at which the player descends again after jumping.")]
    public float gravityMultiplier = 10f;
    [Tooltip("Adjust settings for the rays cast to detect the ground.")]
    public GroundRaycast groundRaycast;
    [Tooltip("Enable and customize the wall-jump feature for your character.")]
    public WallJump wallJump;
    [Tooltip("Modify the behaviour of your climbing.")]
    public ClimbBehaviour climbing;
    [Tooltip("Modify the behaviour of your rope-swinging feature.")]
    public RopeSwing ropeSwing;
    [Tooltip("A collection of properties regarding the behaviour that occurs when the player takes a hit with health enabled.")]
    public Recoil recoil;
    [Tooltip("If you feel that your character reaches unreasonable speeds at certain stages of your game, either from wall-jumping or falling etc., then you can set max X and Y speed here.")]
    public MaxSpeed maxSpeed;

    public Coroutine displayPopUp { get; set; }
    public SpriteRenderer spriteRenderer { get; set; }
    public Collider2D col2d { get; set; }
    public Rigidbody2D rb2d { get; set; }
    public bool jump { get; set; }
    public float h { get; set; }
    public float v { get; set; }
    public float originalGravityScale { get; set; }
    public int currentJump { get; set; }

    //[NonSerialized]
    public bool isReceivingDamage = false;
    [NonSerialized]
    public Vector2 facingDirection = Vector2.right;

    private Collider2D climbableCollider;
    private Coroutine loadScene;
    private Color spriteRendererColour;
    private Animator anim;
    private bool isClimbing;
    private float fallTime = 0.0f;
    private int groundLayerMask;
    private int wallLayerMask;
    private Vector2 defaultPosAsSpawnPoint;
    
    // Ad hoc
    [Space(20)]
    public float delayBeforeDeath = 0.125f;

    void OnEnable() {
        Init.EventManager.OnHitPlayer += ReceiveDamage;
        Init.EventManager.OnDiePlayer += Die;
    }
    
    void OnDisable() {
        Init.EventManager.OnHitPlayer -= ReceiveDamage;
        Init.EventManager.OnDiePlayer -= Die;
    }

    void Start() {
        defaultPosAsSpawnPoint = transform.position;
        spriteRenderer = GetComponent<SpriteRenderer>();
        spriteRendererColour = spriteRenderer.color;
        anim = GetComponent<Animator>();
        col2d = GetComponent<Collider2D>();
        rb2d = GetComponent<Rigidbody2D>();
        rb2d.constraints = RigidbodyConstraints2D.FreezeRotation;
        originalGravityScale = rb2d.gravityScale;
        groundRaycast.originalDistance = groundRaycast.distance;
        gameObject.tag = "Player";
        gameObject.layer = LayerMask.NameToLayer("Player");
        groundLayerMask = 1 << LayerMask.NameToLayer("Ground");
        wallLayerMask = 1 << LayerMask.NameToLayer(wallJump.wallRayCast.wallLayerMask);

        if (ropeSwing) {
            ropeSwing.RopeSwingInitialization();
        }
    }

    void Update() {
        CheckDescension();

        if (!wallJump.enableWallJump 
        || wallJump.enableWallJump && (!(wallJump.autoRebounding && wallJump.isRebounding) && !(wallJump.bindToWall && wallJump.wallRayCast.isWallSliding && !groundRaycast.isGrounded))
        ) {
            if (rawAxialInput) {
                h = Generic.isControllable ? Input.GetAxisRaw("Horizontal") : 0.0f;
            } else {
                h = Generic.isControllable ? Input.GetAxis("Horizontal") : 0.0f;
            }

            if (Input.GetButton("Horizontal")) {
                facingDirection = new Vector2(Input.GetAxisRaw("Horizontal"), 0);
            }
        }

        v = Generic.isControllable ? Input.GetAxisRaw("Vertical") : 0.0f;

        if (!jump) {
            jump = Input.GetButtonDown ("Jump");
        }

        if (climbableCollider && Input.GetButtonDown("Vertical")) {
            if (ropeSwing) {
                ropeSwing.ForgetRopeHierarchy();
            }

            isClimbing = true;
        }

        rb2d.gravityScale = isClimbing ? 0 : originalGravityScale;
    }

    void FixedUpdate() {
        Move();
        jump = false;
    }

    void OnCollisionEnter2D(Collision2D col2D){
        if (col2D.gameObject.tag == "Bouncy") {
            Bouncy bouncy = col2D.gameObject.GetComponent<Bouncy> ();

            Bouncy[] bouncies = FindObjectsOfType<Bouncy> ();
            foreach (Bouncy b in bouncies){
                b.bouncePower += bouncy.bounceIncrement;
            }

            bouncy.bouncePower = Mathf.Clamp(bouncy.bouncePower, bouncy.bouncePower, bouncy.bouncePowerLimit);

            rb2d.velocity = Vector2.up * bouncy.bouncePower;
        }
    }

    IEnumerator WaitForDeath(KillCollider killCollider) {
        Generic.isControllable = false;
        Generic.PlaySFX(killCollider.killSound);

        yield return new WaitForSeconds(delayBeforeDeath);

        StartCoroutine(Respawn(killCollider.deathMessages));
    }

    void OnTriggerEnter2D(Collider2D other) {
        if (other.tag == "KillBox" && !isReceivingDamage) {
            KillCollider killCollider = other.GetComponent<KillCollider>();
            if (killCollider.damage > 0) {
                Debug.Log("keuhh");
                StopDamagingCoroutines();
                StartCoroutine(ReceiveDamage(killCollider.killSound, killCollider.deathMessages, -killCollider.damage));
            } else {
                StopAllCoroutines();
                StartCoroutine(WaitForDeath(killCollider));
            }
        }

        if (other.tag == "Climbable") {
            climbableCollider = other;

            if (climbing.autoClimb && !groundRaycast.isGrounded) {
                if (ropeSwing) {
                    ropeSwing.ForgetRopeHierarchy();
                }

                isClimbing = true;
            }
        }
    }

    void OnTriggerExit2D(Collider2D other) {
        if (other.tag == "Climbable") {
            isClimbing = false;
            climbableCollider = null;
        }
    }

    void OnTriggerStay(Collider other) {
        if (other.tag == "KillBox" && !isReceivingDamage) {
            KillCollider killCollider = other.GetComponent<KillCollider>();

            if (killCollider.damage > 0) {
                StopDamagingCoroutines();
                StartCoroutine(ReceiveDamage(killCollider.killSound, killCollider.deathMessages, -killCollider.damage));
            } else {
                Die(killCollider.killSound, killCollider.deathMessages);
            }
        }
    }

    void OnParticleCollision(GameObject other) {
        if (other.tag == "KillBox" && !isReceivingDamage && Generic.isControllable) {
            KillParticles killParticles = other.GetComponent<KillParticles>();

            if (killParticles.damage > 0) {
                StopDamagingCoroutines();
                StartCoroutine(ReceiveDamage(killParticles.killSound, killParticles.deathMessages, -killParticles.damage));
            } else {
                Die(killParticles.killSound, killParticles.deathMessages);
            }
        }
    }

    private void ResetAllBouncies() {
        Bouncy[] bouncies = FindObjectsOfType<Bouncy> ();
        foreach (Bouncy b in bouncies){
            b.bouncePower = b.originalBouncePower;
        }
    }

    private void Move() {
        if (isClimbing) {
            Climb();
        } else if (groundRaycast.isGrounded) {
            HandleGroundedMovement();
        } else {
            HandleAirborneMovement();
        }

        ClampVelocity();
        UpdateAnimator();
    }

    private void Climb() {
        if (Generic.isControllable) {
            rb2d.velocity = new Vector2(h * climbing.climbSpeed.x, v * climbing.climbSpeed.y);

            if (groundRaycast.isGrounded && v < 0) {
                isClimbing = false;
            }

            if (jump) {
                isClimbing = false;
                rb2d.velocity = new Vector2(rb2d.velocity.x, jumpPower);
                groundRaycast.distance = groundRaycast.originalDistance;
            }
        }
    }

    private void ClampVelocity() {
        if (maxSpeed.enableVelocityCap) {
            float x;
            float y;

            x = Mathf.Clamp(rb2d.velocity.x, -maxSpeed.MaxVelocity.x, maxSpeed.MaxVelocity.x);
            y = Mathf.Clamp(rb2d.velocity.y, -maxSpeed.MaxVelocity.y, maxSpeed.MaxVelocity.y);

            rb2d.velocity = new Vector2(x, y);
        }

        if (maxSpeed.logMySpeed) {
            Debug.Log(rb2d.velocity);
        }
    }

    private void UpdateAnimator() {
        anim.SetFloat("speed", Mathf.Abs(h));
        anim.SetFloat("facingDirection", h);
        anim.SetFloat("climbingDirection", v);

        anim.SetBool("isWallSliding", wallJump.enableWallJump && wallJump.wallRayCast.isWallSliding);

        anim.SetBool("isClimbing", isClimbing);

        if (!isClimbing) {
            anim.SetBool("isGrounded", groundRaycast.isGrounded);
        }

        if (groundRaycast.isGrounded) {
            anim.SetFloat("jump", rb2d.velocity.y);
        }
    }

    private void HandleAirborneMovement() {
        if (ropeSwing && ropeSwing.isSwinging) {
            ropeSwing.Swing();
        } else {
            // Multi-jump.
            if (jump && currentJump < multiJumpAllowance && Generic.isControllable) {
                rb2d.velocity = new Vector2(rb2d.velocity.x, jumpPower);
                currentJump++;
            }

            if (wallJump.enableWallJump) {
                DoWallJump();
            }

            if (wallJump.isRebounding) {
                rb2d.AddForce(new Vector2(h * wallJump.airSpeed, 0.0f));
            } else {
                rb2d.velocity = new Vector2(h * speed, rb2d.velocity.y);
            }

            // apply extra gravity from multiplier
            Vector3 extraGravityForce = (Physics.gravity * gravityMultiplier) - Physics.gravity;
            rb2d.AddForce(extraGravityForce);
        }

        groundRaycast.distance = rb2d.velocity.y < 0 ? groundRaycast.originalDistance : groundRaycast.ascendingDistance;
        anim.SetFloat("jump", rb2d.velocity.y);
    }

    private void HandleGroundedMovement() {
        rb2d.velocity = new Vector2(h * speed, rb2d.velocity.y);

        if (jump && Generic.isControllable) {
            rb2d.velocity = new Vector2(rb2d.velocity.x, jumpPower);
        }
    }

    public void DebugRaycasts(SpriteRenderer sr) {
        groundRaycast.isGrounded = IsGrounded();

        if (wallJump.enableWallJump) {
            wallJump.wallRayCast.isWallSliding = IsWallSliding(sr);
        }
    }

    private bool IsGrounded() {
        groundRaycast.spacing = (groundRaycast.width * 2) / groundRaycast.samplesDivisions;

        for (int i = 0; i <= groundRaycast.samplesDivisions; i++) {
            RaycastHit2D hit = Physics2D.Raycast(
                (Vector2)transform.position + ((Vector2)transform.up * groundRaycast.originOffset) + ((-(Vector2)transform.right * groundRaycast.width)) + ((Vector2)transform.right * (groundRaycast.spacing * i))
            , -(Vector2)transform.up
            , groundRaycast.distance,
                groundLayerMask
            );

            if (hit) {
#if UNITY_EDITOR
                Debug.DrawLine(
                    (Vector2)transform.position + ((Vector2)transform.up * groundRaycast.originOffset) + ((-(Vector2)transform.right * groundRaycast.width)) + ((Vector2)transform.right * (groundRaycast.spacing * i))
                ,   hit.point
                ,   Color.green);
#endif

                wallJump.isRebounding = false;
                currentJump = 0;
                if (ropeSwing) {
                    ropeSwing.ForgetRopeHierarchy();
                }

                ResetAllBouncies ();

                return true;
            } else {
#if UNITY_EDITOR
                Debug.DrawRay(
                    (Vector2)transform.position + ((Vector2)transform.up * groundRaycast.originOffset) + ((-(Vector2)transform.right * groundRaycast.width)) + ((Vector2)transform.right * (groundRaycast.spacing * i))
                ,   -(Vector2)transform.up * groundRaycast.distance
                ,   Color.red);
#endif
            }
        }

        return false;
    }

    private bool IsWallSliding(SpriteRenderer sr) {
        wallJump.wallRayCast.spacing = (wallJump.wallRayCast.span * 2) / wallJump.wallRayCast.samplesDivisions;

        for (int i = 0; i <= wallJump.wallRayCast.samplesDivisions; i++) {
            RaycastHit2D hit = Physics2D.Raycast(
                (Vector2)sr.bounds.center + (Vector2.up * wallJump.wallRayCast.originOffset) + (Vector2.down * wallJump.wallRayCast.span) + (Vector2.up * (wallJump.wallRayCast.spacing * i))
            ,   facingDirection
            , wallJump.wallRayCast.distance,
                wallLayerMask
            );

            if (hit) {
#if UNITY_EDITOR
                Debug.DrawLine(
                    (Vector2)sr.bounds.center + (Vector2.up * wallJump.wallRayCast.originOffset) + (Vector2.down * wallJump.wallRayCast.span) + (Vector2.up * (wallJump.wallRayCast.spacing * i))
                ,   hit.point
                ,   Color.blue);
#endif

                wallJump.isRebounding = false;
                currentJump = 0;
                if (ropeSwing) {
                    ropeSwing.ForgetRopeHierarchy();
                }

                return true;
            } else {
#if UNITY_EDITOR
                Debug.DrawRay(
                    (Vector2)sr.bounds.center + (Vector2.up * wallJump.wallRayCast.originOffset) + (Vector2.down * wallJump.wallRayCast.span) + (Vector2.up * (wallJump.wallRayCast.spacing * i))
                ,   facingDirection * wallJump.wallRayCast.distance
                ,   Color.black);
#endif
            }
        }

        return false;
    }

    private void DoWallJump() {
        if (wallJump.wallRayCast.isWallSliding && jump) {
            if (facingDirection == Vector2.left) {
                rb2d.velocity = ((Vector2.right) * wallJump.reboundPower.x) + (Vector2.up * wallJump.reboundPower.y);
                wallJump.isRebounding = true;
                h = 1.0f;

                if (wallJump.autoRebounding) {
                    facingDirection = Vector2.right;
                }
            } else if (facingDirection == Vector2.right) {
                rb2d.velocity = ((Vector2.left) * wallJump.reboundPower.x) + (Vector2.up * wallJump.reboundPower.y);
                wallJump.isRebounding = true;
                h = -1.0f;

                if (wallJump.autoRebounding) {
                    facingDirection = Vector2.left;
                }
            }
        }
    }

    public void StopDamagingCoroutines() {
        StopCoroutine("ReceiveDamage");
        StopCoroutine("Stun");

        Generic.isControllable = true;
        isReceivingDamage = false;
        spriteRenderer.color = spriteRendererColour;
    }

    public IEnumerator ReceiveDamage(AudioClip[] audioClips, PopUpTrigger.PopUpEvent[] deathMessages, int damage) {
        if (!isReceivingDamage) {
            isReceivingDamage = true;
            Generic.isControllable = false;
            Generic.UpdateHealth(damage);
            Generic.PlaySFX(audioClips);
            rb2d.velocity = Vector2.up * recoil.recoilVelocity;

            if (Generic.health <= 0) {
                if (Generic.lifeSystem && Generic.lives <= 1) {
                    GameOver();
                } else {
                    StartCoroutine(Respawn(deathMessages));
                }
            } else {
                StartCoroutine("Stun");

                yield return StartCoroutine(Invincible());

                Generic.playerspriteRenderer.color = spriteRendererColour;
                isReceivingDamage = false;
            }
        }
        yield return null;
    }

    private IEnumerator Invincible() {
        float singleFlickerDuration = 0.5f / recoil.flickerRate;
        float invincibilityTimer = 0.0f;

        while (invincibilityTimer < recoil.invincibilityTime) {
            float t = 0.0f;

            while (t < singleFlickerDuration) {
                Generic.playerspriteRenderer.color = Color.Lerp(
                    spriteRendererColour
                ,   Color.clear
                ,   t / singleFlickerDuration);

                invincibilityTimer += Time.deltaTime;
                if (invincibilityTimer >= recoil.invincibilityTime) {
                    break;
                }

                t += Time.deltaTime;

                yield return null;
            }

            Generic.playerspriteRenderer.color = Color.clear;

            t = 0.0f;

            while (t < singleFlickerDuration) {
                Generic.playerspriteRenderer.color = Color.Lerp(
                    Color.clear
                ,   spriteRendererColour
                ,   t / singleFlickerDuration);

                invincibilityTimer += Time.deltaTime;
                if (invincibilityTimer >= recoil.invincibilityTime) {
                    break;
                }

                t += Time.deltaTime;

                yield return null;
            }
            Generic.playerspriteRenderer.color = spriteRendererColour;

            yield return null;
        }
    }

    private IEnumerator Stun() {
        yield return new WaitForSeconds(recoil.stunnedTime);
        Generic.isControllable = true;
    }

    public void Die (AudioClip[] audioClips, PopUpTrigger.PopUpEvent[] deathMessages) {
        if (!isReceivingDamage) {
            isReceivingDamage = true;
            Generic.PlaySFX(audioClips);
            Generic.isControllable = false;
            rb2d.velocity = Vector2.up * 20;

            if (Generic.lifeSystem && Generic.lives <= 1) {
                GameOver();
            } else {
                StartCoroutine(Respawn(deathMessages));
            }
        }
    }

    void ResetBlockadePuzzlesInProgress() {
        foreach (PuzzleBlockade pb in FindObjectsOfType<PuzzleBlockade>()) {
            if (!pb.isCompleted && pb.switchAnimator.GetBool("active")) {
                pb.ResetBlockades();
            }
        }
    }

    public IEnumerator Respawn (PopUpTrigger.PopUpEvent[] deathMessages) {
        ResetBlockadePuzzlesInProgress();

        float transitionDuration = 0.25f;
        Generic.UpdateLives(-1);
        Generic.UpdateHealth(Generic.maxHealth);

        // Prepare the current camera.
        Camera cam = Camera.main;
        UnityStandardAssets.ImageEffects.ScreenOverlay scOverlay = cam.gameObject.AddComponent<UnityStandardAssets.ImageEffects.ScreenOverlay>();
        scOverlay.overlayShader = Shader.Find("Hidden/BlendModesOverlay");
        scOverlay.blendMode = UnityStandardAssets.ImageEffects.ScreenOverlay.OverlayBlendMode.Multiply;
        scOverlay.intensity = 2.0f;

        float t = 0.0f;

        // Fade out.
        while (t < transitionDuration) {
            scOverlay.intensity = Mathf.Lerp(
                2.0f,
                0.0f,
                (t / transitionDuration));

            t += Time.deltaTime;
            yield return null;
        }
        scOverlay.intensity = 0.0f;
        rb2d.velocity = Vector2.zero;

        // Actually dying.
        transform.position = Generic.spawnPoint ? Generic.spawnPoint.position : (Vector3)defaultPosAsSpawnPoint;
        isReceivingDamage = false;
        Generic.UpdateHealth(Generic.health - Generic.maxHealth);

        // Fade in.
        t = 0.0f;
        while (t < transitionDuration) {
            scOverlay.intensity = Mathf.Lerp(
                0.0f,
                2.0f,
                (t / transitionDuration));

            t += Time.deltaTime;
            yield return null;
        }
        scOverlay.intensity = 2.0f;

        Destroy(scOverlay);

        TriggerPopUp(deathMessages);
        Generic.isControllable = true;
    }

    private void TriggerPopUp(PopUpTrigger.PopUpEvent[] popUpEvents) {
        if (popUpEvents.Length > 0) {
            Generic.InterruptAllPopUps();

            PopUpTrigger.PopUpEvent[] newPopUpArray = {
                popUpEvents[UnityEngine.Random.Range(0, popUpEvents.Length)]
            };

            // Still a random chance that you end up spawning a dialogue.
            // Reduce the higher end of the random range to increase the odds.
            if (UnityEngine.Random.Range(0, 1) == 0) {
                if (displayPopUp != null) {
                    StopCoroutine(displayPopUp);
                }
                displayPopUp = StartCoroutine(Generic.DisplayPopUp(newPopUpArray));
            }
        }
    }

    private void GameOver() {
        Generic.TallyTotalScore();

        if (loadScene != null) {
            StopCoroutine(loadScene);
        }

        loadScene = StartCoroutine(Generic.LoadScene("GameOver"));
    }

    private void CheckDescension() {
        //If we're falling and not already dying
        if (Generic.deathByFallTime && rb2d.velocity.y < Generic.fallVelocityThreshold && !isReceivingDamage && !isClimbing && (ropeSwing && !ropeSwing.isSwinging)) {
            fallTime += Time.deltaTime;

            //if we fall a certain distance
            if (fallTime > Generic.maxFallTime) {
                Die(Generic.fallDeathSounds, Generic.fallDeathMessages);
            }
        } else {
            fallTime = 0;
        }
    }
}
