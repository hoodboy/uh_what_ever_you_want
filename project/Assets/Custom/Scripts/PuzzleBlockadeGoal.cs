﻿using UnityEngine;

public class PuzzleBlockadeGoal : MonoBehaviour {
    public PuzzleBlockade puzzleBlockade { get; set; }
    public PuzzleBlockadeUnit puzzleBlockadeUnit { get; set; }

	void Start(){
		SpriteRenderer sprRnd = GetComponent<SpriteRenderer>();
		sprRnd.enabled = false;
	}

    void OnTriggerEnter2D(Collider2D col2D) {
        if (col2D.tag == "Player" && puzzleBlockade.switchAnimator.GetBool("active") && !puzzleBlockadeUnit.goalHasBeenReached) {
            puzzleBlockadeUnit.StopAllCoroutines();
            puzzleBlockadeUnit.StartCoroutine("Rush");
        }
    }
}
