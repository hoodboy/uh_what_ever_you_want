﻿using UnityEngine;
using GameManager;
using System.Collections;

[AddComponentMenu("AnimCol/Misc/Pickup")]
public class PickUp : Events {
    [Header("Pick Up")]
    [Tooltip("The amount of lives awarded to the player for collecting this pickup.")]
    public int lives;
    [Tooltip("The amount by which health is adjusted when collecting this pickup.")]
    public int health;
    [Tooltip("The score awarded to the player for collecting this pickup.")]
    public int score;
    [Tooltip("An array of clips from which one is played when collecting this pickup.")]
    public AudioClip[] audioClips;
    [Tooltip("The particle system prefab that gets spawned at the location where this pickup is collected.")]
    public ParticleSystem collectionEffect;
    [Tooltip("Offset the spawn-position of the collection effect.")]
    public Vector2 spawnOffset;

    private bool isCollected;
    private Collider2D col;

    void Start() {
        Rigidbody2D rb2D = GetComponent<Rigidbody2D>();
        col = GetComponent<Collider2D>();

        if (rb2D) {
            rb2D.constraints = RigidbodyConstraints2D.FreezePositionX | RigidbodyConstraints2D.FreezeRotation;
        }

        col.isTrigger = !(rb2D != null);

    }

    void OnTriggerEnter2D(Collider2D other) {
        if (other.tag == "Player" && !isCollected) {
            StartCoroutine(ObtainPickup());
            featureConfig.SetPlayerFeatures();
        }
    }

    void OnCollisionEnter2D(Collision2D other) {
        if (other.transform.tag == "Player" && !isCollected) {
            StartCoroutine(ObtainPickup());
            featureConfig.SetPlayerFeatures();
        }
    }

    private IEnumerator ObtainPickup() {
        isCollected = true;

        Generic.UpdateLives  (lives);
        Generic.UpdateScore  (score);
        Generic.UpdateHealth (health);
        Generic.PlaySFX      (audioClips);

        if (collectionEffect) {
            Instantiate(collectionEffect, (Vector2)transform.position + spawnOffset, Quaternion.identity);
        }

        yield return StartCoroutine(TriggerEvents(false, null));

        Destroy(gameObject);
    }

    private void Log () {
        if (!col) Debug.Log(gameObject.name + ": No Collider2D has been assigned.");
    }
}
