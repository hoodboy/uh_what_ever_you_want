﻿using UnityEngine;

[AddComponentMenu("AnimCol/Character/EnemySpawnPoint")]
public class EnemySpawnPoint : MonoBehaviour {
    [Tooltip("The enemy prefab that spawns at the location of this spawn point object. The prefab should have the Enemy script attached to it.")]
    public Enemy enemy;

    private EnemyZone enemyZone;

    public void SpawnEnemy() {
        enemyZone = transform.parent.GetComponent<EnemyZone>();
        if (enemy) {
            enemy = (Enemy)Instantiate(enemy, transform.position, Quaternion.identity);
            enemyZone.enemies.Add(enemy);
            enemy.transform.SetParent(transform);
            enemy.patrolBoundaries = enemyZone.patrolBoundaries;
            enemy.enemyZone = enemyZone;
        }
    }

    void OnDrawGizmos(){
        Gizmos.color = new Color(1.0f, 0.0f, 0.0f, 0.5f);
        Gizmos.DrawCube(transform.position, Vector3.one);
    }
}
