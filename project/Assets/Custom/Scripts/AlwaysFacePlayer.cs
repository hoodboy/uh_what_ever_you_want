﻿using UnityEngine;

[AddComponentMenu("AnimCol/Misc/AlwaysFacePlayer")]
public class AlwaysFacePlayer : MonoBehaviour {
    [Tooltip("Enable this if your sprite was drawn facing right.")]
    public bool spriteFacingRightAlready = true;

    private SpriteRenderer spriteRenderer;

    void Start() {
        spriteRenderer = GetComponent<SpriteRenderer>();
    }

    void Update () {
        spriteRenderer.flipX =
            GameManager.Generic.player.transform.position.x < transform.position.x
        ?   (spriteFacingRightAlready ? true : false)
        :   (spriteFacingRightAlready ? false : true);
    }
}
