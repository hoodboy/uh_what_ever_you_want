﻿using UnityEngine;
using System.Collections;
using GameManager;

/// <summary>
/// Micellaneous manager for functions that run in update.
/// Really, this could be part of the Initializer, but I 
/// wanted to make it clear that the initializer is strictly
/// for setting up all the properties our game needs when it
/// starts. I do not want it to be applied manually, the 
/// initializer will take care of this.
/// </summary>

[AddComponentMenu("")]
public class Updater : MonoBehaviour {
    private bool inTransition = false;
    private Vector2 screenPoint = Vector2.zero;

    void Update() {
        PauseToggle();
        Generic.timeElapsed += Time.deltaTime;
    }

    void FixedUpdate() {
        UpdatePopUpPosition();
    }

    private void UpdatePopUpPosition() {
        if (Generic.popUpTarget && Generic.popUp) {
            screenPoint = (
                Vector2.Scale(
                    (Vector2)Camera.main.WorldToViewportPoint(
                        Generic.popUpTarget.transform.position 
                    +   new Vector3(
                            Generic.popUpOffset.x, 
                            Generic.heightOfPopUpTarget + Generic.popUpOffset.y,
                            0
                        )
                    ),
                    Generic.popUpCanvas.sizeDelta
                )
            ) - (Generic.popUpCanvas.sizeDelta * 0.5f);


            // Lerp.
            if (Generic.popUp.anchoredPosition != screenPoint && !inTransition) {
                StartCoroutine(LerpPopUpPosition());
            }

            // Non-lerp.
            //Generic.popUp.anchoredPosition = screenPoint;
        }
    }

    private IEnumerator LerpPopUpPosition() {
        inTransition = true;

        float t = 0.0f;

        while (Generic.popUp.anchoredPosition != screenPoint) {
            Generic.popUp.anchoredPosition = Vector2.Lerp(
                Generic.popUp.anchoredPosition, 
                screenPoint, t / 0.5f);

            t += Time.deltaTime;

            yield return null;
        }

        inTransition = false;
    }

    private void PauseToggle () {
        if(Input.GetButtonDown("Cancel") && Generic.isControllable) {
            UIEvents.TogglePause();
        }

        if ((Input.GetButton("Fire1") || Input.GetButton("Fire2") || Input.GetButton("Fire3")) && Time.timeScale == 0.0f) {
            Generic.defaultPauseButton.Select();
        }
    }
}
