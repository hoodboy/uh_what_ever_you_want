﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using GameManager;

[AddComponentMenu("AnimCol/UI/StartScreen")]
[RequireComponent(typeof(CoroutineCompanion))]
public class StartScreen : MonoBehaviour {
    [Serializable]
    public class MainMenu {
        [Tooltip("The animator resposible for clearing the Main Menu HUD when viewing the high scores table.")]
        public Animator mainMenuAnimator;
        [Tooltip("The button used to enter the game.")]
        public Button playButton;
        [Tooltip("The button used to view high scores.")]
        public Button highScoreButton;
        [Tooltip("The button used to quit the application.")]
        public Button quitButton;
    }

    [Serializable]
    public class HighScoreTable {
        [Tooltip("The animator resposible for showing the high scores table.")]
        public Animator highScoreAnimator;
        [Tooltip("The button used to return from viewing the highScores.")]
        public Button highScoreReturnButton;
    }

    [Tooltip("Main menu properties.")]
    public MainMenu mainMenu;
    [Tooltip("High Score properties.")]
    public HighScoreTable highScoreTable;

    private bool isTriggered;

    void Awake() {
        Generic.coroutineCompanion = GetComponent<CoroutineCompanion>();
        ButtonSetUp();
        mainMenu.playButton.Select();

        Log();
    }

    void Start() {
        StartCoroutine(Generic.SceneFadeIn());
    }

    void Update() {
        if (Input.GetButton("Fire1") || Input.GetButton("Fire2") || Input.GetButton("Fire3")) {
            mainMenu.playButton.Select();
        }
    }

    private void ButtonSetUp() {
        if (mainMenu.playButton) {
            mainMenu.playButton.onClick.RemoveAllListeners();
            mainMenu.playButton.onClick.AddListener(delegate { StartCoroutine(Generic.LoadScene("CSIntro")); });
            mainMenu.playButton.interactable = true;
        }

        if (mainMenu.highScoreButton) {
            mainMenu.highScoreButton.onClick.RemoveAllListeners();
            mainMenu.highScoreButton.onClick.AddListener(delegate { StartCoroutine(ToggleHighScoreMenu(true)); });
            mainMenu.highScoreButton.interactable = true;
        }

        if (mainMenu.quitButton) {
            mainMenu.quitButton.onClick.RemoveAllListeners();
            mainMenu.quitButton.onClick.AddListener(delegate { UIEvents.Quit(); });
            mainMenu.quitButton.interactable = true;
        }

        if (highScoreTable.highScoreReturnButton) {
            highScoreTable.highScoreReturnButton.onClick.RemoveAllListeners();
            highScoreTable.highScoreReturnButton.onClick.AddListener(delegate { StartCoroutine(ToggleHighScoreMenu(false)); });
            highScoreTable.highScoreReturnButton.interactable = false;
        }
    }

    private IEnumerator ToggleHighScoreMenu(bool state) {
        mainMenu.mainMenuAnimator.SetBool("active", !state);
        highScoreTable.highScoreAnimator.SetBool("active", state);

        while (highScoreTable.highScoreAnimator.IsInTransition(0)) {
            yield return null;
        }

        mainMenu.playButton.interactable = !state;
        mainMenu.quitButton.interactable = !state;
        mainMenu.highScoreButton.interactable = !state;
        highScoreTable.highScoreReturnButton.interactable = state;

        if (state) {
            highScoreTable.highScoreReturnButton.Select();
        }
        else {
            mainMenu.playButton.Select();
        }
    }

    void Log() {
        if (!mainMenu.mainMenuAnimator)            Debug.Log(gameObject.name + ": No 'Play' button has been assigned.");
        if (!highScoreTable.highScoreAnimator)     Debug.Log(gameObject.name + ": No 'Play' button has been assigned.");
        if (!mainMenu.playButton)                  Debug.Log(gameObject.name + ": No 'Play' button has been assigned.");
        if (!mainMenu.highScoreButton)             Debug.Log(gameObject.name + ": No 'High Score' button has been assigned.");
        if (!mainMenu.quitButton)                  Debug.Log(gameObject.name + ": No 'Quit' button has been assigned.");
        if (!highScoreTable.highScoreReturnButton) Debug.Log(gameObject.name + ": No 'High Score Return Button' button has been assigned.");
    }
}
