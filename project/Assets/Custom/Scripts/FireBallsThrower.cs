﻿using UnityEngine;
using System.Collections;

public class FireBallsThrower : MonoBehaviour {
    public Rigidbody2D fireBall;
    public float speed;
    public float frequency;
    public Vector2 rangeOfFrequency = new Vector2(1.0f, 0.0f);

    // Use this for initialization
    void Start () {
        StartCoroutine(Timer());
    }
	
	// Update is called once per frame
	void Update () {
	
	}

    private IEnumerator Timer() {
        while (true) {
            SpawnObject();
            yield return new WaitForSeconds(Random.Range(rangeOfFrequency.x, rangeOfFrequency.y));
        }
    }

    void SpawnObject() {
        Rigidbody2D rb = (Rigidbody2D)Instantiate(fireBall, transform.position, Quaternion.identity);
        rb.velocity = transform.up * 5f;
    }
}
