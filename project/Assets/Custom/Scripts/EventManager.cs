﻿using UnityEngine;
using System.Collections;

namespace Init {
    public class EventManager : MonoBehaviour {
        public delegate IEnumerator Hit(AudioClip[] audioClips, PopUpTrigger.PopUpEvent[] deathMessages, int damage);
        public static event Hit OnHitPlayer;

        public delegate void Die(AudioClip[] audioClips, PopUpTrigger.PopUpEvent[] deathMessages);
        public static event Die OnDiePlayer;

        public void HitPlayer(AudioClip[] audioClips, PopUpTrigger.PopUpEvent[] deathMessages, int damage) {
            Hit hit = OnHitPlayer;
            if (OnHitPlayer != null) {
                StartCoroutine(hit(audioClips, deathMessages, -damage));
            }
        }

        public void DiePlayer(AudioClip[] audioClips, PopUpTrigger.PopUpEvent[] deathMessages) {
            if (OnDiePlayer != null) {
                OnDiePlayer(audioClips, deathMessages);
            }
        }
    }
}
