﻿using UnityEngine;
using System;
using System.Collections.Generic;

[RequireComponent(typeof(BoxCollider2D))]
[AddComponentMenu("AnimCol/Character/EnemyZone")]
public class EnemyZone : MonoBehaviour {
    [Serializable]
    public struct PatrolBoundaries {
        public Vector2 min;
        public Vector2 max;
    }

    [HideInInspector]
    public List<Enemy> enemies;

    [NonSerialized]
    public PatrolBoundaries patrolBoundaries;

    void Awake() {
        BoxCollider2D boxCol2D = GetComponent<BoxCollider2D>();
        boxCol2D.isTrigger = true;
        patrolBoundaries.min = boxCol2D.bounds.min;
        patrolBoundaries.max = boxCol2D.bounds.max;
        
        foreach (Transform esp in transform) {
            if (esp.GetComponent<EnemySpawnPoint>()) {
                esp.GetComponent<EnemySpawnPoint>().SpawnEnemy();
            }
        }
    }

    void OnTriggerEnter2D(Collider2D other) {
        if (other.tag == "Player") {
            foreach (Enemy enemy in enemies) {
                if (enemy.aIBehaviour.agroNearbyPlayer) {
                    enemy.StopAllCoroutines();
                    enemy.StartCoroutine("Agro");
                }
            }
        }
    }

    void OnTriggerExit2D(Collider2D other) {
        if (other.tag == "Player") {
            foreach (Enemy enemy in enemies) {
                enemy.StopAllCoroutines();
                enemy.StartCoroutine("MainLoop");
            }
        }
    }
}
