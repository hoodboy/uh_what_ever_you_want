﻿using UnityEngine;
using System.Collections;
using GameManager;

[ExecuteInEditMode]
public class PuzzleBlockadeUnit : MonoBehaviour {
    public Vector3 startPos;
    public Vector3 endPos;
    public float delay;
    public float duration;
    public bool beginOnEndPos;
    public PuzzleBlockadeGoal goal;
    public Color debugColor = Color.magenta;

    public bool goalHasBeenReached { get; set; }
    public PuzzleBlockade puzzleBlockade { get; set; }

    private float rushDuration = 0.25f;

    void Start() {
        goal.puzzleBlockadeUnit = this;
        goal.puzzleBlockade = puzzleBlockade;
        goal.GetComponent<Collider2D>().isTrigger = true;
    }

#if UNITY_EDITOR
    void Update() {
        Debug.DrawLine(transform.position, goal.transform.position, debugColor);
    }
#endif

    public IEnumerator Travel() {
        if (!goalHasBeenReached) {
            float t = 0;

            if (beginOnEndPos) {
                while (t < rushDuration) {
                    transform.position = Vector2.Lerp(
                        endPos,
                        startPos,
                        t / rushDuration
                    );

                    t += Time.deltaTime;
                    yield return null;
                }
            }

            transform.position = startPos;
            t = 0;

            while (t < duration) {
                transform.position = Vector2.Lerp(
                    startPos,
                    endPos,
                    t / duration
                );

                t += Time.deltaTime;
                yield return null;
            }

            transform.position = endPos;

            if (!goalHasBeenReached) {
                puzzleBlockade.StopAllCoroutines();
                puzzleBlockade.StartCoroutine("RevertBlockades");
            }
        }
    }

    public IEnumerator Revert() {
        float t = 0;
        Vector2 pos = transform.position;

        while (t < rushDuration) {
            transform.position = Vector2.Lerp(
                pos,
                beginOnEndPos ? endPos: startPos,
                t / rushDuration
            );

            t += Time.deltaTime;
            yield return null;
        }

        transform.position = beginOnEndPos ? endPos: startPos;
        goalHasBeenReached = false;
    }

    public IEnumerator Rush() {
        goalHasBeenReached = true;

        bool puzzleIsComplete = true;
        foreach (PuzzleBlockadeUnit pbu in puzzleBlockade.puzzleBlockadeUnits) {
            if (!pbu.goalHasBeenReached) {
                puzzleIsComplete = false;
                break;
            }
        }

        if (puzzleIsComplete) {
            Generic.ToggleAnimators(puzzleBlockade.animators);
            puzzleBlockade.animatorsShouldBeToggled = false;
            puzzleBlockade.isCompleted = true;
        }

        float t = 0;
        Vector2 pos = transform.position;

        while (t < rushDuration) {
            transform.position = Vector2.Lerp(
                pos,
                endPos,
                t / rushDuration
            );

            t += Time.deltaTime;
            yield return null;
        }

        transform.position = endPos;
    }
}
