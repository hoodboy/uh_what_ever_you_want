﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using GameManager;

[AddComponentMenu("AnimCol/Character/RopeSwing")]
public class RopeSwing : MonoBehaviour {
    [Serializable]
    public class RopeItem {
        public Transform ropeTransform;
        public Rigidbody2D ropeRigidbody;
        public float ropeGravityScale;
        public float ropeDrag;
    }

    [Tooltip("The amount of horizontal force to apply while swinging and pressing left/right.")]
    public float swingPower = 10.0f;
    [Tooltip("The amount of force you can apply when mid-way through the air after leaping from a rope.")]
    public float midLeapForce = 20.0f;
    [Tooltip("Sometimes you need a little help to get off the rope. These values will multiply your velocity in X and Y when you attempt to jump from the rope.")]
    public float initialLeapBoost = 6.0f;
    [Tooltip("Pressing up/down while on a rope will attempt to cease rope movement so that you can climb it. The ceased movement is achieved in part by modifying the gravity scale on each segment of the rope. Adjust this value to decide how much gravity is applied during this step.")]
    public float assertiveGravity = 30.0f;
    [Tooltip("Pressing up/down while on a rope will attempt to cease rope movement so that you can climb it. The ceased movement is achieved in part by modifying the linear on each segment of the rope. Adjust this value to decide how much drag is applied during this step.")]
    public float assertiveDrag = 10.0f;
    [Tooltip("Catching a rope on contact will maintain your character's position on the brink of the rope colliders. To remedy this problem, we delay the 'catching' by a number of frames specified by this property. A value of '1' is usually enough.")]
    public int catchDelay = 1;

    public bool isSwinging { get; set; }

    private bool isLeapingFromRope;
    private HingeJoint2D hingeJoint2d;
    private bool playerRb2dAutoMass;
    private float playerRb2dMass;
    private List<RopeItem> currentRopeHierarchy;
    private List<Collider2D> colliders = new List<Collider2D>();

    public void RememberThisRopeHierarchy() {
        currentRopeHierarchy = new List<RopeItem>();

        foreach (HingeJoint2D hj2d in hingeJoint2d.connectedBody.transform.parent.GetComponentsInChildren<HingeJoint2D>()) {
            RopeItem tempRopeItem = new RopeItem();
            tempRopeItem.ropeTransform = hj2d.transform;
            tempRopeItem.ropeRigidbody = hj2d.GetComponent<Rigidbody2D>();
            tempRopeItem.ropeGravityScale = tempRopeItem.ropeRigidbody.gravityScale;
            tempRopeItem.ropeDrag = tempRopeItem.ropeRigidbody.drag;

            currentRopeHierarchy.Add(tempRopeItem);
        }
    }

    public void ForgetRopeHierarchy() {
        isSwinging = false;
        isLeapingFromRope = false;
        currentRopeHierarchy = null;
    }

    void OnTriggerStay2D(Collider2D other) {
        if (other.tag == "Rope") {
            Catch(other);
        }
    }

    void OnTriggerExit2D(Collider2D other) {
        if (other.tag == "Rope") {
            colliders.Remove(other);
            if (colliders.Count == 0) {
                RopeRelease();
            }
        }
    }

    private void Catch(Collider2D other) {
        StartCoroutine(DelayCatch(other));

        foreach (Collider2D col2D in colliders) {
            if (col2D == other) {
                return;
            }
        }

        colliders.Add(other);
    }

    private IEnumerator DelayCatch(Collider2D other) {
        for (int i = 0; i < catchDelay; i++) {
            yield return null;
        }

        RopeCatch(other);
    }

    public void RopeSwingInitialization() {
        playerRb2dAutoMass = Generic.player.rb2d.useAutoMass;
        playerRb2dMass = Generic.player.rb2d.mass;

        hingeJoint2d = Generic.player.GetComponent<HingeJoint2D>();

        if (!hingeJoint2d) {
            hingeJoint2d = Generic.player.gameObject.AddComponent<HingeJoint2D>();
        }

        hingeJoint2d.enabled = false;
    }

    public void RopeCatch(Collider2D other) {
        hingeJoint2d.connectedBody = other.GetComponent<Rigidbody2D>();

        if (currentRopeHierarchy != null && currentRopeHierarchy.Count > 0) {
            foreach (RopeItem ri in currentRopeHierarchy) {
                if (other.transform == ri.ropeTransform) {
                    return;
                }
            }
        }

        hingeJoint2d.connectedBody.MoveRotation(0);
        ForgetRopeHierarchy();
        Generic.player.currentJump = Generic.player.multiJumpAllowance - 1;
        isSwinging = true;
        Generic.player.rb2d.useAutoMass = false;
        Generic.player.rb2d.mass = 0.5f;
        hingeJoint2d.enabled = true;
        RememberThisRopeHierarchy();
    }

    public void RopeRelease() {
        if (!isLeapingFromRope) {
            isLeapingFromRope = true;

            Generic.player.rb2d.useAutoMass = playerRb2dAutoMass;

            if (!playerRb2dAutoMass) {
                Generic.player.rb2d.mass = playerRb2dMass;
            }

            hingeJoint2d.connectedBody = null;
            hingeJoint2d.enabled = false;

            Generic.player.rb2d.velocity = new Vector2(Generic.player.rb2d.velocity.x, initialLeapBoost);
            Generic.player.currentJump++;
        }
    }

    public void Swing() {
        if (Generic.player.jump && Generic.player.currentJump < Generic.player.multiJumpAllowance) {
            RopeRelease();
            return;
        }

        if (Generic.player.v != 0 && !isLeapingFromRope) {
            RopeClimb();
            return;
        }

        if (hingeJoint2d.connectedBody) {
            Generic.player.rb2d.AddForce(new Vector3(Generic.player.h, 0, 0) * swingPower);
        } else {
            Generic.player.rb2d.AddForce(new Vector3(Generic.player.h, 0, 0) * midLeapForce);
        }

        foreach (RopeItem ri in currentRopeHierarchy) {
            ri.ropeRigidbody.gravityScale = ri.ropeGravityScale;
            ri.ropeRigidbody.drag = ri.ropeDrag;
        }

        if (!isLeapingFromRope) {
            hingeJoint2d.enabled = true;
            Generic.player.rb2d.gravityScale = Generic.player.originalGravityScale;
        }
    }

    void RopeClimb() {
        foreach (RopeItem ri in currentRopeHierarchy) {
            ri.ropeRigidbody.gravityScale = assertiveGravity;
            ri.ropeRigidbody.drag = assertiveDrag;
        }

        foreach (RopeItem ri in currentRopeHierarchy) {
            if (!(Mathf.Abs(ri.ropeTransform.localEulerAngles.z) < 3f || Mathf.Abs(ri.ropeTransform.localEulerAngles.z) > 357f)) {
                return;
            }
        }

        hingeJoint2d.enabled = false;
        Generic.player.rb2d.gravityScale = 0.0f;
        Generic.player.rb2d.velocity = new Vector2(0.0f, Generic.player.v * Generic.player.climbing.climbSpeed.y);
    }
}
