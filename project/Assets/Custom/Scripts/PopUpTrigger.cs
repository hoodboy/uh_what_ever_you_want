﻿using UnityEngine;
using GameManager;
using System;

[AddComponentMenu("AnimCol/Events/PopUpTrigger")]
public class PopUpTrigger : MonoBehaviour {
    [Serializable]
    public class PopUpEvent {
        [Tooltip("The Text component that renders the popoUp text.")]
        public string popUpText;
        [Tooltip("The amount of time in seconds for which the popUp is displayed.")]
        public float duration = 3.0f;
        [Tooltip("The transform above which the pop-up will be displayed. If none is specified, the Player character will be used.")]
        public Transform target;
    }

    [Tooltip("A series of PopUps that appear when this event is triggered.")]
    public PopUpEvent[] popUpEvents;
    [Tooltip("Whether or not this PopUp can be triggered repeatedly, or should fire only once.")]
    public bool fireOnceOnly = true;

    private bool hasFired = false;
    private Coroutine myCoroutine;

    private void Start() {
        Generic.CheckForCollider(gameObject);
    }

    void OnTriggerEnter2D(Collider2D other) {
        if (other.GetComponent<Player>()) {
            if (!hasFired) {
                if (fireOnceOnly) {
                    hasFired = true;
                }

                Generic.InterruptAllPopUps();

                myCoroutine = StartCoroutine(Generic.DisplayPopUp(popUpEvents));
            }
        }
    }

     public void InterruptCoroutine() {
        if (myCoroutine != null) {
            StopCoroutine(myCoroutine);
        }
    }
}
