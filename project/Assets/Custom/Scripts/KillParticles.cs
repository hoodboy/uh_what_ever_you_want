﻿using UnityEngine;

[RequireComponent(typeof(ParticleSystem))]
[AddComponentMenu("AnimCol/Misc/KillParticles")]
public class KillParticles : MonoBehaviour {
    [Tooltip("The amount of damage applied when this KillCollider collides with the player.")]
    public int damage;
    [Tooltip("An array of clips from which one is played when collecting this pickup.")]
    public AudioClip[] killSound;
    [Tooltip("The potential messages that can be played when you die from this KillCollider.")]
    public PopUpTrigger.PopUpEvent[] deathMessages;

    void Start () {
        gameObject.tag = "KillBox";

        ParticleSystem ps = GetComponent<ParticleSystem>();
        ParticleSystem.CollisionModule psCol = ps.collision;
        psCol.enabled = true;
        psCol.type = ParticleSystemCollisionType.World;
        psCol.mode = ParticleSystemCollisionMode.Collision2D;
        psCol.enableDynamicColliders = true;
        psCol.sendCollisionMessages = true;
    }
}
