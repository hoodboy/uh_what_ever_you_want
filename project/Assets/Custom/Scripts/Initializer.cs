﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections.Generic;
using GameManager;

namespace Init {
    [Serializable]
    public class Features {
        [Tooltip("Disable this and you will no longer have the score HUD appear, and gem collecting will no longer be a requirement to complete a level. The Game Over and Game Completion screens will no longer display a score. High score features should also be disabled in the Start Menu.")]
        public bool scoreSystem = true;
        [Tooltip("Losing all lives will result in a transition to the Game Over screen. The Game Over screen is only reached in the case that this feature is enabled. Disable this feature if you want to be able to die an infinite amount of times without any consequence. The life gauge parent will be hidden when the game starts, if you haven't done this already.")]
        public bool lifeSystem = true;
        [Tooltip("If this is set to zero, the health system will be disabled completely.")]
        public int startHealth = 0;
    }

    [Serializable]
    public class PauseMenu {
        [Serializable]
        public class Buttons {
            [Tooltip("The object you wish to trigger an animation for. It must have an Animator component attached.")]
            public Button resumeButton;
            [Tooltip("The state that should be triggered from your selected animator. Check your state within the animator to find this name. If the name is not found or specified, then we will look for an 'active' bool parameter in the 'triggeredAnimator'.")]
            public Button quitButton;
        }

        [Tooltip("The game object that gets toggled ective / inactive when pressing the 'Option' button. This is usually the parent of all Pause Menu elements.")]
        public RectTransform pauseMenu;
        [Tooltip("The buttons used for your Pause menu.")]
        public Buttons pauseMenuButtons;
    }

    [Serializable]
    public class Hud {
        [Tooltip("The UI element to which all the lives are parented to.")]
        public RectTransform lifeGaugeParent;
        [Tooltip("The UI element that animates from empty to full, displaying your percentage of health remaining.")]
        public RectTransform healthGauge;
        [Tooltip("The UI element that animates from empty to full, displaying your percentage of level completion.")]
        public RectTransform scoreGauge;
        [Tooltip("Usually the ultimate parent of the score gauge. Capable of animating anything within it's hierarchy. This will play the transition to an animation via the 'active' bool parameter when the score has reached the target score.")]
        public Animator scoreGaugeAnimator;
    }

    [Serializable]
    public class Falling {
        [Tooltip("A list of sounds that can be played in the event that the player character dies from falling.")]
        public AudioClip[] fallDeathSounds;
        [Tooltip("The potential messages that can be played when you die from falling.")]
        public PopUpTrigger.PopUpEvent[] fallDeathMessages;
        [Tooltip("Uncheck this if you would rather manage all of your death using KillColliders instead,useful if automated fall detection just isn't working out for you.")]
        public bool deathByFallTime = false;
        [Tooltip("The velocity you should hit before activating the fall timer. Positive or negative values have an identical effect.")]
        public float fallVelocityThreshold = -1.0f;
        [Tooltip("The amount of time that the player character should fall before being killed.")]
        public float maxFallTime = 1.0f;
    }

    [Serializable]
    public class DialogueProperties {
        [Tooltip("The UI element that has the animator responsible for the active / inactive states of the dialogue UI.")]
        public Animator dialogueAnimator;
        [Tooltip("The UI element responsible for displaying dialogue text.")]
        public Text dialogueText;
        [Tooltip("The UI element that renders the avatar image for this dialogue text.")]
        public Image dialogueAvatarImage;
        [Tooltip("The UI element that renders the avatar's name for this dialogue text.")]
        public Text dialogueAvatarName;
        [Tooltip("The choice box that is enabled / disabled depending on whether or not the dialogue was triggered by 'ChoiceTrigger'.")]
        public RectTransform choiceBox;
        [Tooltip("The button that represents choice 'A' when presented with a choice.")]
        public Button choiceButtonA;
        [Tooltip("The button that represents choice 'B' when presented with a choice.")]
        public Button choiceButtonB;
    }

    [Serializable]
    public class PopUp {
        [Tooltip("The parent of all relevant pop-up elements. It's toggled on-and off for pop-up events.")]
        public RectTransform popUp;
        [Tooltip("The text element that renders the pop-up text.")]
        public Text popUpText;
        [Tooltip("By default, the pop-up is positioned at the head, or central-height of a collider, sometimes a gap is needed for better viewing, if so, adjust this.")]
        public Vector2 offset;
    }

    [Serializable]
    public class Misc {
        [Tooltip("You should have one canvas in your scene that holds all of your UI, sometimes it gets switched off while you're working. If you had to switch yours off, this initializer will do you a favour, and remember to turn it back on for you when your game begins... as long as you've remembered to drag it in to this slot.")]
        public Canvas mainCanvas;
        [Tooltip("Set a transform to use as the spawnPoint when the game begins. This will be over-written if your player character begins the game \ninside a spawn point.")]
        public Transform initialSpawnPoint;
        [Tooltip("The gameObject from which your background music is playing.")]
        public AudioSource bgMusic;
        [Tooltip("One of the score multipliers when a level is completed is based on level completion time. This value describes how many minutes the player must complete the level in, in order to receive the bonus score.")]
        public float idealCompletionTimeInMinutes = 5;
        [Tooltip("A list of transform objects which have their transform data stored upon entering this spawn point. Upon death, these objects have their transform data restored.")]
        public Transform[] objectsToReset;
    }


    [AddComponentMenu("AnimCol/SceneManagers/Initializer")]
    [RequireComponent(typeof(AudioSource))]
    [RequireComponent(typeof(CoroutineCompanion))]
    [RequireComponent(typeof(EventManager))]
    [RequireComponent(typeof(Updater))]
    public class Initializer : MonoBehaviour {
        [Tooltip("Enable or disable certain features in the game.")]
        public Features features;
        [Tooltip("A collection of properties concerning the Pause menu.")]
        public PauseMenu pauseMenu;
        [Tooltip("A collection of properties concerning the score gauge and life-meter.")]
        public Hud hud;
        [Tooltip("A collection of properties concerning the option where fall-death is calculated by the amount of time spent falling.")]
        public Falling falling;
        [Tooltip("A collection of properties concerning the Dialogue UI.")]
        public DialogueProperties dialogue;
        [Tooltip("A collection of properties concerning the pop-up dialogue UI.")]
        public PopUp popUp;
        [Tooltip("A collection of properties with general effects on the game.")]
        public Misc misc;

        private List<Image> lifeIcons = new List<Image>();
        private float cutSceneTransitionDuration = 0.5f;

        void Awake () {
            // Just in-case somebody turned off their canvas.
            if (misc.mainCanvas) {
                misc.mainCanvas.gameObject.SetActive(true);
            }

            // Features.
            Generic.scoreSystem = features.scoreSystem;
            Generic.lifeSystem = features.lifeSystem;
            Generic.health = features.startHealth;

            // Cameras.
            CameraSetUp();

            // Player
            Generic.player = FindObjectOfType<Player>();
            Generic.playerspriteRenderer = Generic.player.GetComponent<SpriteRenderer>();

            // Lives.
            if (Generic.lifeSystem && hud.lifeGaugeParent) {
                foreach (Transform t in hud.lifeGaugeParent) {
                    lifeIcons.Add(t.GetChild(0).GetComponent<Image>());
                }

                Generic.lives = lifeIcons.Count > 0 ? lifeIcons.Count : 5;
                Generic.maxLives = Generic.lives;
                Generic.lifeIcons = lifeIcons.Count > 0 ? lifeIcons : null;
            } else if (hud.lifeGaugeParent) {
                Destroy(hud.lifeGaugeParent.gameObject);
            }

            // Health.
            if (Generic.health > 0) {
                Generic.maxHealth = Generic.health;
                Generic.healthGauge = hud.healthGauge;
                Generic.healthGaugeWidth = hud.healthGauge ? hud.healthGauge.rect.width : 0;
            } else if (hud.healthGauge) {
                Destroy(hud.healthGauge.transform.parent.gameObject);
            }

            // Score.
            if (Generic.scoreSystem) {
                Generic.score = 0;
                Generic.targetScore = CalculateTargetScore();
                Generic.scoreGauge = hud.scoreGauge;
                Generic.scoreGaugeAnimator = hud.scoreGaugeAnimator;
                Generic.scoreGaugeWidth = hud.scoreGauge ? hud.scoreGauge.rect.width : 0;
            } else if (hud.scoreGaugeAnimator) {
                Destroy(hud.scoreGaugeAnimator.gameObject);
            }

            // Dialogue.
            Generic.dialogueAnimator = dialogue.dialogueAnimator;
            Generic.dialogueAudioSource = dialogue.dialogueAnimator ? dialogue.dialogueAnimator.gameObject.AddComponent<AudioSource>() : null;
            Generic.dialogueText = dialogue.dialogueText;
            Generic.dialogueAvatarImage = dialogue.dialogueAvatarImage;
            Generic.dialogueAvatarName = dialogue.dialogueAvatarName;
            Generic.choiceBox = dialogue.choiceBox;
            Generic.choiceButtonA = dialogue.choiceButtonA;
            Generic.choiceButtonB = dialogue.choiceButtonB;

            // PopUpDialogue.
            Generic.popUp = popUp.popUp;

            if (popUp.popUp) {
                Generic.popUpCanvas = popUp.popUp.transform.root.GetComponent<Canvas>() ? popUp.popUp.transform.root.GetComponent<RectTransform>() : null;
                Generic.popUpText = popUp.popUpText;
                Generic.popUpTextDummy = popUp.popUpText && popUp.popUpText.rectTransform.GetChild(0).GetComponent<Text>() ? popUp.popUpText.rectTransform.GetChild(0).GetComponent<Text>() : null;

                if (popUp.popUpText) {
                    Generic.popUpTextDummy.font = popUp.popUpText.font;
                    Generic.popUpTextDummy.fontSize = popUp.popUpText.fontSize;
                }

                Generic.popUpOffset = popUp.offset;
            }

            // Pause.
            Generic.pauseMenu = pauseMenu.pauseMenu;
            Generic.bgMusic = misc.bgMusic;
            Generic.defaultPauseButton = pauseMenu.pauseMenuButtons.resumeButton;
            if (Generic.defaultPauseButton) {
                pauseMenu.pauseMenuButtons.resumeButton.onClick.RemoveAllListeners();
                pauseMenu.pauseMenuButtons.resumeButton.onClick.AddListener(delegate {  UIEvents.TogglePause(); });
            }
            if (pauseMenu.pauseMenuButtons.quitButton) {
                pauseMenu.pauseMenuButtons.quitButton.onClick.RemoveAllListeners();
                pauseMenu.pauseMenuButtons.quitButton.onClick.AddListener(delegate {    UIEvents.ReturnToStartScreen(); });
            }

            // Falling parameters.
            if (falling.fallVelocityThreshold > 0) {
                falling.fallVelocityThreshold = -falling.fallVelocityThreshold;
            }

            Generic.fallDeathSounds = falling.fallDeathSounds.Length > 0 ? falling.fallDeathSounds : new AudioClip[0];
            Generic.fallDeathMessages = falling.fallDeathMessages.Length > 0 ? falling.fallDeathMessages : new PopUpTrigger.PopUpEvent[0];
            Generic.deathByFallTime = falling.deathByFallTime;
            Generic.fallVelocityThreshold = falling.fallVelocityThreshold;
            Generic.maxFallTime = falling.maxFallTime;

            // Misc.
            Generic.timeElapsed = 0;
            Generic.totalScore = 0;
            Generic.spawnPoint = misc.initialSpawnPoint;
            Generic.sfx = GetComponent<AudioSource>();
            Generic.cutSceneTransitionDuration = cutSceneTransitionDuration * 0.5f;
            Generic.coroutineCompanion = GetComponent<CoroutineCompanion>();
            Generic.rewardBonusCompletionTime = misc.idealCompletionTimeInMinutes * 60;
            Generic.SetObjectsToReset(misc.objectsToReset);
            Generic.eventManager = GetComponent<EventManager>();

            Log();
        }

        void Start() {
            Generic.UpdateLives(0);
            Generic.UpdateScore (0);
            Generic.UpdateHealth (0);
            Generic.isControllable = true;
            StartCoroutine(Generic.SceneFadeIn());
        }

        private int CalculateTargetScore() {
            PuzzleRotation[] puzzleRotations = (PuzzleRotation[])FindObjectsOfType(typeof(PuzzleRotation));
            EventsTrigger[] eventTriggers = (EventsTrigger[])FindObjectsOfType(typeof(EventsTrigger));
            EnemySpawnPoint[] enemySpawnPoints = (EnemySpawnPoint[])FindObjectsOfType(typeof(EnemySpawnPoint));
            PickUp[] pickups = (PickUp[])FindObjectsOfType(typeof(PickUp));

            int tally = new int();

            // Calculate score from Pickup objects.
            foreach (PickUp p in pickups) {
                tally += p.score;
            }

            // Extract score from PuzzleRotation objects.
            if (puzzleRotations != null) {
                foreach (PuzzleRotation pr in puzzleRotations) {
                    foreach (Events.ObjectsToSpawn ots in pr.events.objectsToSpawn) {
                        if (ots.objectToSpawn && ots.objectToSpawn.GetComponent<PickUp>()) {
                            tally += ots.objectToSpawn.GetComponent<PickUp>().score;
                        }
                    }

                    // Remember to search thr cutscenes as well.
                    foreach (Events.CutScenes cs in pr.cutScenes) {
                        foreach (Events.ObjectsToSpawn ots in cs.advancedEvents.objectsToSpawn)
                        if (ots.objectToSpawn.GetComponent<PickUp>()) {
                            tally += ots.objectToSpawn.GetComponent<PickUp>().score;
                        }
                    }
                }
            }

            // Extract score from EventTrigger objects.
            foreach (EventsTrigger et in eventTriggers) {
                foreach (Events.ObjectsToSpawn ots in et.events.objectsToSpawn) {
                    if (ots.objectToSpawn.GetComponent<PickUp>()) {
                        tally += ots.objectToSpawn.GetComponent<PickUp>().score;
                    }
                }

                // Remember to search thr cutscenes as well.
                foreach (Events.CutScenes cs in et.cutScenes) {
                    foreach (Events.ObjectsToSpawn ots in cs.advancedEvents.objectsToSpawn)
                    if (ots.objectToSpawn && ots.objectToSpawn.GetComponent<PickUp>()) {
                        tally += ots.objectToSpawn.GetComponent<PickUp>().score;
                    }
                }
            }

            // Extract from enemies.
            foreach (EnemySpawnPoint esp in enemySpawnPoints) {
                tally += esp.enemy.whenEnemyDies.pickupToSpawn.score;
            }

            return tally;
        }

        private void CameraSetUp() {
            foreach (Camera cam in FindObjectsOfType<Camera>()) {
                if (cam == Camera.main) {
                    cam.enabled = true;
                    if (cam.GetComponent<AudioListener>()) {
                        cam.GetComponent<AudioListener>().enabled = true;
                    }
                } else {
                    cam.enabled = false;
                    if (cam.GetComponent<AudioListener>()) {
                        cam.GetComponent<AudioListener>().enabled = false;
                    }
                }
            }
        }

        private void Log() {
            if (!misc.mainCanvas) Debug.Log(gameObject.name + ": The main canvas has not been applied to misc.mainCanvas.");
            if (!Generic.player)        Debug.Log(gameObject.name + ": No object tagged as 'Player' was found in the scene.");
            if (Generic.lifeSystem && !hud.lifeGaugeParent)   Debug.Log(gameObject.name + ": 'lifeGaugeParent' has not been assigned.");
            if (Generic.lifeSystem && !(lifeIcons.Count > 0)) Debug.Log(gameObject.name + ": No 'lifeIcons' with Image componenent were found beneath the 'lifeGaugeParent'.");

            if (Generic.scoreSystem && !hud.scoreGauge)         Debug.Log(gameObject.name + ": The 'scoreGauge' has not been assigned.");
            if (Generic.scoreSystem && !hud.scoreGaugeAnimator) Debug.Log(gameObject.name + ": The 'scoreGaugeAnimator' has not been assigned.");
            if (features.startHealth > 0 && !hud.healthGauge)   Debug.Log(gameObject.name + ": The 'healthGauge' has not been assigned.");
            if (!dialogue.dialogueAnimator)    Debug.Log(gameObject.name + ": The 'dialogueAnimator' has not been assigned.");
            if (!dialogue.dialogueText)        Debug.Log(gameObject.name + ": The 'dialogueText' has not been assigned.");
            if (!dialogue.dialogueAvatarImage) Debug.Log(gameObject.name + ": The 'dialogueAvatarImage' has not been assigned.");
            if (!dialogue.choiceBox)           Debug.Log(gameObject.name + ": The 'choiceBox' has not been assigned.");
            if (!dialogue.choiceButtonA)       Debug.Log(gameObject.name + ": The 'choiceButtonA' has not been assigned.");
            if (!dialogue.choiceButtonB)       Debug.Log(gameObject.name + ": The 'choiceButtonB' has not been assigned.");

            if (!popUp.popUp)                                       Debug.Log(gameObject.name + ": The 'popUp' has not been assigned.");
            if (!popUp.popUp.transform.root.GetComponent<Canvas>()) Debug.Log(gameObject.name + ": The root parent of the 'popUpText' needs to be a UI Canvas. One possible cause could be that you have your 'popUp' parented to something odd, or that you have parented the canvas to some other object. The Canvas should not have a parent.");
            if (!popUp.popUpText)                                   Debug.Log (gameObject.name + ": The 'popUpText' has not been assigned.");

            if (!pauseMenu.pauseMenu)                     Debug.Log(gameObject.name + ": No 'pauseMenu' has been assigned.");
            if (!pauseMenu.pauseMenuButtons.resumeButton) Debug.Log(gameObject.name + ": No 'resumeButton' has been assigned.");
            if (!pauseMenu.pauseMenuButtons.quitButton)   Debug.Log(gameObject.name + ": No 'quitButton' has been assigned.");

            if (!(falling.fallDeathSounds.Length > 0)) Debug.Log(gameObject.name + ": No `fallDeathSounds` have been assigned");
            if (falling.fallDeathSounds.Length > 0) {
                for (int i = 0; i < falling.fallDeathSounds.Length; i++) {
                    if (!falling.fallDeathSounds[i]) Debug.Log(gameObject.name + ": 'fallingDeathSounds' (" + i + ") needs an audio clip applied to it.");
                }
            }
            if (!(falling.fallDeathMessages.Length > 0)) Debug.Log(gameObject.name + ": No `fallDeathMessages` have been assigned");
            if (falling.fallDeathMessages.Length > 0) {
                for (int i = 0; i < falling.fallDeathMessages.Length; i++) {
                    if (!(falling.fallDeathMessages[i].popUpText.Length > 0)) Debug.Log(gameObject.name + ": 'fallingDeathSounds' (" + i + ") needs some text in it's 'popUpText' property.");
                }
            }

            if (!misc.initialSpawnPoint) Debug.Log(gameObject.name + ": 'initialSpawnPoint' has not been set. World Position (x0, y0, z0) will be used as the initial respawn point if the character dies.");
        }
    }
}