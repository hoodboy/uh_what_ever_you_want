﻿using UnityEngine;

[AddComponentMenu("AnimCol/Misc/KillCollider")]
public class KillCollider : MonoBehaviour {
    [Tooltip("The amount of damage applied when this KillCollider collides with the player. Leave this at zero, and it will cause instant death.")]
    public int damage;
    [Tooltip("An array of clips from which one is played when collecting this pickup.")]
    public AudioClip[] killSound;
    [Tooltip("The potential messages that can be played when you die from this KillCollider.")]
    public PopUpTrigger.PopUpEvent[] deathMessages;
    [Tooltip("Enable this to manually toggle the killbox on and off based on animation events.")]
    public bool waitForAnimation = false;
    [Tooltip("If this KillCollider has a spriteRenderer applied to it purely as a design guide, toggling this feature on will hide that sprite during your game.")]
    public bool hideSpriteRenderer = false;

    private Collider2D col;

    void Start() {
        gameObject.tag = "KillBox";
        col = GetComponent<Collider2D>();

        if (col) {
            col.isTrigger = true;
        } else {
            Debug.LogError (gameObject.name +  " is missing a Collider2D.");
        }

        if (waitForAnimation) {
            col.enabled = false;
        }

        SpriteRenderer spriteRenderer = GetComponent<SpriteRenderer>();
        if (spriteRenderer && hideSpriteRenderer) {
            spriteRenderer.enabled = false;
        }
    }

    public void EnableKillBox() {
        if (waitForAnimation) {
            col.enabled = true;
        }
    }

    public void DisableKillBox() {
        if (waitForAnimation) {
            col.enabled = false;
        }
    }
}
