﻿using UnityEngine;
using GameManager;

/// <summary>
/// Provides a way to trigger the GameManager's coroutines from within the GameManager or any other static class.
/// </summary>
[AddComponentMenu("")]
public class CoroutineCompanion : MonoBehaviour {
    public void DisplayDialogue(Events.Dialogue dialogue) {
        StartCoroutine(Generic.DisplayDialogue(dialogue));
    }

    public void SpawnObjects(Events.ObjectsToSpawn[] objectsToSpawn) {
        StartCoroutine(Generic.SpawnObjects(objectsToSpawn));
    }

    public void DestroyObjects(Events.ObjectsToDestroy[] objectsToDestroy) {
        StartCoroutine(Generic.DestroyObjects(objectsToDestroy));
    }

    public void TriggerAnimations(Events.AdvancedAnimator[] advancedAnimators) {
        StartCoroutine(Generic.TriggerAnimators(advancedAnimators));
    }

    public void LoadScene(string sceneName) {
        StartCoroutine(Generic.LoadScene(sceneName));
    }

    public void InterruptAllPopUps () {
        if (Generic.player.displayPopUp != null) {
            StopCoroutine(Generic.player.displayPopUp);
        }

        foreach (PopUpTrigger popUpTrigger in FindObjectsOfType<PopUpTrigger>()) {
            popUpTrigger.InterruptCoroutine();
        }

        if (Generic.popUp) {
            Generic.popUp.gameObject.SetActive(false);
        }
    }
}