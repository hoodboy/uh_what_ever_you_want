﻿using UnityEngine;
using UnityEngine.UI;
using UnityStandardAssets.ImageEffects;
using System.Collections;
using System.Collections.Generic;

namespace GameManager {
    /// <summary>
    /// Generic fields and methods designed to be accessed by any class.
    /// </summary>
    /// 
    /// <methods>
    /// ButtonSetUp
    /// CameraCutSceneSwitch
    /// CheckForCollider
    /// DestroyObjects
    /// DisplayDialogue
    /// DisplayPopUp
    /// InterruptAllPopUps
    /// LoadScene
    /// SceneFadeIn
    /// PlaySFX
    /// SpawnObjects
    /// TallyTotalScore
    /// ToggleParticleSystems
    /// ToggleAnimators
    /// TriggerAnimators
    /// UpdateLives
    /// UpdateScore
    /// </methods>
    [AddComponentMenu("")]
    public static class Generic {
        // Features.
        public static bool scoreSystem { get; set; }
        public static bool lifeSystem { get; set; }

        // Player.
        public static Player player { get; set; }
        public static SpriteRenderer playerspriteRenderer { get; set; }

        // Lives.
        public static int lives { get; set; }
        public static int maxLives { get; set; }
        public static List<Image> lifeIcons { get; set; }

        // Health.
        public static int health { get; set; }
        public static int maxHealth { get; set; }
        public static RectTransform healthGauge { get; set; }
        public static float healthGaugeWidth { get; set; }

        // Score.
        public static int score { get; set; }
        public static int targetScore { get; set; }
        public static RectTransform scoreGauge { get; set; }
        public static Animator scoreGaugeAnimator { get; set; }
        public static float scoreGaugeWidth { get; set; }

        // Dialogue.
        public static Animator dialogueAnimator { get; set; }
        public static AudioSource dialogueAudioSource { get; set; }
        public static Text dialogueText { get; set; }
        public static Image dialogueAvatarImage { get; set; }
        public static Text dialogueAvatarName { get; set; }
        public static RectTransform choiceBox { get; set; }
        public static Button choiceButtonA { get; set; }
        public static Button choiceButtonB { get; set; }

        // Pop-Up.
        public static RectTransform popUpCanvas { get; set; }
        public static RectTransform popUp { get; set; }
        public static Text popUpText { get; set; }
        public static Text popUpTextDummy { get; set; }
        public static Transform popUpTarget { get; set; }
        public static float heightOfPopUpTarget { get; set; }
        public static Vector2 popUpOffset { get; set; }

        // Pause menu.
        public static RectTransform pauseMenu { get; set; }
        public static Button defaultPauseButton { get; set; }

        // Falling parameters.
        public static AudioClip[] fallDeathSounds { get; set; }
        public static PopUpTrigger.PopUpEvent[] fallDeathMessages { get; set; }
        public static bool deathByFallTime { get; set; }
        public static float fallVelocityThreshold { get; set; }
        public static float maxFallTime { get; set; }

        // Misc.
        public static float rewardBonusCompletionTime { get; set; }
        public static float timeElapsed { get; set; }
        public static int totalScore { get; set; }
        public static Transform spawnPoint { get; set; }
        public static AudioSource sfx { get; set; }
        public static AudioSource bgMusic { get; set; }
        public static Init.EventManager eventManager { get; set; }

        private static bool IsControllable = true;
        public static bool isControllable {
            get { return IsControllable; }
            set { IsControllable = value; }
        }

        public static List<SpawnPoint.ObjectToReset> objectsToReset { get; set; }
        public static float cutSceneTransitionDuration { get; set; }
        public static CoroutineCompanion coroutineCompanion { get; set; }

        private static bool isTyping;

        /// <summary>
        /// Iterate through each cutScene and render through it's camera while running all events for that cut-scene.
        /// </summary>
        /// <param name="cutScenes"></param>
        /// <param name="delayInSeconds"></param>
        /// <param name="shouldEndTheGame"></param>
        /// <returns></returns>
        public static IEnumerator CameraCutSceneSwitch (Events.CutScenes[] cutScenes, float delayInSeconds, bool shouldEndTheGame) {
            if (cutScenes.Length > 0) {
                bool isLast = false;

                Camera camPlayer = Camera.main;

                yield return new WaitForSeconds(delayInSeconds);

                for (int i = 0; i < cutScenes.Length; i++) {
                    // Prepare the current camera.
                    Camera camCurrent = Camera.main;
                    AudioListener camAudioCurrent = camCurrent.GetComponent<AudioListener>();
                    ScreenOverlay scOverlayCurrent = camCurrent.gameObject.AddComponent<ScreenOverlay>();
                    scOverlayCurrent.overlayShader = Shader.Find("Hidden/BlendModesOverlay");
                    scOverlayCurrent.blendMode = ScreenOverlay.OverlayBlendMode.Multiply;
                    scOverlayCurrent.intensity = 2.0f;

                    // Prepare the camera we're about to transition to.
                    // If we're truly on our last iteration, the next camera we transition to is the player camera
                    // Default to the main camera.
                    Camera camNext = isLast ? camPlayer : cutScenes[i].camera;
                    camNext = camNext ? camNext : Camera.main;

                    if (camNext) {
                        AudioListener camAudioNext = camNext.GetComponent<AudioListener>();
                        ScreenOverlay scOverlayNext = camNext.gameObject.AddComponent<ScreenOverlay>();
                        scOverlayNext.overlayShader = Shader.Find("Hidden/BlendModesOverlay");
                        scOverlayNext.blendMode = ScreenOverlay.OverlayBlendMode.Multiply;
                        scOverlayNext.intensity = 0.0f;

                        float t = 0.0f;

                        // Fade out
                        if (camCurrent != camNext && cutScenes[i].fadeToBlack) {
                            while (t < cutSceneTransitionDuration) {
                                if (scOverlayCurrent) {
                                    scOverlayCurrent.intensity = Mathf.Lerp(
                                        2.0f,
                                        0.0f,
                                        (t / cutSceneTransitionDuration));
                                }

                                t += Time.deltaTime;
                                yield return null;
                            }
                            if (scOverlayCurrent) {
                                scOverlayCurrent.intensity = 0.0f;
                            }
                        }

                        // Switch cameras
                        camCurrent.enabled = false;
                        camAudioCurrent.enabled = false;
                        camCurrent.tag = "Untagged";

                        camNext.enabled = true;
                        if (camAudioNext) {
                            camAudioNext.enabled = true;
                        }
                        camNext.tag = "MainCamera";

                        // Fade in
                        if (camCurrent != camNext && cutScenes[i].fadeToBlack) {
                            t = 0.0f;

                            while (t < cutSceneTransitionDuration) {
                                if (scOverlayNext) {
                                    scOverlayNext.intensity = Mathf.Lerp(
                                        0.0f,
                                        2.0f,
                                        (t / cutSceneTransitionDuration)
                                    );
                                }

                                t += Time.deltaTime;
                                yield return null;
                            }

                            if (scOverlayNext) {
                                scOverlayNext.intensity = 2.0f;
                            }
                        }

                        // Remove the image effects that we no longer need.
                        if (scOverlayCurrent) {
                            Object.Destroy(scOverlayCurrent);
                        }

                        if (scOverlayNext) {
                            Object.Destroy(scOverlayNext);
                        }

                        // If this is our first time approaching the last cutscene iteration.
                        if (!isLast) {
                            // Execute advanced events
                            ToggleLights(cutScenes[i].advancedEvents.lights);
                            ToggleParticleSystems(cutScenes[i].advancedEvents.particleSystems);
                            ToggleAnimators(cutScenes[i].advancedEvents.animators);
                            coroutineCompanion.TriggerAnimations(cutScenes[i].advancedEvents.advancedAnimators);
                            coroutineCompanion.SpawnObjects(cutScenes[i].advancedEvents.objectsToSpawn);
                            coroutineCompanion.DestroyObjects(cutScenes[i].advancedEvents.objectsToDestroy);

                            yield return new WaitForSeconds(cutScenes[i].padding.x);

                            if (cutScenes[i].dialogue.text.Length > 0) {
                                coroutineCompanion.DisplayDialogue(cutScenes[i].dialogue);
                            }

                            // Wait for dialogue to finish typing.
                            while (isTyping) {
                                yield return null;
                            }

                            yield return new WaitForSeconds(cutScenes[i].padding.y);
        
                            // Ensure that while we may be on our last iteration, that we get one extra iteration to allow us to cycle back to the main camera.
                            if (i == cutScenes.Length - 1) {
                                // if returnToPlayerCam is false, then we assume this is to endTheLevel, in which case we want to break from this coroutine instead of cycling to the playerCam
                                if (shouldEndTheGame) {
                                    yield break;
                                }

                                isLast = true;
                                i--;
                            }
                        }
                    } else {
                        Debug.Log("Some cut-scenes have not been set-up correctly.");
                        yield break;
                    }

                }
            }
        }

        /// <summary>
        /// Report whether or not a collider is attached to any game object.
        /// </summary>
        /// <param name="go"></param>
        public static void CheckForCollider(GameObject go) {
            Collider2D col2D = go.GetComponent<Collider2D>();

            if (col2D) {
                col2D.isTrigger = true;
            } else {
                Debug.LogWarning(go.name + " is missing a Collider2D component.");
            }
        }

        /// <summary>
        /// Iterate through an array of objects, destroying each one, playing a sound and a particle effect for each.
        /// </summary>
        /// <param name="objectsToDestroy"></param>
        /// <returns></returns>
        public static IEnumerator DestroyObjects (Events.ObjectsToDestroy[] objectsToDestroy) {
            if (objectsToDestroy.Length > 0) {
                foreach (Events.ObjectsToDestroy otd in objectsToDestroy) {
                    if (otd.destroyDelayInSeconds > 0) {
                        yield return new WaitForSeconds (otd.destroyDelayInSeconds);
                    }

                    if (otd.objectToDestroy) {
                        if (otd.destroyEffect) {
                            Object.Instantiate(otd.destroyEffect, (Vector2)otd.objectToDestroy.transform.position + otd.destroyEffectOffset, Quaternion.identity);
                        }

                        Object.Destroy(otd.objectToDestroy);

                        PlaySFX(otd.audioClip);
                    }
                }
            }
        }

        /// <summary>
        /// Display a dialogue until it either times out, or is dismissed by the player.
        /// </summary>
        /// <param name="dialogue"></param>
        /// <returns></returns>
        public static IEnumerator DisplayDialogue (Events.Dialogue dialogue) {
            isTyping = true;
            if (dialogueAnimator
            &&  dialogueText
            &&  dialogue.text.Length > 0) {
                // Audio.
                if (!dialogueAudioSource.isPlaying && dialogue.audio) {
                    dialogueAudioSource.clip = dialogue.audio;
                    dialogueAudioSource.Play();
                }

                // Avatar.
                if (dialogue.avatar) {
                    dialogueText.alignment = TextAnchor.UpperLeft;
                    dialogueAvatarName.gameObject.SetActive(true);

                    if (dialogue.avatar.avatarSprite) {
                        dialogueAvatarImage.gameObject.SetActive(true);
                        dialogueAvatarImage.sprite = dialogue.avatar.avatarSprite;
                    } else {
                        dialogueAvatarImage.sprite = null;
                        dialogueAvatarImage.gameObject.SetActive(false);
                    }

                    dialogueAvatarName.text = dialogue.avatar.displayName ? dialogue.avatar.avatarName : "";
                }
                else {
                    dialogueAvatarImage.sprite = null;
                    dialogueAvatarImage.gameObject.SetActive(false);
                    dialogueAvatarName.text = "";
                    dialogueAvatarName.gameObject.SetActive(false);
                    dialogueText.alignment = TextAnchor.MiddleCenter;
                }

                // Choice dialogue.
                choiceBox.gameObject.SetActive(false);
                if (dialogue.isAwaitingChoice) {
                    choiceBox.gameObject.SetActive(true);
                    choiceButtonA.Select();
                }

                // Begin to display dialogue.
                dialogueText.text = "";
                dialogueAnimator.SetBool("active", true);
                // Begin hack to maintain whether or not this dialogue is for "choices". It would be great to know why it isAwaitingChoice forgets it's value in the first place.
                bool temp = dialogue.isAwaitingChoice;
                yield return new WaitForEndOfFrame();
                dialogue.isAwaitingChoice = temp;
                // End hack
                while (dialogueAnimator.IsInTransition(0)) {
                    yield return null;
                }

                // Print each letter, one-frame-at-a-time.
                for (int a = 0; a < dialogue.text.Length + 1; a++) {
                    dialogueText.text = dialogue.text.Remove(a, dialogue.text.Length - a);
                    yield return null;
                }

                if (dialogue.isAwaitingChoice) {
                    // If this dialogue is a "choice" called from ChoiceTrigger, keep the dialogue up until the user has made a choice.
                    while (dialogue.isAwaitingChoice) {
                        yield return null;
                    }
                } else {
                    // Otherwise, delay until end of dialogue.
                    while (true) {
                        if (Input.GetButtonDown("Submit")) {
                            break;
                        }

                        yield return null;
                    }
                }

                // Finish displaying dialogue.
                dialogueAnimator.SetBool("active", false);
                yield return new WaitForEndOfFrame();
                while (dialogueAnimator.IsInTransition(0)) {
                    yield return null;
                }

                choiceBox.gameObject.SetActive(false);
                dialogueText.text = "";

                //audio
                if (dialogueAudioSource.isPlaying && dialogue.audio){
                    dialogueAudioSource.Stop();
                    dialogueAudioSource.clip = null;
                }
            }
            isTyping = false;
        }

        /// <summary>
        /// Display the popup dialogue for a set period of time per string in array.
        /// </summary>
        public static IEnumerator DisplayPopUp(PopUpTrigger.PopUpEvent[] popUpEvents) {
            if (popUp) {
                foreach (PopUpTrigger.PopUpEvent popUpEvent in popUpEvents) {
                    popUpTarget = popUpEvent.target ? popUpEvent.target : player.transform;
                    heightOfPopUpTarget = popUpTarget.GetComponent<Collider2D>().bounds.size.y;

                    yield return new WaitForSeconds(0.5f);

                    popUp.gameObject.SetActive(true);

                    popUpTextDummy.text = popUpEvent.popUpText;
                    popUpText.text = "";

                    // Print each letter, one-frame-at-a-time.
                    for (int a = 0; a < popUpEvent.popUpText.Length + 1; a++) {
                        popUpText.text = popUpEvent.popUpText.Remove(a, popUpEvent.popUpText.Length - a);
                        yield return null;
                    }

                    yield return new WaitForSeconds(popUpEvent.duration + 0.5f);
                    popUp.gameObject.SetActive(false);
                }
            }
        }

        /// <summary>
        /// Stop all pop-ups in the scene, allowing you to safely trigger the next one via the 'DisplayPopUp' method.
        /// </summary>
        public static void InterruptAllPopUps() {
            coroutineCompanion.InterruptAllPopUps();
        }

        /// <summary>
        /// Load any scene after fading-to-black.
        /// </summary>
        /// <param name="sceneName"></param>
        /// <returns></returns>
        public static IEnumerator LoadScene(string sceneName) {
            Camera cam = Camera.main;
            ScreenOverlay scOverlayCurrent = cam.gameObject.AddComponent<ScreenOverlay>();
            scOverlayCurrent.overlayShader = Shader.Find("Hidden/BlendModesOverlay");
            scOverlayCurrent.blendMode = ScreenOverlay.OverlayBlendMode.Multiply;
            scOverlayCurrent.intensity = 2.0f;

            float t = 0.0f;
            while (t < 0.5f) {
                scOverlayCurrent.intensity = Mathf.Lerp(
                    2.0f,
                    0.0f,
                    (t / 0.5f));

                t += Time.deltaTime;
                yield return null;
            }
            scOverlayCurrent.intensity = 0.0f;

            UnityEngine.SceneManagement.SceneManager.LoadScene(sceneName);
        }

        /// <summary>
        /// Used at the start of scenes to fade-from-black.
        /// </summary>
        /// <returns></returns>
        public static IEnumerator SceneFadeIn () {
            Camera cam = Camera.main;
            ScreenOverlay scOverlayCurrent = cam.gameObject.AddComponent<ScreenOverlay>();
            scOverlayCurrent.overlayShader = Shader.Find("Hidden/BlendModesOverlay");
            scOverlayCurrent.blendMode = ScreenOverlay.OverlayBlendMode.Multiply;
            scOverlayCurrent.intensity = 2.0f;

            float t = 0.0f;
            while (t < 0.5f) {
                scOverlayCurrent.intensity = Mathf.Lerp(
                    0.0f,
                    2.0f,
                    (t / 0.5f));

                t += Time.deltaTime;
                yield return null;
            }
            scOverlayCurrent.intensity = 2.0f;
            Object.Destroy(scOverlayCurrent);
        }

        /// <summary>
        /// Play an array of audioClips, choosing one at random. Useful for playing a sound without having to have a reference to any audioSource.
        /// </summary>
        /// <param name="audioClips"></param>
        public static void PlaySFX (AudioClip[] audioClips) {
            if (audioClips != null && audioClips.Length > 0) {
                int random = Random.Range(0, audioClips.Length);
                sfx.PlayOneShot(audioClips[random]);
            }
        }

        /// <summary>
        /// Spawn an array of objects with accompanying sounds and particle effects.
        /// </summary>
        /// <param name="objectsToSpawn"></param>
        /// <returns></returns>
        public static IEnumerator SpawnObjects (Events.ObjectsToSpawn[] objectsToSpawn) {
            if (objectsToSpawn != null && objectsToSpawn.Length > 0) {
                foreach (Events.ObjectsToSpawn ots in objectsToSpawn) {
                        if (ots.spawnDelayInSeconds > 0) {
                            yield return new WaitForSeconds(ots.spawnDelayInSeconds);
                        }

                        if (ots.objectToSpawn) {
                        Object.Instantiate(ots.objectToSpawn, ots.spawnLocationObject.transform.position, Quaternion.identity);

                        if (ots.spawnEffect) {
                            Object.Instantiate(ots.spawnEffect, (Vector2)ots.spawnLocationObject.transform.position + ots.spawnEffectOffset, Quaternion.identity);
                        }

                        PlaySFX(ots.audioClip);
                    }
                }
            }
        }

        /// <summary>
        /// Get the total score based on an algorithm evaluating current score, lives, and the difference between time-taken and the target time.
        /// </summary>
        public static void TallyTotalScore() {
            if (scoreSystem && (timeElapsed < rewardBonusCompletionTime)) {
                float timeBonus = (rewardBonusCompletionTime - timeElapsed) / 60;
                totalScore = score + (int)((score * timeBonus) + ((lifeSystem ? lives : 1) * timeBonus));

                //Debug.Log("Score from gems: " + score);
                //Debug.Log("Reward Bonus Completion Time: " + rewardBonusCompletionTime);
                //Debug.Log("Seconds elapsed: " + timeElapsed);
                //Debug.Log("Time bonus: " + timeBonus);
                //Debug.Log("Score should be: " + score + (int)((score * timeBonus) + ((lifeSystem ? lives : 1) * timeBonus)));
            }

        }

        /// <summary>
        /// Toggle an array of lights on or off, depending on their current state.
        /// </summary>
        /// <param name="lights"></param>
        public static void ToggleLights (Light[] lights) {
            if (lights != null && lights.Length > 0) {
                foreach (Light light in lights) {
                    light.enabled = !light.enabled;
                }
            }
        }

        /// <summary>
        /// Toggle an array of particleSystems on or off, depending on their current state.
        /// </summary>
        /// <param name="particleSystems"></param>
        public static void ToggleParticleSystems (ParticleSystem[] particleSystems) {
            if (particleSystems != null && particleSystems.Length > 0) {
                foreach (ParticleSystem ps in particleSystems) {
                    if (ps) {
                        //ps.Clear();
                        ps.Play();

                        ParticleSystem.EmissionModule em = ps.emission;
                        em.enabled = !em.enabled;
                    }
                }
            }
        }

        /// <summary>
        /// Step through each animator in animators, attempt to toggle the active bool parameter in the triggeredAnimator.
        /// </summary>
        /// <param name="animators"></param>
        public static void ToggleAnimators (Animator[] animators) {
            if (animators != null && animators.Length > 0) {
                foreach (Animator animator in animators) {
                    if (animator) {
                        // Check if our 'active' parameter exists.
                        foreach (AnimatorControllerParameter param in animator.parameters) {
                            if (param != null) {
                                if (param.name == "active") {
                                    animator.SetBool("active", !animator.GetBool("active"));
                                }
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// An advanced version of ToggleAnimators. Trigger an array of animators with advanced options set in the inspector.
        /// </summary>
        /// <param name="advancedAnimators"></param>
        /// <returns></returns>
        public static IEnumerator TriggerAnimators (Events.AdvancedAnimator[] advancedAnimators) {
            if (advancedAnimators != null && advancedAnimators.Length > 0) {
                foreach (Events.AdvancedAnimator advancedAnimator in advancedAnimators) {
                    if (advancedAnimator.delayAnimationInSeconds > 0) {
                        yield return new WaitForSeconds(advancedAnimator.delayAnimationInSeconds);
                    }

                    // Either trigger a specified state, or attempt to switch the 'active' bool to 'true'.
                    if (advancedAnimator.triggeredAnimator) {
                        // Check if our 'active' parameter exists.
                        bool hasActiveBoolParam = false;
                        bool hasCycleOffsetFloatParam = false;
                        foreach (AnimatorControllerParameter param in advancedAnimator.triggeredAnimator.parameters) {
                            if (param.name == "active") {
                                hasActiveBoolParam = true;
                            }

                            if (param.name == "cycleOffset") {
                                hasCycleOffsetFloatParam = true;
                            }
                        }

                        if (advancedAnimator.triggeredState != null 
                        && advancedAnimator.triggeredState.Length > 0
                        && advancedAnimator.triggeredAnimator.HasState(0, Animator.StringToHash(advancedAnimator.triggeredState))) {
                            advancedAnimator.triggeredAnimator.Play(advancedAnimator.triggeredState);

                            if (hasCycleOffsetFloatParam) {
                                advancedAnimator.triggeredAnimator.SetFloat("cycleOffset", advancedAnimator.cycleOffset);
                            }
                        } else if (hasActiveBoolParam) {
                            advancedAnimator.triggeredAnimator.SetBool("active", !advancedAnimator.triggeredAnimator.GetBool("active"));

                            if (hasCycleOffsetFloatParam) {
                                advancedAnimator.triggeredAnimator.SetFloat("cycleOffset", advancedAnimator.cycleOffset);
                            }
                        } else {
                            Debug.Log("An 'objectToAnimate' was found with neither a desired 'triggeredState' or an boolean parameter named 'active'.");
                        }
                    } else {
                        Debug.Log("Please check that all your objectsToAnimate have an 'Animator' component");
                    }
                }
            }
        }

        /// <summary>
        /// Adjust the player's lives by -/+ any int value.
        /// </summary>
        /// <param name="lifeAddition"></param>
        public static void UpdateLives (int lifeAddition) {
            if (lifeSystem) {
                lives += lifeAddition;

                // Cap them in case the player picks up too many lives.
                lives = Mathf.Clamp(lives, 0, maxLives);

                if (lifeIcons != null) {
                    foreach (Image img in lifeIcons) {
                        img.enabled = false;
                        img.transform.parent.GetComponent<Animator>().SetBool("active", false);
                    }
                }

                if (lives > 0) {
                    Animator[] animators = new Animator[lives];

                    if (lifeIcons != null && lifeIcons.Count > 0) {
                        for (int i = 0; i < lives; i++) {
                            lifeIcons[i].enabled = true;
                            animators[i] = lifeIcons[i].transform.parent.GetComponent<Animator>();
                        }

                        foreach (Animator animator in animators) {
                            foreach (AnimatorControllerParameter param in animator.parameters) {
                                if (param.name == "active") {
                                    animator.SetBool("active", !animator.GetBool("active"));
                                }
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Adjust the player's score by -/+ any int value.
        /// </summary>
        /// <param name="scoreAddition"></param>
        public static void UpdateScore (int scoreAddition) {
            if (scoreSystem) {
                score += scoreAddition;

                float percent = score > 0 ? (float)score / (float)targetScore : 0.0f;

                // Resize the gauge, capping it's width if the target score has been reached.
                if (scoreGauge) {
                    scoreGauge.sizeDelta = 
                        score >= targetScore ? 
                        Vector2.zero 
                    :   new Vector2(-scoreGaugeWidth + (percent * scoreGaugeWidth), scoreGauge.sizeDelta.y);
                }

                // If the target score has been reached, set the active bool on the scoreGaugeAniator.
                if (scoreGaugeAnimator) {
                    scoreGaugeAnimator.SetBool("active", score >= targetScore ? true : false);
                }
            }
        }

        /// <summary>
        /// Adjust the player's health by -/+ any int value.
        /// </summary>
        /// <param name="healthAddition"></param>
        public static void UpdateHealth (int healthAddition) {
            if (maxHealth > 0 && health <= maxHealth) {
                health += healthAddition;
                health = Mathf.Clamp(health, 0, maxHealth);

                float percent = health > 0 ? (float)health / (float)maxHealth : 0.0f;

                if (healthGauge) {
                    healthGauge.sizeDelta = 
                        health >= maxHealth ? 
                        Vector2.zero 
                    :   new Vector2(-healthGaugeWidth + (percent * healthGaugeWidth), healthGauge.sizeDelta.y);
                }
            }
        }

        /// <summary>
        /// Store objects from the initializer or spawnPoint's objectsToReset property. These objects have their transforms remembered so that they can be restored upon death.
        /// </summary>
        /// <param name="transforms"></param>
        public static void SetObjectsToReset(Transform[] transforms) {
            objectsToReset = new List<SpawnPoint.ObjectToReset>();

            if (transforms != null && transforms.Length > 0) {
                foreach (Transform t in transforms) {
                    SpawnPoint.ObjectToReset otr = new SpawnPoint.ObjectToReset();
                    otr.obj = t;
                    otr.pos = t.position;
                    otr.scale = t.localScale;
                    otr.rot = t.localEulerAngles;

                    objectsToReset.Add(otr);
                }
            }
        }

        /// <summary>
        /// Upon death, objects found in the objectsToReset array will be set back to whatever position, scale and rotation they were found with when the player last entered a spawn point.
        /// </summary>
        public static void ResetObjects() {
            if (objectsToReset != null && objectsToReset.Count > 0) {
                foreach (SpawnPoint.ObjectToReset otr in objectsToReset) {
                    otr.obj.position = otr.pos;
                    otr.obj.localScale = otr.scale;
                    otr.obj.localEulerAngles = otr.rot;
                }
            }
        }
    }

    /// <summary>
    /// Assign the following methods to buttons in the UI via script.
    /// </summary>
    /// 
    /// <methods>
    /// TogglePause
    /// EnterMainScene
    /// ReturnToStartScreen
    /// Quit
    /// </methods>
    [AddComponentMenu("")]
    public static class UIEvents {
        /// <summary>
        /// Toggle the pause menu on or off, depending on the current state.
        /// </summary>
        public static void TogglePause() {
            if (Time.timeScale == 1.0f) {
                Time.timeScale = 0.0f;
                Generic.pauseMenu.gameObject.SetActive(true);
                Generic.defaultPauseButton.Select();

                if (Generic.bgMusic) {
                    Generic.bgMusic.Pause();
                }
            }
            else {
                Time.timeScale = 1.0f;
                Generic.pauseMenu.gameObject.SetActive(false);

                if (Generic.bgMusic) {
                    Generic.bgMusic.Play();
                }
            }
        }

        /// <summary>
        /// Assign a method to any button to enable them to transition to the startScreen scene.
        /// </summary>
        public static void ReturnToStartScreen() {
            Time.timeScale = 1.0f;
            Generic.coroutineCompanion.LoadScene("SplashScreen");
        }

        /// <summary>
        /// Assign a method to any button to enable them to exit the application.
        /// </summary>
        public static void Quit() {
            Application.Quit();
        }
    }

}