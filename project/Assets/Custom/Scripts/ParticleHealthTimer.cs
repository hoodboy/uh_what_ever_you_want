﻿using UnityEngine;
using GameManager;

[AddComponentMenu("AnimCol/Misc/ParticleHealthTimer")]
[RequireComponent(typeof(ParticleSystem))]
public class ParticleHealthTimer : MonoBehaviour {
    [Header("Particles")]
    [Range(0, 255)]
    [Tooltip("The value of alpha that your ParticleSystem is set to when health is at 0%.")]
    public float minAlpha;
    [Range(0, 255)]
    [Tooltip("The value of alpha that your ParticleSystem is set to when health is at 100%.")]
    public float maxAlpha;
    [Header("Damage")]
    [Tooltip("The amount of damage to sustain at each interval.")]
    public int damagePerTick = 2;
    [Tooltip("The amount of time in seconds between each health deduction.")]
    public float damageFrequency = 1.0f;
    [Header("Death")]
    [Tooltip("You can have a small message appear upon death via time-out.")]
    public PopUpTrigger.PopUpEvent[] deathPopUps;
    [Tooltip("A list of audio clips, from which one will be chosen to play when the player dies via time-out.")]
    public AudioClip[] audioClips;

    private ParticleSystem parti;

    void Start () {
        parti = GetComponent<ParticleSystem>();
        InvokeRepeating("TakeDamage", damageFrequency, damageFrequency);
    }
    
    void Update () {
        parti.startColor = new Color (parti.startColor.r, parti.startColor.g, parti.startColor.b, GetAlphaAsPercentage());
    }

    void TakeDamage() {
        if (Generic.isControllable && Generic.health > 0) {
            Generic.UpdateHealth(-damagePerTick);

            if (Generic.health <= 0) {
                Generic.player.Die(audioClips, deathPopUps);
            }
        }
    }

    private float GetAlphaAsPercentage () {
        return GetAlphaFromHealth() / 255.0f;
    }

    private float GetAlphaFromHealth() {
        return Mathf.Lerp(minAlpha, maxAlpha, GetHealthPercentage());
    }

    private float GetHealthPercentage() {
        return Generic.health > 0 ? (float)Generic.health / (float)Generic.maxHealth : 0.0f;
    }
}
