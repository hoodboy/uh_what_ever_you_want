﻿using UnityEngine;

[AddComponentMenu("AnimCol/Misc/ScreenShot")]
public class ScreenShot : MonoBehaviour {
    [Tooltip("Which button do you want to assign for taking screenshots?")]
    public KeyCode ScreenShotButton;

    void Update() {
        if (Input.GetKeyDown (ScreenShotButton)){
            print ("Say cheese!!!! :D");
            Application.CaptureScreenshot("ScreenShot.png",4);
        }
    }
}