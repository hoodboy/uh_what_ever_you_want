﻿using UnityEngine;
using UnityEngine.UI;

[AddComponentMenu("AnimCol/Misc/Movie")]
[RequireComponent(typeof(AudioSource))]
[RequireComponent(typeof(RawImage))]
public class Movie : MonoBehaviour {
    [Tooltip("Add time between when the scene begins and when the movie begins. This can solve the issue of lag or frame skipping at the start of the movie, if that's a problem.")]
    public float delay;
    [Tooltip("The name of the scene you intend to switch to once the movie has either finished playing or has been interrupted.")]
    public string nextScene = "Main";

    private bool hasStarted;
    private bool hasEnded;
    private RawImage rawImage;
    private Texture movTexture;
    private MovieTexture movie;
    private AudioSource audioSource;

    void Start() {
        rawImage = GetComponent<RawImage>();
        movTexture = rawImage.texture;
        audioSource = GetComponent<AudioSource>();

        if (movTexture) {
            movie = (MovieTexture)movTexture;
        }

        if (movie) {
            rawImage.color = Color.black;
            Invoke("Play", delay);
        } else {
            Debug.Log("No movie found.");
            GoToNextScene();
        }
    }

    void Update() {
        if (movie) {
            if (movie.isPlaying && !hasStarted) {
                hasStarted = true;
            }

            if (hasStarted && !movie.isPlaying && !hasEnded) {
                hasEnded = true;
                GoToNextScene();
            }

            if (Input.GetButtonDown("Submit") && movie.isPlaying && !hasEnded) {
                GoToNextScene();
            }
        }
    }

    private void Play () {
        rawImage.color = Color.white;

        movie.Play();
        movie.loop = false;

        audioSource.clip = movie.audioClip;
        audioSource.Play();
    }

    private void GoToNextScene() {
        hasEnded = true;

        if (movie && audioSource) {
            movie.Stop();
            audioSource.Stop();
        }

        rawImage.color = Color.black;
        StartCoroutine(GameManager.Generic.LoadScene(nextScene));
    }
}