﻿using UnityEngine;

[AddComponentMenu("AnimCol/Misc/RandomMovement")]
public class RandomMovement : MonoBehaviour {
    [Header("Position")]
    [Tooltip("Allow position to be randomly animated.")]
    public bool randomizePosition;
    [Tooltip("The first position to potentially shift to.")]
    public Vector3 minPos;
    [Tooltip("The second position to potentially shift to.")]
    public Vector3 maxPos;
    [Tooltip("The speed of the randomized position.")]
    public float positionSpeed = 1.0f;

    [Header("Rotation")]
    [Tooltip("Allow rotation to be randomly animated.")]
    public bool randomizeRotation;
    [Tooltip("The first rotation to potentially shift to.")]
    public Vector3 minRot;
    [Tooltip("The second rotation to potentially shift to.")]
    public Vector3 maxRot;
    [Tooltip("The speed of the randomized rotation.")]
    public float rotationSpeed = 1.0f;

    [Header("Scale")]
    [Tooltip("Allow scale to be randomly animated.")]
    public bool randomizeScale;
    [Tooltip("The first scale to potentially shift to.")]
    public Vector3 minScale;
    [Tooltip("The second scale to potentially shift to.")]
    public Vector3 maxScale;
    [Tooltip("The speed of the randomized scale.")]
    public float scaleSpeed = 1.0f;

    private Vector3 random;
    private Vector3 initialPosition;
    private Vector3 initialRotation;
    private Vector3 initialScale;

    void Start() {
        random = new Vector3(RandomValue(), RandomValue(), RandomValue());
        initialPosition = transform.position;
        initialRotation = transform.localRotation.eulerAngles;
        initialScale = transform.localScale;
    }
    
    void Update() {
        if (randomizePosition) {
            transform.position = GetRandomizedV3(initialPosition, minPos, maxPos, positionSpeed);
        }

        if (randomizeRotation) {
            transform.rotation = Quaternion.Euler(GetRandomizedV3(initialRotation, minRot, maxRot, rotationSpeed));
        }

        if (randomizeScale) {
            transform.localScale = GetRandomizedV3(initialScale, minScale, maxScale, scaleSpeed);
        }

    }

    private Vector3 GetRandomizedV3(Vector3 initialVal, Vector3 val1, Vector3 val2, float speed) {
        Vector3 noise = new Vector3(
            GetPerlinNoise(random.x, speed),
            GetPerlinNoise(random.y, speed),
            GetPerlinNoise(random.z, speed)
        );

        return new Vector3(
            initialVal.x + Mathf.Lerp(val1.x, val2.x, noise.x),
            initialVal.y + Mathf.Lerp(val1.y, val2.y, noise.y),
            initialVal.z + Mathf.Lerp(val1.z, val2.z, noise.z)
        );
    }

    private float RandomValue() {
        return Random.Range(0.0f, 65535.0f);
    }

    private float GetPerlinNoise(float val, float speed) {
        return Mathf.PerlinNoise(val, Time.time * speed);
    }
}