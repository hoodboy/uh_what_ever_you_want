﻿using UnityEngine;
using System;
using GameManager;

[AddComponentMenu("AnimCol/Gizmos/SpawnPoint")]
[RequireComponent(typeof(BoxCollider2D))]
public class SpawnPoint : GizmoAndCollider {
    [Serializable]
    public class ObjectToReset {
        public Transform obj;
        public Vector3 pos;
        public Vector3 scale;
        public Vector3 rot;
    }

    [Tooltip("The animator that sets it's 'active' bool parameter to true upon entry.")]
    public Animator animator;
    [Tooltip("A particle system prefab that spawns when the checkpoint is activated.")]
    public ParticleSystem spawnParticles;
    [Tooltip("The position by which the spawned effect is offset.")]
    public Vector3 spawnOffset;
    [Tooltip("A list of transform objects which have their transform data stored upon entering this spawn point. Upon death, these objects have their transform data restored.")]
    public Transform[] objectsToReset;

    [NonSerialized]
    public bool active;

    void OnTriggerEnter2D(Collider2D other) {
        if (other.tag == "Player" && !active) {
            foreach (SpawnPoint sp in FindObjectsOfType<SpawnPoint>()) {
                sp.active = false;

                if (sp.animator) {
                    sp.animator.SetBool("active", false);
                }
            }

            active = true;

            Generic.spawnPoint = transform.GetChild(0);

            if (animator) {
                animator.SetBool("active", true);
            }

            if (spawnParticles) {
                Instantiate(spawnParticles, transform.position + spawnOffset, Quaternion.identity);
            }

            Generic.SetObjectsToReset(objectsToReset);
        }
    }
}
