﻿using UnityEngine;

[AddComponentMenu("AnimCol/Gizmos/Gizmo")]
public class Gizmo : MonoBehaviour {
    [Tooltip("Set the size for a cubic gizmo.")]
    public Vector3 size = new Vector3 (1.0f, 1.0f, 1.0f);
    [Tooltip("Set the gizmo colour.")]
    public Color color = new Color(0,0,0, 1.0f);
    [Tooltip("By default, the gizmo is hidden when the game begins, you can disable this if you need to see it for design purposes during play.")]
    public bool showOnStart;

    private bool on = true;
    
    void Start () {
        if(!showOnStart)
            on = false;
    }
    
    void OnDrawGizmos() {
        Gizmos.color = color;
        
        if (on)	{
            Gizmos.DrawCube (transform.position, size);
        }
    }
}
