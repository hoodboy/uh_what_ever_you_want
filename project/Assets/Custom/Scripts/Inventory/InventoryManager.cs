﻿public static class InventoryManager {
    public static Inventory inventory { get; set; }

    public static bool CheckInventory(Inventory.CheckList requirements) {
        return (
            (requirements.collectible      <= inventory.checkList.collectible)
        ||  (requirements.fakeTreasure     && !inventory.checkList.fakeTreasure)
        ||  (requirements.journalPage      && !inventory.checkList.journalPage)
        ||  (requirements.orb              && !inventory.checkList.orb)
        ||  (requirements.superCollectible && !inventory.checkList.superCollectible)
        ||  (requirements.superOrb         && !inventory.checkList.superOrb)
        );
    }
}