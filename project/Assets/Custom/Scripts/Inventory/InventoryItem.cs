﻿using UnityEngine;

[AddComponentMenu("AnimCol/Inventory/InventoryItem")]
public class InventoryItem : MonoBehaviour {
    [Tooltip("What item is this?")]
    public Inventory.Item item;
    [Tooltip("The icon that represents this object in your Journal and when acquiring the item.")]
    public Sprite itemIcon;
    [Tooltip("An array of clips from which one is played when collecting this pickup.")]
    public AudioClip audioClip;
    [Tooltip("The particle system prefab that gets spawned at the location where this pickup is collected.")]
    public ParticleSystem collectionEffect;
    [Tooltip("The dialogue that plays when you acquire the item.")]
    public Events.Dialogue acquisitionDialogue;
}