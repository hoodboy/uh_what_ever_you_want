﻿using UnityEngine;
using GameManager;
using System.Collections;
#if UNITY_EDITOR
using UnityEditor;
#endif

[AddComponentMenu("AnimCol/Inventory/InventoryPickUp")]
[RequireComponent(typeof(SpriteRenderer))]
#if UNITY_EDITOR
[InitializeOnLoad]
#endif
[ExecuteInEditMode]
public class InventoryPickUp : MonoBehaviour {
    [Tooltip("the prefab of the inventory item you plan to attach to this InventoryPickUp.")]
    public InventoryItem inventoryItem;
    [Tooltip("A set of player features that can be tweaked when receiving this InventoryPickUp.")]
    public Player.FeatureConfig featureConfig;

    private Collider2D col;
    private SpriteRenderer spriteRenderer;

    void Start() {
        if (Application.isPlaying) {
            Rigidbody2D rb2D = GetComponent<Rigidbody2D>();
            col = GetComponent<Collider2D>();

            if (rb2D) {
                rb2D.constraints = RigidbodyConstraints2D.FreezePositionX | RigidbodyConstraints2D.FreezeRotation;
            }

            col.isTrigger = !(rb2D != null);
        }
    }

    void Update() {
        if (!Application.isPlaying && inventoryItem) {
            if (!spriteRenderer) {
                spriteRenderer = GetComponent<SpriteRenderer>();
            }

            if (spriteRenderer && spriteRenderer.sprite != inventoryItem.itemIcon) {
                Collider2D[] colliders = GetComponents<Collider2D>();
                if (colliders != null && colliders.Length > 0) {
                    foreach (Collider2D col2d in colliders) {
                        DestroyImmediate(col2d);
                    }
                }
                spriteRenderer.sprite = inventoryItem.itemIcon;
                gameObject.AddComponent<PolygonCollider2D>();
            }
        }
    }

    void OnTriggerEnter2D(Collider2D other) {
        if (Application.isPlaying && other.tag == "Player" && Generic.isControllable) {
            StartCoroutine(ObtainInventoryPickUp());
        }
    }

    private IEnumerator ObtainInventoryPickUp() {
        if (!string.IsNullOrEmpty(inventoryItem.acquisitionDialogue.text)) {
            Generic.isControllable = false;
            yield return StartCoroutine(Generic.DisplayDialogue(inventoryItem.acquisitionDialogue));
            Generic.isControllable = true;
        }

        EvaluateWhichInventoryPickUp();

        if (inventoryItem.collectionEffect) {
            Instantiate(inventoryItem.collectionEffect, transform.position, Quaternion.identity);
        }

        AudioClip[] audioClips = { inventoryItem.audioClip };
        Generic.PlaySFX(audioClips);
        featureConfig.SetPlayerFeatures();
        Destroy(gameObject);
    }

    private void EvaluateWhichInventoryPickUp() {
        switch ((int)inventoryItem.item) {
            case 0: //Colectible
                InventoryManager.inventory.EvaluateAcquisition(ref InventoryManager.inventory.checkList.collectible);
                break;
            case 1: //Fake Treasure
                InventoryManager.inventory.EvaluateAcquisition(ref InventoryManager.inventory.checkList.fakeTreasure, true);
                break;
            case 2: //Journal Page
                InventoryManager.inventory.EvaluateAcquisition(ref InventoryManager.inventory.checkList.journalPage, true);
                break;
            case 3: //Orb 
                InventoryManager.inventory.EvaluateAcquisition(ref InventoryManager.inventory.checkList.orb, true);
                break;
            case 4: //Super Collectible
                InventoryManager.inventory.EvaluateAcquisition(ref InventoryManager.inventory.checkList.superCollectible, true);
                break;
            case 5: //Super Orb
                InventoryManager.inventory.EvaluateAcquisition(ref InventoryManager.inventory.checkList.superOrb, true);
                break;
        }
    }

    private void Log () {
        if (!col && Application.isPlaying) Debug.Log(gameObject.name + ": No Collider2D has been assigned.");
    }
}
