﻿using UnityEngine;
using System.Collections;
using GameManager;

[AddComponentMenu("AnimCol/Inventory/InventoryEventsTrigger")]
public class InventoryEventsTrigger : Events {
    [Header("Trigger parameters")]
    [Tooltip("Whether or not the event should only be allowed to fire once.")]
    public bool fireOnlyOnce = true;
    [Tooltip("Whether or not the 'Submit' button needs to be pressed while inside the trigger-collider in order to trigger the events.")]
    public bool isButtonActivated;
    [Tooltip("An animator object that will animate between active and inactive unconditionally when entering and exiting the trigger collider.")]
    public Animator enterExitAnimator;
    [Header("EndGame")]
    [Tooltip("This will cause the events to only be triggerable if the player's score has met the target score. That will then mean that this event will end the game by transitioning to the CSOutro scene, which will happen after all of the events have fired. If this is enabled, the endDialogue will not be played.")]
    public bool shouldEndTheGame;
    [Tooltip("Dialogue that plays if you don't have enough gems to trigger this End Game event.")]
    public Dialogue fallbackDialogue;
    [Header("Inventory Check")]
    [Tooltip("A list of items you must have acquired before this event can be triggered.")]
    public Inventory.CheckList requirements;
    [Tooltip("Dialogue that will play if the player encounters this while also meeting the requirements.")]
    public Dialogue successDialogue;
    [Tooltip("Dialogue that will play if the player encounters this while failing to meet the requirements.")]
    public Dialogue failDialogue;
    [Tooltip("The level to transition to when ending the level.")]
    public string nextLevel = "CSOutro";

    private bool hasFired;
    private Animator[] enterExitAnimatorArray;

    private void Start() {
        Generic.CheckForCollider(gameObject);

        if (enterExitAnimator) {
            enterExitAnimatorArray = new Animator[1];
            enterExitAnimatorArray[0] = enterExitAnimator;
        }
    }

    private void OnTriggerEnter2D(Collider2D other) {
        if (!hasFired 
        &&  other.tag == "Player" 
        &&  !isButtonActivated) {
            if (enterExitAnimator) {
                Generic.ToggleAnimators(enterExitAnimatorArray);
            }
            AttemptActivation();
        }
    }

    private void OnTriggerExit2D(Collider2D other) {
        if (enterExitAnimator
        &&  other.tag == "Player" 
        && !hasFired) {
            Generic.ToggleAnimators(enterExitAnimatorArray);
        }
    }

    private void OnTriggerStay2D(Collider2D other) {
        if (!hasFired 
        &&  other.tag == "Player" 
        &&  isButtonActivated 
        &&  Input.GetButtonDown("Submit")) {
            AttemptActivation();
        }
    }

    void AttemptActivation() {
        if (shouldEndTheGame) {
            if ((Generic.scoreSystem && HasReachedTargetScore()) || !Generic.scoreSystem) {
                StartCoroutine(Activate());
            } else {
                StartCoroutine(FallBackDialogue());
            }
        } else {
            StartCoroutine (Activate());
        }
    }

    private IEnumerator Activate() {
        Generic.isControllable = false;
        hasFired = true;

        if (InventoryManager.CheckInventory(requirements)) {
            if (!string.IsNullOrEmpty(successDialogue.text)) {
                yield return StartCoroutine(Generic.DisplayDialogue(successDialogue));
            }

            yield return StartCoroutine(TriggerEvents(shouldEndTheGame, nextLevel));

            hasFired = fireOnlyOnce;
            isButtonActivated = !fireOnlyOnce;
        } else if (!string.IsNullOrEmpty(failDialogue.text)) {
            yield return StartCoroutine(Generic.DisplayDialogue(failDialogue));
            hasFired = false;
        }

        Generic.isControllable = true;
    }

    private IEnumerator FallBackDialogue() {
        Generic.isControllable = false;
        yield return StartCoroutine(Generic.DisplayDialogue(fallbackDialogue));
        Generic.isControllable = true;
    }

    bool HasReachedTargetScore() {
        return Generic.scoreSystem && (Generic.score >= Generic.targetScore);
    }
}
