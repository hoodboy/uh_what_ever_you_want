﻿using UnityEngine;
using System;
#if UNITY_EDITOR
using UnityEditor;
#endif

#if UNITY_EDITOR
[InitializeOnLoad]
#endif
[ExecuteInEditMode]
[AddComponentMenu("AnimCol/Inventory/Inventory")]
public class Inventory : MonoBehaviour {
    public enum Item {
        Collectible      = 0,
        FakeTreasure     = 1,
        JournalPage      = 2,
        Orb              = 3,
        SuperCollectible = 4,
        SuperOrb         = 5
    }

    [Serializable]
    public class CheckList {
        public int collectible;
        public bool fakeTreasure;
        public bool journalPage;
        public bool orb;
        public bool superCollectible;
        public bool superOrb;
    }

    public CheckList checkList;

    void Awake() {
        InventoryManager.inventory = this;
    }

    public void EvaluateAcquisition(ref bool item, bool acquisition) {
        item = acquisition;
    }

    public void EvaluateAcquisition(ref int item) {
        item ++;
    }
}