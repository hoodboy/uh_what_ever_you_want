﻿using UnityEngine;
using GameManager;
using System;
using System.Collections;

[AddComponentMenu("AnimCol/Character/Enemy")]
public class Enemy : MonoBehaviour {
    public enum Mode {
        idle,
        patrol,
        agro
    }

    [Serializable]
    public class WhenEnemyDies {
        [Tooltip("When the player jumps on this enemy's head, he will bounce. This value determines how much power is applied to that bounce.")]
        public float bounceMultiplier = 10.0f;
        [Tooltip("Place as many Audio Clips in here as you like, when the enemy dies, one sound will play at random.")]
        public AudioClip[] deathSounds;
        [Tooltip("A prefab of a particle system should go here. The particle system will be spawned when this enemy is killed, and will appear at the enemy's transform position by default.")]
        public ParticleSystem deathEffect;
        [Tooltip("A prefab of a PickUp will should go here. The PickUp will be spawned when this enemy is killed, and will appear at the enemy's transform position by default.")]
        public PickUp pickupToSpawn;
        [Tooltip("Offset the location that the deathEffect and pickUpToSpawn spawn at. This is a resolution for the fact that this enemy's pivot point may not necessarily be the ideal place to spawn these prefabs.")]
        public Vector3 spawnOffset;
    }

    [Serializable]
    public struct WhenPlayerDies {
        [Tooltip("A random list of sounds from which one will be chosen to play when this enemy kills the player.")]
        public AudioClip[] killSounds;
        [Tooltip("The potential messages that can be played when you die from this KillCollider.")]
        public PopUpTrigger.PopUpEvent[] deathMessages;
    }

    [Serializable]
    public class AIBehaviour {
        [Tooltip("Enable this, and jumping on the enemy's head will trigger a bounce animation instead of dying.")]
        public bool isInvincible;
        [Tooltip("If this is enabled, LOS and attack range become meaningless, this enemy will no longer attack, rather, simply touching the enemy will damage the player, unless the player collides from above, which will kill the enemy.")]
        public bool damagePlayerOnContact = false;
        [Tooltip("The enemy will enter it's Agro state if the Player is found inside the EnemySpawner's trigger volume.")]
        public bool agroNearbyPlayer = true;
        [Tooltip("The enemy will stop moving left/right while playing the 'Recoil' animation state.")]
        public bool pauseDuringRecoil = false;
        [Tooltip("The line-of-sight of the enemy is a single ray. That ray will always look in the forward-facing direction of this enemy. Zero will place it at the same height as this enemy's transform. USe this value to offset that height.")]
        public float LOSHeight = 1.0f;
        [Tooltip("The line-of-sight of the enemy is a single ray. That ray will always look in the forward-facing direction of this enemy. This value determines the length of this ray, which ultimately, is how far in front of him/herself this enemy is able to detect the player, before attacking.")]
        public float LOSDistance = 2.0f;
        [Tooltip("Attacks will have a different reach than the line-of-sight. Adjust the value to set the distance at which the player should be detected when determining damage.")]
        public float attackRange = 1.5f;
    }

    [Tooltip("Enemies will patrol left and right, set the speed at which they move.")]
    public float speed = 1.0f;
    [Tooltip("The amount of damage applied when this enemy manages to strike the player. Leave this at zero, and it will cause instant death.")]
    public int damage;
    [Tooltip("A random list of sounds from which one will be chosen to play when beginning to attack the player.")]
    public AudioClip[] attackSounds;
    [Tooltip("Parameters particularly pertaining to the enemy's patrol behaviour.")]
    public AIBehaviour aIBehaviour;
    [Tooltip("Some features pertaining to an event in which the player is killed by this enemy.")]
    public WhenPlayerDies whenPlayerDies;
    [Tooltip("Some features pertaining to an event in which this enemy is killed by the player.")]
    public WhenEnemyDies whenEnemyDies;

    [NonSerialized]
    public EnemyZone enemyZone;
    [NonSerialized]
    public EnemyZone.PatrolBoundaries patrolBoundaries;

    private SpriteRenderer spriteRenderer;
    private Rigidbody2D rb2d;
    private Vector2 targetPosition;
    private Vector2 patrolPosition;
    private Vector2 lineOfSight;
    private Animator anim;
    private Collider2D col2D;
    private int layerMask;
    private bool isFacingRight;
    private bool isRecoiling;
    private bool isAttacking;

    void Awake() {
        col2D = GetComponent<Collider2D>();
        rb2d = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
        spriteRenderer = GetComponent<SpriteRenderer>();

        layerMask = 1 << LayerMask.NameToLayer("Player");
    }

    void Start () {
        patrolBoundaries.max = new Vector2(patrolBoundaries.max.x, patrolBoundaries.min.y);
        StartCoroutine(MainLoop());
    }

    void Update() {
        isFacingRight = IsFacingRight();
        anim.SetFloat("FacingDirection", isFacingRight ? -1.0f : 1.0f);
        if (!aIBehaviour.damagePlayerOnContact) {
            RaycastToFindTarget();
        }

        anim.SetBool("IsAttack", isAttacking);
        isRecoiling = anim.GetCurrentAnimatorStateInfo(0).IsName("Recoil");
    }

    void OnCollisionEnter2D(Collision2D col) {
        if (col.gameObject.tag == "Player") {
            if ((col2D.bounds.extents.y + transform.position.y) <= col.transform.position.y) {
                AttackedByPlayer(col);
            } else if (aIBehaviour.damagePlayerOnContact) {
                HitPlayer();
            }
        }
    }

    private bool IsFacingRight() {
        return targetPosition.x < transform.position.x;
    }

    private IEnumerator MainLoop() {
        while (true) {
            if (UnityEngine.Random.Range(0, 2) == 0) {
                yield return StartCoroutine(Idle());
            } else {
                yield return StartCoroutine(Patrol());
            }
        }
    }

    private IEnumerator Idle() {
        anim.SetInteger("mode", (int)Mode.idle);

        float timer = UnityEngine.Random.Range(1.0f, 3.0f);
        while (timer > 0.0f) {
            if (transform.position.x > patrolBoundaries.max.x) {
                rb2d.velocity = (Vector3.left * (speed * 1.5f));
            }

            if (transform.position.x < patrolBoundaries.min.x) {
                rb2d.velocity = (Vector3.right * (speed * 1.5f));
            }

            timer -= Time.deltaTime;
            yield return null;
        }
    }

    public IEnumerator Patrol() {
        anim.SetInteger("mode", (int)Mode.patrol);

        // Get new patrol position.
        targetPosition = UnityEngine.Random.Range(0, 2) == 0 ? patrolBoundaries.min : patrolBoundaries.max;

        float randomCountdown = UnityEngine.Random.Range(3, 8);
        while (Vector3.Distance(targetPosition, transform.position) >= aIBehaviour.LOSDistance 
        &&    randomCountdown > 0.0f) {
            GoToTarget();

            randomCountdown -= Time.deltaTime;
            yield return null;
        }
    }

    public IEnumerator Agro() {
        anim.SetInteger("mode", (int)Mode.agro);

        while (true) {
            targetPosition = Generic.player.transform.position;
            GoToTarget();
            yield return null;
        }
    }

    private void GoToTarget() {
        if (!aIBehaviour.pauseDuringRecoil || (aIBehaviour.pauseDuringRecoil && !isRecoiling)) {
            rb2d.velocity = new Vector2(anim.GetFloat("FacingDirection") * speed, rb2d.velocity.y);
        }

        if (transform.position.x > patrolBoundaries.max.x) {
            rb2d.velocity = (Vector3.left * (speed * enemyZone.enemies.Count));
        }

        if (transform.position.x < patrolBoundaries.min.x) {
            rb2d.velocity = (Vector3.right * (speed * enemyZone.enemies.Count));
        }
    }

    private void AttackedByPlayer(Collision2D col) {
        ConfirmAttackHasEnded();
        // Make player bounce off of enemy in a direction that kinda can make sense, I'm not confident this actually makes sense right now though :P.
        Vector2 bounceDirection = (col.transform.position.x < transform.position.x ? Vector2.left : Vector2.right) + Vector2.up;
        col.gameObject.GetComponent<Rigidbody2D>().velocity = bounceDirection * whenEnemyDies.bounceMultiplier;

        Generic.player.StopDamagingCoroutines();

        if (aIBehaviour.isInvincible) {
            Generic.PlaySFX(whenEnemyDies.deathSounds);
            anim.SetTrigger("Recoil");
        } else {
            Generic.PlaySFX(whenEnemyDies.deathSounds);
            Instantiate(whenEnemyDies.deathEffect, transform.position + whenEnemyDies.spawnOffset, Quaternion.identity);
            Instantiate(whenEnemyDies.pickupToSpawn, transform.position + whenEnemyDies.spawnOffset, Quaternion.identity);
            enemyZone.enemies.Remove(this);

            Destroy(gameObject);
        }
    }

    public void HitPlayer() {
        if (Vector3.Distance (Generic.player.transform.position, transform.position) <= aIBehaviour.attackRange) {
            Generic.PlaySFX(attackSounds);

            if (Mathf.Abs(damage) > 0 && !Generic.player.isReceivingDamage) {
                Generic.player.StopDamagingCoroutines();
                // StartCoroutine(Generic.player.ReceiveDamage(whenPlayerDies.killSounds, whenPlayerDies.deathMessages, -damage));
                // We do this with an event instead.
                Generic.eventManager.HitPlayer(whenPlayerDies.killSounds, whenPlayerDies.deathMessages, damage);
            } else {
                // Generic.player.Die(whenPlayerDies.killSounds, whenPlayerDies.deathMessages);
                // We do this with an event instead.
                Generic.eventManager.DiePlayer(whenPlayerDies.killSounds, whenPlayerDies.deathMessages);
            }
        }
    }

    private bool RaycastToFindTarget() {
        lineOfSight = new Vector2( 
            transform.position.x,
            transform.position.y + aIBehaviour.LOSHeight
        );

        if (anim.GetInteger("mode") == (int)Mode.agro) {
            Debug.DrawLine(lineOfSight, Generic.player.transform.position, Color.red);
        };

        Vector2 topOfSprite = new Vector2(transform.position.x, spriteRenderer.bounds.max.y);
        Debug.DrawLine(lineOfSight, lineOfSight + (isFacingRight ? Vector2.left : Vector2.right) * aIBehaviour.LOSDistance, Color.magenta);
        Debug.DrawLine(topOfSprite, patrolBoundaries.min, Color.green);
        Debug.DrawLine(topOfSprite, patrolBoundaries.max, Color.blue);
        Debug.DrawRay(topOfSprite, transform.forward * aIBehaviour.LOSDistance, Color.blue);

        RaycastHit2D hit = Physics2D.Raycast(
            lineOfSight,
            isFacingRight ? Vector2.left : Vector2.right,
            aIBehaviour.LOSDistance,
            layerMask
        );

        if (hit && hit.collider.tag == "Player" && !isAttacking) {
            isAttacking = true;
        }

        return false;
    }

    public void ConfirmAttackHasEnded() {
        isAttacking = false;
    }
}
