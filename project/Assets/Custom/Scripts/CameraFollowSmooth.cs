﻿using UnityEngine;
using System;
using System.Collections;
#if UNITY_EDITOR
using UnityEditor;
#endif

#if UNITY_EDITOR
[InitializeOnLoad]
#endif
[ExecuteInEditMode]
[AddComponentMenu("AnimCol/Camera/CameraFollowSmooth")]
public class CameraFollowSmooth : MonoBehaviour {
    [Serializable]
    public class CameraShake {
        [Tooltip("The amount of time that the Camera Shake should last for.")]
        public float duration = 1.0f;
        [Range(0,1)]
        [Tooltip("Essentially, increase this for a shakier motion when the Shake() method is called.")]
        public float magnitude = 1.0f;
    }

    [Tooltip("Increase this value to make the camera take longer to catch up to it's target.")]
    public float dampTime = 0.15f;
    [Tooltip("This camera follows a target smoothly, that target is placed here.")]
    public Transform target;
    [Tooltip("Whether or not to limit the minimum and maximum X position of this Camera.")]
    public bool useLimitX;
    [Tooltip("The minimum and maximum X position that the camera is allowed to travel between.")]
    public Vector2 minMaxX = new Vector2(-100, 100);
    [Tooltip("Whether or not to limit the minimum and maximum Y position of this Camera.")]
    public bool useLimitY;
    [Tooltip("The minimum and maximum Y position that the camera is allowed to travel between.")]
    public Vector2 minMaxY = new Vector2(-100, 100);
    [Tooltip("The colour of the lines that are drawn when you have 'Debug Mode' enabled.")]
    public Color rayCastColour = Color.magenta;
    [Tooltip("Enable this to see a line drawn from your lower to upper xPos limits.")]
    public bool debugMode;
    public CameraShake cameraShake;

    private Vector3 velocity = Vector3.zero;
    private Camera cam;
    private bool isFollowing = true;

    void Awake() {
        cam = GetComponent<Camera>();
    }

    void FixedUpdate() {
        if (Application.isPlaying) {
            if (!cam) {
                cam = GetComponent<Camera>();
            }

            if (target && isFollowing) {
                Vector3 point = cam.WorldToViewportPoint(target.position);
                Vector3 delta = target.position - cam.ViewportToWorldPoint(new Vector3(0.5f, 0.5f, point.z));
                Vector3 destination = transform.position + delta;
                Vector3 newPosition = Vector3.SmoothDamp(transform.position, destination, ref velocity, dampTime);
                float xPos = newPosition.x;
                float yPos = newPosition.y;

                if (useLimitX) {
                    xPos = Mathf.Clamp(xPos, minMaxX.x, minMaxX.y);
                }
                if (useLimitY) {
                    yPos = Mathf.Clamp(yPos, minMaxY.x, minMaxY.y);
                }

                newPosition = new Vector3(xPos, yPos, newPosition.z);
                transform.position = newPosition;
            }
        }
    }

    void LateUpdate() {
        if (!Application.isPlaying) {
            float xPos = target.position.x;
            float yPos = target.position.y;

            if (useLimitX) {
                xPos = Mathf.Clamp(target.position.x, minMaxX.x, minMaxX.y);
            }
            if (useLimitY) {
                yPos = Mathf.Clamp(target.position.y, minMaxY.x, minMaxY.y);
            }

            transform.position = new Vector3(xPos, yPos, transform.position.z);
        }

        DebugCameraLimits();
    }

    private void DebugCameraLimits() {
#if UNITY_EDITOR
        if (debugMode) {
            float centerOfX = Mathf.Lerp(minMaxX.x, minMaxX.y, 0.5f);
            float centerOfY = Mathf.Lerp(minMaxY.x, minMaxY.y, 0.5f);

            Vector3 debugStart = new Vector2(minMaxX.x, centerOfY);
            Vector3 debugEnd = new Vector2(minMaxX.y, centerOfY);
            Debug.DrawLine(debugStart, debugEnd, rayCastColour);

            debugStart = new Vector2(centerOfX, minMaxY.x);
            debugEnd = new Vector2(centerOfX, minMaxY.y);
            Debug.DrawLine(debugStart, debugEnd, rayCastColour);
        }
#endif
    }

    public IEnumerator Shake() {
        isFollowing = false;

        Vector3 originalCamPos = Camera.main.transform.position;

        float elapsed = 0.0f;

        while (elapsed < cameraShake.duration) {
            elapsed += Time.deltaTime;

            float percentComplete = elapsed / cameraShake.duration;
            float damper = 1.0f - Mathf.Clamp(4.0f * percentComplete - 3.0f, 0.0f, 1.0f);

            // map value to [-1, 1]
            float x = UnityEngine.Random.value * 2.0f - 1.0f;
            float y = UnityEngine.Random.value * 2.0f - 1.0f;
            x *= cameraShake.magnitude * damper;
            y *= cameraShake.magnitude * damper;

            Camera.main.transform.position = originalCamPos + new Vector3(x, y, 0);

            yield return null;
        }

        Camera.main.transform.position = originalCamPos;
        isFollowing = true;
    }
}