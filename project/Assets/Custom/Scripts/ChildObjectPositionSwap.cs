﻿using UnityEngine;
using System.Collections.Generic;

public class ChildObjectPositionSwap : MonoBehaviour {
    void Awake () {
        List<Transform> objects = new List<Transform>();
        List<Vector3> positions = new List<Vector3>();

        foreach (Transform t in transform) {
            objects.Add(t);
            positions.Add(t.position);
        }

        while (objects.Count > 0) {
            Transform randomObj = objects[Random.Range(0, objects.Count)];
            Vector3 randomPosition = positions[Random.Range(0, positions.Count)];

            randomObj.position = randomPosition;

            objects.Remove(randomObj);
            positions.Remove(randomPosition);
        }
    }
}
