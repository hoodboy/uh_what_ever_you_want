﻿using UnityEngine;
using System.Collections;

[AddComponentMenu("")]
public class PuzzleRotationTrigger : MonoBehaviour {
    public PuzzleRotation puzzle { get; set; }
    public Transform targetToRotate { get; set; }

    private bool rotating = false;
    private Animator animator;

    private void Start() {
        if (GetComponent<Animator>()) {
            animator = GetComponent<Animator>();
        }
    }

    private void OnTriggerEnter2D(Collider2D other) {
        if (other.tag == "Player") {
            GameManager.Generic.PlaySFX(puzzle.rotateSound);

            if (animator) {
                animator.SetBool("active", true);
            } else {
                Debug.LogWarning(gameObject.name + " is missing an animator, please assign one and provide an active and inactive state with a bool named 'active', use the active state to transition between the two states.");
            }

            if (!rotating && !puzzle.hasFired) {
                StartCoroutine("Rotate");
            }
        }
    }

    private void OnTriggerExit2D(Collider2D other) {
        if (other.tag == "Player") {
            if (animator) {
                animator.SetBool("active", false);
            }
        }
    }

    private IEnumerator Rotate() {
        rotating = true;

        float targetZRotation = targetToRotate.eulerAngles.z >= (360-(int)puzzle.rotationStep) ?
            360.0f : 
            targetToRotate.eulerAngles.z + (int)puzzle.rotationStep;

        while (targetToRotate.eulerAngles.z < targetZRotation) {
            targetToRotate.Rotate(Vector3.forward * Time.deltaTime * (int)puzzle.rotationStep);

            if (targetZRotation == 360.0f) {
                if (targetToRotate.eulerAngles.z < (int)puzzle.rotationStep) {
                    targetToRotate.eulerAngles = Vector3.zero;
                    break;
                }
            }
            else if (targetToRotate.eulerAngles.z >= targetZRotation) {
                targetToRotate.eulerAngles = new Vector3(0, 0, targetZRotation);
                break;
            }

            yield return null;
        }

        rotating = false;
        puzzle.RotationCheck();
    }
}
