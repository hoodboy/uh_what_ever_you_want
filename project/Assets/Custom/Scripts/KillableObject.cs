﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[AddComponentMenu("AnimCol/Misc/KillableObject")]
public class KillableObject : MonoBehaviour {
    [Tooltip("Whether or not the object should respawn at it's original location")]
    public bool respawn;
    [Tooltip("The time it should take before the object is respawned after dying.")]
    public float respawnDelay;
    [Tooltip("The particle system prefab that appears at the location where the object is respawned.")]
    public ParticleSystem respawnFX;
    [Tooltip("The particle system prefab that appears at the location where the object is destroyed.")]
    public ParticleSystem killedFX;
    [Tooltip("Leave this at zero if you just want it to respawn at it's currently placed position")]
    public Vector3 spawnPosition;

    private Quaternion initialRotation;
    private Rigidbody2D rb;
    private List<MeshRenderer> meshRenderers = new List<MeshRenderer>();

    void Start () {
        if (spawnPosition == Vector3.zero) {
            spawnPosition = transform.position;
        }

        rb = GetComponent<Rigidbody2D>();

        foreach (MeshRenderer mRenderer in GetComponentsInChildren<MeshRenderer>()) {
            meshRenderers.Add(mRenderer);
        }

        initialRotation = transform.rotation;
    }

    void OnTriggerEnter2D(Collider2D other) {
        if (other.tag == "KillBox") {
            if (killedFX) {
                Instantiate(killedFX.gameObject, transform.position, Quaternion.identity);
            }

            if (respawn) {
                StartCoroutine("Respawn");
            }
            else {
                Destroy(gameObject);
            }
        }
    }

    private IEnumerator Respawn () {
        foreach (MeshRenderer mRenderer in meshRenderers) {
            mRenderer.enabled = false;
        }

        yield return new WaitForSeconds(respawnDelay);
        rb.velocity = Vector3.zero;

        transform.position = spawnPosition;
        transform.rotation = initialRotation;

        foreach (MeshRenderer mRenderer in meshRenderers) {
            mRenderer.enabled = true;
        }

        if (respawnFX) {
            Instantiate(respawnFX.gameObject, transform.position, Quaternion.identity);
        }

        yield return null;
    }
}
