﻿using UnityEngine;

public class FallEvent : MonoBehaviour {
	public string triggerName;

    private Animator anim;

	void Start () {
		anim = GameManager.Generic.player.GetComponent<Animator>();
	}
	
	void OnTriggerEnter2D(Collider2D col2d){
		if (col2d.tag == "Player") {
			anim.SetTrigger (triggerName);
        }
	}
}
