﻿using UnityEngine;

public class SetCameraSmoothDamp : MonoBehaviour {
	private float originalDampTime;

    void Start () {
		originalDampTime = Camera.main.GetComponent<CameraFollowSmooth>().dampTime;
		Camera.main.GetComponent<CameraFollowSmooth> ().dampTime = 0.0f;
	}

	void OnTriggerEnter2D(Collider2D col2d) {
		Camera.main.GetComponent<CameraFollowSmooth>().dampTime = originalDampTime;
        GameManager.Generic.spawnPoint = transform;
	}
}
