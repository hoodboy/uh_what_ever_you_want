﻿using UnityEngine;
using GameManager;

public class AudioAnimEvents : MonoBehaviour {
    public AudioClip[] wildTake2;
    public AudioClip wildTake3;
    public AudioClip[] wildTake4;

    public void AudioWildTake2 () {
        Generic.PlaySFX(wildTake2);
    }

    public void AudioWildTake3() {
        Generic.sfx.clip = wildTake3;
        Generic.sfx.Play();
    }

    public void AudioWildTake4() {
        Generic.sfx.Stop();
        Generic.PlaySFX(wildTake4);
    }
}
