﻿using UnityEngine;

public class LightTest : MonoBehaviour {
    public float speed = 3.0f;
    public float jumpPower = 3.0f;
    public Light spotlightRight;
    public Light spotlightLeft;
    public ParticleSystem boostEffect;

    private Rigidbody2D rb2d;
    private SpriteRenderer sprRend;
    private SpriteRenderer headSprRend;

    void Start() {
        rb2d = GetComponent<Rigidbody2D>();
        sprRend = GetComponent<SpriteRenderer>();
        headSprRend = transform.GetChild(0).GetComponent<SpriteRenderer>();
        spotlightLeft.enabled = false;
    }

    void Update() {
        float h = Input.GetAxis("Horizontal");
        rb2d.velocity = new Vector2(h * speed, rb2d.velocity.y);

        if (Input.GetButton("Horizontal")) {
            sprRend.flipX = !(Input.GetAxis("Horizontal") > 0);
            headSprRend.flipX = !(Input.GetAxis("Horizontal") > 0);
            spotlightRight.enabled = (Input.GetAxis("Horizontal") > 0);
            spotlightLeft.enabled = !(Input.GetAxis("Horizontal") > 0);
        }

        if (Input.GetMouseButtonDown(0)) {
            rb2d.AddForce(Vector3.up * jumpPower, ForceMode2D.Impulse);
            boostEffect.Play();
        }

        Vector3 mousePosition = Input.mousePosition;
        mousePosition = Camera.main.ScreenToWorldPoint(mousePosition);

        sprRend.flipX = !(mousePosition.x > transform.position.x);
        headSprRend.flipX = !(mousePosition.x > transform.position.x);
        spotlightRight.enabled = (mousePosition.x > transform.position.x);
        spotlightLeft.enabled = !(mousePosition.x > transform.position.x);
    }
}
