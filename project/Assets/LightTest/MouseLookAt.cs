﻿using UnityEngine;

public class MouseLookAt : MonoBehaviour {
    public float rotationPower;
    public float rotationOffset = 22.5f;

    private Vector3 mousePosition;
    private SpriteRenderer sprRend;

    void Start() {
        sprRend = GetComponent<SpriteRenderer>();
    }

    void Update () {
        mousePosition = Input.mousePosition;
        mousePosition = Camera.main.ScreenToWorldPoint(mousePosition);

        transform.localEulerAngles =
            sprRend.flipX
        ?   new Vector3(0, 0, ((-mousePosition.y) * rotationPower) + rotationOffset)
        :   new Vector3(0, 0, ((mousePosition.y) * rotationPower) - rotationOffset);
    }
}
