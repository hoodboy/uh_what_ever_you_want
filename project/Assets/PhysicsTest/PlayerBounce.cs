﻿using UnityEngine;
using UnityEngine.UI;

public class PlayerBounce : MonoBehaviour {
    public enum MyColor {
        Red,
        Blue
    }

    public MyColor myColor;
    public KeyCode boostButton;
    public float boostPower = 3.0f;
    public Text scoreText;

    static int redScore = 0;
    static int blueScore = 0;

    private Rigidbody2D rb2d;
    private bool isGrounded;

    void Awake () {
        rb2d = GetComponent<Rigidbody2D>();

        if (myColor == MyColor.Red) {
            scoreText.text = redScore.ToString();
        } else {
            scoreText.text = blueScore.ToString();
        }
    }
    
    void Update () {
        if (isGrounded && Input.GetKeyDown(boostButton)) {
            rb2d.velocity = Vector2.one * boostPower;
            isGrounded = false;
        }
    }

    void OnCollisionEnter2D(Collision2D col2D) {
        isGrounded = true;
    }

    void OnTriggerEnter2D(Collider2D other) {
        if (other.tag == "Victory") {
            if (myColor == MyColor.Red) {
                redScore++;
                scoreText.text = redScore.ToString();
            } else {
                blueScore++;
                scoreText.text = blueScore.ToString();
            }
        }

        StartCoroutine(GameManager.Generic.LoadScene(UnityEngine.SceneManagement.SceneManager.GetActiveScene().name));
    }
}
